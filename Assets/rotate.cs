﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotate : MonoBehaviour {


    public float smoothTime = 1f;
    private Vector3 currentVelocity = Vector3.zero;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () { 

        Vector3 rotationDelta = transform.eulerAngles ;
        rotationDelta.y += 2f;

        transform.eulerAngles = Vector3.SmoothDamp(transform.eulerAngles, rotationDelta, ref currentVelocity , smoothTime);

        
          
	}
}
