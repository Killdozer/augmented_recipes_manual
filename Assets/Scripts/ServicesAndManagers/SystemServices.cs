﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Vuforia;

/// <summary>
/// This class loads different low level system services on startup and makes them accessible.
/// </summary>


[RequireComponent(typeof(TextToSpeech))]

public class SystemServices : Singleton<SystemServices>
{


                                                     /// <summary> The url used for testing whether the application has internet connectivity or not.</summary>
    [Tooltip("Set the url used for testing whether the application has internet connectivity or not.")]
    public string connectivityTestUrl;
                                                     /// <summary> The system Web APIs endpoint.</summary>
    [Tooltip("Enter the system Web APIs endpoint.")]
    public string systemApiEndpoint;
                                                     /// <summary> The base url of the server from where to fetch the recipes videos.</summary>
    [Tooltip("Set the base url of the server from where to fetch the recipes videos.")]
    public string videoLoaderUrl;
                                                     /// <summary> The web requests timeout period in seconds.Timeout period is the time after which the web requests are aborted automatically.</summary>
    [Tooltip("Set the web requests timeout period in seconds.Timeout period is the time after which the web requests are aborted automatically.")]
    public int timeout;

                                                     /// <summary> Flag indicating whether the TTS engine initialized successfully or not.</summary>
    public bool textToSpeechFailed { get; private set; }                         

                                                     /// <summary> Reference to the TextToSpeech engine. </summary> 
    public TextToSpeech textToSpeech { get; private set; }

                                                     /// <summary> Reference to the Tesseract engine. </summary> 
    public TesseractWrapper_And tesseract { get; private set; }

                                                     /// <summary> This flag indicates whether the application is connected to the internet or not.It gets refreshed whenever a call to AsyncInternetConnectivityCheck is made.  </summary> 
    [HideInInspector]
    public bool internetConnectivityExists;
   
                                                     /// <summary>  The unique device identifier for this device.</summary>
    [HideInInspector]
    public string deviceUniqueIdentifier;
                                                     /// <summary>  Contains different regex patterns used in the application.</summary>
    [HideInInspector]
    public RegexPatterns regexPatterns;
                                                            
                                                     /// <summary> Indicates whether the copy to file thread is running or not. </summary> 
    private bool threadRunning;

                                                     /// <summary> This variable can be checked whenever an HTTP GET or POST request is made.It contains the HTTP response code against the request made.</summary> 
    public long responseCode { private set; get; }






    [System.Serializable]

    public struct RegexPatterns
    {
        public string netError;
        public string nullOrEmpty;
        public string generalError;
        public string apiMistmatch;
        public string resourceNotFound404;
        public string tabsNewLinesQuotes;
        public string tableRowsDelimitter;
        public string tableColumnsDelimitter;
        public string recipesStepsDelimitter;
        public string ingredientsDelimitter;
    }



    // OnAwake may cause problems use Start instead
    protected override void OnAwake() {



        regexPatterns.netError               = "<neterror>";
        regexPatterns.nullOrEmpty            = "<nullorempty>";
        regexPatterns.generalError           = "<generalerror>";
        regexPatterns.apiMistmatch           = "<apimismatch>";
        regexPatterns.resourceNotFound404    = "<404 Not Found>";
        regexPatterns.tabsNewLinesQuotes     = "\t|\n|\r|\"";
        regexPatterns.tableRowsDelimitter    = "<\\+>";
        regexPatterns.tableColumnsDelimitter = "<\\&>";
        regexPatterns.recipesStepsDelimitter = "<\\#>";
        regexPatterns.ingredientsDelimitter  = "<\\#>";


        deviceUniqueIdentifier = SystemInfo.deviceUniqueIdentifier;
        BlockingInternetConnectivityCheck();

        if(!internetConnectivityExists)
        {
            InterfaceManager interfaceManager = InterfaceManager.instance;
            Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
            interfaceManager.ShowModalWindow("Internet failure", icon, "Some of our services won't be usable.Please check your internet connection or contact admin for further support.", true);
        }

#if !UNITY_EDITOR

        textToSpeech = GetComponent<TextToSpeech>();
        StartCoroutine(LoadTTSEngine());
        StartCoroutine(LoadTesseractEngine());
#else
        StartCoroutine(LoadTesseractEngine());
        Debug.LogWarning("Will not load TTS engine on Unity Editor.");

#endif
        
    }






    /// <summary> Loads and initializes the tesseract engine components. </summary> 

    private IEnumerator LoadTesseractEngine()
    {


        yield return new WaitForEndOfFrame();

        // Files are not accessible in the .jar, so copy them to persistentDataPath

       /*
        string fileDirectory = System.IO.Path.Combine(Application.streamingAssetsPath, "tessdata/");
        FileInfo[] tessDataFiles   = new DirectoryInfo(fileDirectory).GetFiles();
        fileDirectory = System.IO.Path.Combine(Application.streamingAssetsPath, "tessdata/tessconfigs/");
        FileInfo[] tessConfigFiles = new DirectoryInfo(fileDirectory).GetFiles();

        int length = (tessDataFiles.Length + tessConfigFiles.Length);

        for (int a = 0 , b = 0; a < length; a++ )
        {


            //while (threadRunning) { yield return new WaitForSeconds(0); }

            if (a < tessDataFiles.Length)
            {
                if(Regex.IsMatch(tessDataFiles[a].Name , ".meta")) { continue; }
                CopyFile("tessdata/", tessDataFiles[a].Name, false);
            }

            else
            {
                if (Regex.IsMatch(tessConfigFiles[b].Name, ".meta")) { b++;  continue; }
                CopyFile("tessdata/tessconfigs/", tessConfigFiles[b].Name, false);
                b++;
            }
        }
*/

        CopyFile("tessdata/", "eng.cube.bigrams", false);
        CopyFile("tessdata/", "eng.cube.fold", false);
        while (threadRunning) { yield return new WaitForSeconds(0); }
        CopyFile("tessdata/", "eng.cube.lm", false);
        CopyFile("tessdata/", "eng.cube.nn", false);
        while (threadRunning) { yield return new WaitForSeconds(0); }
        CopyFile("tessdata/", "eng.cube.params", false);
        CopyFile("tessdata/", "eng.cube.size", false);
        while (threadRunning) { yield return new WaitForSeconds(0); }
        CopyFile("tessdata/", "eng.cube.word-freq", false);
        CopyFile("tessdata/", "eng.tesseract_cube.nn", false);
        while (threadRunning) { yield return new WaitForSeconds(0); }
        CopyFile("tessdata/", "eng.traineddata", false);
        CopyFile("tessdata/", "eng.user-patterns", false);
        while (threadRunning) { yield return new WaitForSeconds(0); }
        CopyFile("tessdata/", "eng.user-words", false);
        CopyFile("tessdata/", "osd.traineddata", false);
        while (threadRunning) { yield return new WaitForSeconds(0); }
        CopyFile("tessdata/", "pdf.ttf", false);
        CopyFile("tessdata/tessconfigs/", "debugConfigs.txt", false);
        while (threadRunning) { yield return new WaitForSeconds(0); }
        CopyFile("tessdata/tessconfigs/", "recognitionConfigs.txt", false);


        tesseract = new TesseractWrapper_And();

        string datapath = System.IO.Path.Combine(Application.persistentDataPath, "tessdata");
        tesseract.Init("eng", datapath);

    }





    /// <summary> Initializes the TTS engine components. </summary> 

    private IEnumerator LoadTTSEngine()
    {

        yield return new WaitForSeconds(0);
        textToSpeech.Speak("", TTSFailure);

    }




    /// <summary> Delegate that receives the response back from the call to TextToSpeech engine . </summary> 

    private void TTSFailure(string msg)
    {

        if (msg.Length > 0)
        {
            textToSpeechFailed = true;
            Debug.LogError(msg);
            ShowToast(msg + " Please enable Text-to-speech output in Settings>Accessibility. and restart the application.", TextToSpeech.ToastLength.LENGTH_LONG);
        }
        
    }



    /// <summary> Copy a file specified to the folder specified.If the overWrite argument is true the file will be overWritten if it already exists.  </summary> 
    
    private void CopyFile(string folder, string file, bool overWrite)
    {



        string fileUrl = System.IO.Path.Combine(Application.streamingAssetsPath, folder + file);
        string fileDirectory = System.IO.Path.Combine(Application.persistentDataPath, folder);
        string filePath = System.IO.Path.Combine(fileDirectory, file);

     
        //Debug.Log("Copying: " + fileUrl);

        if (!Directory.Exists(fileDirectory)) { Directory.CreateDirectory(fileDirectory); }

        if(File.Exists(filePath) && !overWrite) { return; }

        WWW www = new WWW(fileUrl); 



        while (!www.isDone)
        {
           // Debug.Log("Reading");
        }

        Debug.Log(filePath);
        byte[] bytes = www.bytes;

        Thread thread = new Thread(() => CopyBytesOnThread(filePath, bytes));
        thread.Start();
        
        //Debug.Log("file copy done (" + www.bytes.Length.ToString() + "): " + filePath);

        www.Dispose();
        www = null;

    }





     /// <summary>Copy the bytes specified to the filepath specified using a seperate thread. </summary>

     private void CopyBytesOnThread(string filePath, byte[] bytes)
     {
        threadRunning = true;

        File.WriteAllBytes(filePath, bytes);

        threadRunning = false;
     }






    /// <summary>Show an onscreen toast message for the lenght specified. </summary>

    public void ShowToast(string message, TextToSpeech.ToastLength length)
    {
            if(textToSpeech == null) { Debug.LogWarning("TextToSpeech instance not set.");return; }
            textToSpeech.Toast(message, length);
    }






    /// <summary> Make an asynchronous GET request to the recepient specified in the encodedUrl parameter.Optionally any headers can also be specified. </summary> 

    public IEnumerator AsyncGETRequest(string encodedUrl , Action<string> callback, Dictionary<string, string> headers = null )
    {


        UnityWebRequest webRequest = new UnityWebRequest(encodedUrl);
        webRequest.timeout = timeout;
        webRequest.method = "GET";
        DownloadHandlerBuffer downloadHandler = new DownloadHandlerBuffer();

        webRequest.downloadHandler = downloadHandler;
       

        if (headers != null)
        {
            foreach(var header in headers)
            {
                webRequest.SetRequestHeader(header.Key , header.Value);
            }
        }

        

        yield return webRequest.SendWebRequest();

        responseCode = webRequest.responseCode;


        if(webRequest.isHttpError || webRequest.isNetworkError)
        {
            callback("<neterror>" + webRequest.error);
            //callback(webRequest.responseCode+"-<neterror>" + webRequest.error); 
        }

        else
        {
            if (string.IsNullOrEmpty(webRequest.downloadHandler.text))
            {
                callback("<nullorempty>" + "Error! server returned an empty response.");
                //callback(webRequest.responseCode + "-<nullorempty>" + "Error! server returned an empty response.");
            }

            else
            {
                callback(webRequest.downloadHandler.text);
            }
        }


 }






    /// <summary> Make a blocking GET request to the recepient specified in the encodedUrl parameter.Optionally any headers can also be specified. </summary> 

    public void BlockingGETRequest(string encodedUrl, Action<string> callback, Dictionary<string, string> headers = null)
    {


        UnityWebRequest webRequest = new UnityWebRequest(encodedUrl);
        webRequest.timeout = timeout;
        webRequest.method = "GET";
        DownloadHandlerBuffer downloadHandler = new DownloadHandlerBuffer();

        webRequest.downloadHandler = downloadHandler;


        if (headers != null)
        {
            foreach (var header in headers)
            {
                webRequest.SetRequestHeader(header.Key, header.Value);
            }
        }



        webRequest.SendWebRequest();

        while (!webRequest.isDone) { }

        responseCode = webRequest.responseCode;


        if (webRequest.isHttpError || webRequest.isNetworkError)
        {
            callback("<neterror>" + webRequest.error);
            //callback(webRequest.responseCode + "-<neterror>" + webRequest.error);
        }

        else
        {
            if (string.IsNullOrEmpty(webRequest.downloadHandler.text))
            {
                callback("<nullorempty>" + "Error! server returned an empty response.");
                //callback(webRequest.responseCode + "-<nullorempty>" + "Error! server returned an empty response.");
            }

            else
            {
                callback(webRequest.downloadHandler.text);
            }
        }


    }






    /// <summary> Make a blocking call to POST data to the recepient specified in the baseUrl parameter.Optionally any headers can also be specified. </summary> 

    public void BlockingPOSTRequest(string baseUrl, Action<string> callback, byte[] data , Dictionary<string, string> headers = null)
    {



        UnityWebRequest webRequest = new UnityWebRequest(baseUrl);
        webRequest.timeout = timeout;
        webRequest.method = "POST";
        UploadHandlerRaw uploadHandler        = new UploadHandlerRaw(data);
        DownloadHandlerBuffer downloadHandler = new DownloadHandlerBuffer();
        webRequest.uploadHandler   = uploadHandler; 
        webRequest.downloadHandler = downloadHandler;
        webRequest.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        if (headers != null)
        {
            foreach (var header in headers)
            {
                webRequest.SetRequestHeader(header.Key, header.Value);
            }
        }



        webRequest.SendWebRequest();

        while (!webRequest.isDone) { }


        responseCode = webRequest.responseCode;



        if (webRequest.isHttpError || webRequest.isNetworkError)
        {
            callback("<neterror>" + webRequest.error);
            //callback(webRequest.responseCode + "-<neterror>" + webRequest.error);
        }

        else
        {
            
            if(string.IsNullOrEmpty(webRequest.downloadHandler.text))
            {
                callback("<nullorempty>" + "Error! server returned an empty response.");
                //callback(webRequest.responseCode + "-<nullorempty>" + "Error! server returned an empty response.");
            }

            else
            {
                callback(webRequest.downloadHandler.text);
            }

        }


    }






    /// <summary> POST form data asynchronously to the recipient specified in the baseUrl parameter.Optionally any headers can also be specified.Any response will be passed to the callback as a string. </summary> 

    public IEnumerator AsyncPOSTRequest(string baseUrl, Action<string> callback, byte[] data, Dictionary<string, string> headers = null)
    {


        UnityWebRequest webRequest = new UnityWebRequest(baseUrl);
        webRequest.timeout = timeout;
        webRequest.method = "POST";
        UploadHandlerRaw uploadHandler = new UploadHandlerRaw(data);
        DownloadHandlerBuffer downloadHandler = new DownloadHandlerBuffer();
        webRequest.uploadHandler = uploadHandler;
        webRequest.downloadHandler = downloadHandler;
        webRequest.SetRequestHeader("Content-Type", "application/x-www-form-urlencoded");


        if (headers != null)
        {
            foreach (var header in headers)
            {
                webRequest.SetRequestHeader(header.Key, header.Value);
            }
        }


        // This sometimes get stuck and clogs the main thread
        yield return webRequest.SendWebRequest();

        responseCode = webRequest.responseCode;


        if (webRequest.isHttpError || webRequest.isNetworkError)
        {
              callback("<neterror>" + webRequest.error);
            //callback(webRequest.responseCode + "-<neterror>" + webRequest.downloadHandler.text);
        }

        else
        {

            if (string.IsNullOrEmpty(webRequest.downloadHandler.text))
            {
                callback("<nullorempty>" + "Error! server returned an empty response.");
                //callback(webRequest.responseCode + "-<nullorempty>" + "Error! server returned an empty response.");
            }

            else
            {
                callback(webRequest.downloadHandler.text);
            }

        }


    }






    /// <summary> Download the image specified in the resource url parameter and returns the Texture2D to the callback if successful or null if failed to download. </summary> 

    public IEnumerator AsyncImageDownload(string resourceUrl, Action<Texture2D> callback)
    {

        Texture2D texture;
        texture = new Texture2D(4, 4, TextureFormat.DXT1, false);

        using (WWW www = new WWW(resourceUrl))
        {
        
            yield return www;

            if (www.error != null)
            {
                callback(null);   //"<neterror>" + www.error
            }

            else
            {
                while (!www.isDone) { yield return new WaitForEndOfFrame(); }
                www.LoadImageIntoTexture(texture);
                callback(texture);
            }
            
        }

    }






    /// <summary>Activate a target database given its name and deactivate all other databases.</summary>

    public void SwitchDatasetByName(string activateThisDataset)
    {

        ObjectTracker objectTracker   = TrackerManager.Instance.GetTracker<ObjectTracker>();
        


        if (objectTracker == null) 
        {
            StartCoroutine(LoadDatasetSafely(activateThisDataset)); 
            return;
        }


        Debug.Log("Trying to load  " + activateThisDataset);

        IEnumerable<DataSet> datasets = objectTracker.GetDataSets();
        IEnumerable<DataSet> activeDataSets = objectTracker.GetActiveDataSets();
        List<DataSet> activeDataSetsToBeRemoved = activeDataSets.ToList();


        foreach (DataSet dataset in activeDataSetsToBeRemoved)
        {
            var datasetSplit   = dataset.Path.Split('/');
            string dataSetName = datasetSplit[datasetSplit.Length-1].Split('.')[0];
            if(dataSetName.Equals("RecipeLoaders")) { continue; }    
            objectTracker.DeactivateDataSet(dataset); 
            Debug.Log("De-activated dataset named:  " + dataSetName);
        }


        
        objectTracker.Stop();                                       //Swapping of the datasets should not be done while the ObjectTracker is working at the same time.

        
        foreach (DataSet ds in datasets)
        {

            if (ds.Path.Contains(activateThisDataset))
            {
                objectTracker.ActivateDataSet(ds);
                Debug.Log("Activated dataset named:  "+ activateThisDataset);
            }

        }

        objectTracker.Start(); 
    }





    /// <summary>Tries to activate a target database given its name and deactivate all other databases.(This routine waits until Vuforia has been completely initialized before activating a dataset.This prevents null references.)</summary>

    private IEnumerator LoadDatasetSafely(string activateThisDataset)
    {

        while(TrackerManager.Instance.GetTracker<ObjectTracker>() == null) 
        {
            yield return new WaitForEndOfFrame();
        }

        SwitchDatasetByName(activateThisDataset);
    }




    
        /// <summary> Checks in an asynchronous manner whether the application currently has internet access.Passes true to the callback provided or false if internet connectivity doesn't exist.</summary>

        public IEnumerator AsyncInternetConnectivityCheck(Action<bool> callback)
        {

            bool responseRevieved = false;
            string encodedUrl = connectivityTestUrl;
            string response   = "";


            StartCoroutine(AsyncGETRequest(encodedUrl, (string res) =>
            {
                responseRevieved = true;
                response = res;
            }));


            while(!responseRevieved)
            {
                yield return new WaitForSeconds(0);
            }


            if (Regex.IsMatch(response, regexPatterns.netError, RegexOptions.Compiled))
            {
                Debug.Log("No internet connection or system services are down");
                internetConnectivityExists = false;
                if (callback != null) { callback(false); }
            }

            else
            {
                Debug.Log("Connection to the internet exists");
                internetConnectivityExists = true;
                if(callback != null) { callback(true); }
            }

        }
    





    /// <summary> Blocks the main thread and checks for internet connectivity.Returns true if connected, false otherwise.</summary>

    public bool BlockingInternetConnectivityCheck()
    {


        string encodedUrl = connectivityTestUrl;
        string response = "";

        BlockingGETRequest(encodedUrl, (string res) =>
        {
            response = res;
        });
       


        if (Regex.IsMatch(response, regexPatterns.netError, RegexOptions.Compiled))
        {
            //Debug.Log("No internet connection");
            internetConnectivityExists = false;
            return false;
        }

        else
        {
            //Debug.Log("Connection to the internet exists");
            internetConnectivityExists = true;
            return true;
        }

    }





    /// <summary> Runs a command(function) specified in command argument after the time specified in the dalay argument has passed.</summary>

    public void RunDelayedCommand(float delay, Action command)
    {
        StartCoroutine(RunCommandAfter(delay, command));
    }




    /// <summary> Runs a command(function) specified in command argument after returning the specified IEnumerator object.</summary>

    public void RunDelayedCommand(Action delay, Action command)
    {
        StartCoroutine(RunCommandAfter(delay, command));
    }





    /// <summary> The coroutine that actually runs delayed command when a call to RunDelayedCommand function is made .</summary>

    private IEnumerator RunCommandAfter(float delay, Action command)
    {
        yield return new WaitForSeconds(delay);
        command();
    }




    /// <summary> The coroutine that actually runs delayed command when a call to RunDelayedCommand function with the IEnumerator is made.</summary>

    private IEnumerator RunCommandAfter(Action delay, Action command)
    {
        yield return delay;
        command();
    }

    char[] charsToTrim = { 'O', 'R', '`', ' ', '\'', '=', '1', '-', ';' };


    /// <summary> This method escapes MYSQL special characters given a string.</summary>

    public string EscapeMYSQLString(string stringToEscape)
    {

        stringToEscape = stringToEscape.Replace("#" ,"\\#" );
        stringToEscape = stringToEscape.Replace("'", "\\'");
        stringToEscape = stringToEscape.Replace("\"", "\\\"");
        stringToEscape = stringToEscape.Replace("`", "\\`");
        stringToEscape = stringToEscape.Replace("-", "\\-");
        stringToEscape = stringToEscape.Replace("=", "\\=");
        stringToEscape = stringToEscape.Replace(";", "\\;");

        return stringToEscape;

    }





    public class HTTPMethod
    {

        public readonly string method;
        public HTTPMethod(HTTPMethods method) { this.method = Enum.GetName(typeof(HTTPMethods), method); }

        public enum HTTPMethods
        {
            GET,
            POST,
            PUT
        }

    }


}
