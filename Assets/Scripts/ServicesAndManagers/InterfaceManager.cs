﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.Events;

/// <summary>
/// This class does centralized managment for the user interface such as switching between UI screens and changing UI controls on different user actions.
/// </summary>

public class InterfaceManager : Singleton<InterfaceManager> {


    [Tooltip("Minimum distance between the left and right side of the ROI rect before it cannot be shrinked any further.")]
    [Range(0, 1)]
    public float roiHorShrinkThreshold;
    [Tooltip("Minimum distance between the top and bottom side of the ROI rect before it cannot be shrinked any further.")]
    [Range(0, 1)]
    public float roiVerShrinkThreshold;
    [Tooltip("The time for which the screen crossfade animation will play.")]
    public float screenFadeDuration;
    [Tooltip("Set reference to the unity main camera.")]
    public GameObject unityCamera;
    [Tooltip("Set reference to the AR main camera.")]
    public GameObject arCamera;
    [Tooltip("The time it will take to show or hide the video panel.")]
    public float videoPanelToggleTime;
    [Tooltip("The time it will take to switchbetween authored and unauthored recipes list.")]
    public float recipesListSwitchTime;
    [Tooltip("The time it will take to switch between recipes list view and the recipe viewer panel.")]
    public float recipesViewPanelSwitchTime;
    [Tooltip("The easing function to use for toggling the video panel")]
    public Ease videoPanelEasingFunction;
    [Tooltip("The easing function to use for switching between authored and unauthored recipes list")]
    public Ease recipesListEasingFunction;
    [Tooltip("The easing function to use for switching between a recipes list view and the recipe viewer panel.")]
    public Ease recipesViewPanelEasingFunction;
    [Tooltip("Define a tile skin by specifying colors for a UI tile elements in normal and pressed state")]
    public TileSkin[] tileSkins;



    /// <summary> The screen in which the application currently is.  </summary>
    [Tooltip("Identifies in which screen the application UI currently is.")]
    public UIStates.ScreenSpace currentScreenSpace;
    /// <summary> The sdefault screen orientation.  </summary>
    [Tooltip("Set the default orientation of the screen when the app launches.")]
    public ScreenOrientation defaultOrientation = ScreenOrientation.LandscapeLeft;


    ///<summary> Flag indicates whether the buffering indicator is currently active or not.  </summary>
    [HideInInspector]
    public bool bufferingIndicatorHidden = true;
    ///<summary> Flag indicates whether any loading indicator is currently active or not.</summary>
    [HideInInspector]
    public bool loadingIndicatorHidden = true;
    ///<summary> Indicates which UI panel is currently active.If no panel is active then this will be "UIStates.Panels.None". </summary>
    [HideInInspector]
    public UIStates.Panels currentActivePanel = UIStates.Panels.None;
    ///<summary> Indicates which UI panel is was active before changing to the current activePanel.If no panel is active then this will be "UIStates.Panels.None". </summary>
    [HideInInspector]
    public UIStates.Panels previousActivePanel = UIStates.Panels.None;
    ///<summary> Flag indicates that if the video panel is currently hidden or not.</summary>
    private bool videoPanelHidden = true;
    ///<summary> The original color of the uninteractable panel to cache.</summary>
    private Color? uninteractableOriginalColor = null;
    ///<summary> This tells the on screen messages coroutine to stop or run.</summary>
    private bool hideOnScreenMessage;
    /// <summary> The screen in which the application previously was.  </summary>
    [HideInInspector]
    public UIStates.ScreenSpace previousScreenSpace = UIStates.ScreenSpace.NoScreen;
    /// <summary> Flag indicates whether the modal window is currently active or not.  </summary>
    [HideInInspector]
    public bool isModalWindowActive = false;


    [System.Serializable]

    public struct TileSkin
    {

        [Tooltip("The color of the background of the tile in normal state.")]
        public Color normalTileBackgroundColor;
        [Tooltip("The color of the background of the tile in pressed state.")]
        public Color pressedTileBackgroundColor;
        [Tooltip("The color of the text components within a tile in normal state.")]
        public Color normalTextColor;
        [Tooltip("The color of the text components within a tile in pressed state.")]
        public Color pressedTextColor;
        [Tooltip("The color of the text underline components within a tile  in normal state.")]
        public Color normalTextUnderlineColor;
        [Tooltip("The color of the text underline components within a tile in pressed state.")]
        public Color pressedTextUnderlineColor;

    }




    /// <summary> This is a container only class and can be used to store the coordinates of the four corners of a RectTransform.</summary>

    public class RectangleBounds
    {
        ///<summary> The lower left corner of the RectTransform.</summary>
        public Vector2 lowerLeft;
        ///<summary> The lower right corner of the RectTransform.</summary>
        public Vector2 lowerRight;
        ///<summary> The upper left corner of the RectTransform.</summary>
        public Vector2 upperLeft;
        ///<summary> The upper right corner of the RectTransform.</summary>
        public Vector2 upperRight;

    }



    
    
    private void Start()
    {
        StartCoroutine(StopARCameraSafely());  //baw did
        GameObject arBackground = arCamera.transform.GetChild(0).gameObject;    
    }



    private void Update()
    {
        // Listen to the back button press on android
        
            if (Input.GetKeyDown(KeyCode.Escape) && !isModalWindowActive) { OnBackButtonPress(); }


        //if(EventSystem.current.currentSelectedGameObject) Debug.Log(EventSystem.current.currentSelectedGameObject.name);
    }





    /// <summary> Takes the appropriate action on when the user presses the back button on their device. </summary>

    public void OnBackButtonPress()
    {
        //Debug.Log("currentScreenSpace  " + currentScreenSpace + "  previousScreenSpace  " + previousScreenSpace);

        if (currentScreenSpace == UIStates.ScreenSpace.AudioPlayer)
        {
            GetComponent<AudioPlayer>().PauseMedia();

            if (previousScreenSpace == UIStates.ScreenSpace.ViewRecipes)
            {
                GoToScreen(UIStates.ScreenSpace.ViewRecipes);
            }
            if (previousScreenSpace == UIStates.ScreenSpace.ARScreen)
            {
                GoToScreen(UIStates.ScreenSpace.ARScreen);
            }
        }

        else if (currentScreenSpace == UIStates.ScreenSpace.CreateAndEditRecipes)
        {
            if (previousScreenSpace == UIStates.ScreenSpace.Home || previousScreenSpace == UIStates.ScreenSpace.UserRegistration)
            {
                GoToScreen(UIStates.ScreenSpace.Home);
            }
            else
            {
                GoToScreen(UIStates.ScreenSpace.ViewRecipes);
                if (currentActivePanel == UIStates.Panels.AuthoredRecipesListPanel)
                {
                    SystemServices.instance.RunDelayedCommand(screenFadeDuration , ()=> 
                    {
                        ChangeTileToSelected(UIRepository.instance.uiTiles.authoredRecipesTile);
                    });
                }

            }
        }

        else if (currentScreenSpace == UIStates.ScreenSpace.ViewRecipes)
        {
            if (currentActivePanel == UIStates.Panels.SearchRecipesPanel) { RecipeManager.instance.CloseSearchResults(); }
            else if (currentActivePanel == UIStates.Panels.RecipeViewer) { ToggleRecipeViewerPanel(null); }
            else { GoBackToPreviousScreen(); }
        }


        else if (currentScreenSpace == UIStates.ScreenSpace.UserRegistration) { GoToScreen(previousScreenSpace); }

        else { GoBackToPreviousScreen(); }

    }





    /// <summary> Returns the screen to which the screen specified in the given screen space argument can transit back to. </summary>

    public UIStates.Screen FindGoBackScreen(UIStates.ScreenSpace screenSpace)
    {


        foreach (UIStates.Screen screen in UIRepository.instance.screens)
        {
           
            if (screen.screenName == screenSpace)
            {
                return GetScreenFromScreenSpace(screen.backTransitScreen);
            }

        }

        return GetScreenFromScreenSpace(UIStates.ScreenSpace.NoScreen);

    }






    /// <summary> Returns the screen struct object of the screen space specified in the given screen space argument. </summary>

    public UIStates.Screen GetScreenFromScreenSpace(UIStates.ScreenSpace screenSpace)
    {

        foreach (UIStates.Screen screen in UIRepository.instance.screens)
        {
            if (screen.screenName == screenSpace) { return screen; }
        }

        return GetScreenFromScreenSpace(UIStates.ScreenSpace.NoScreen);

    }




    /// <summary> Displays or hides the ROI on the OCR canvas.Note that the gameObject is deactivated at the end of the frame, not immediately!. </summary>

    public void ShowRoi(bool show)

    {
        UIRepository.instance.ocrScreenElements.regionOfInterest.gameObject.SetActive(show);
    }






    /// <summary> Stretches or shrinks the ROI horizontally. </summary>

    public void ManipulateRoiHorizontally()

    {

        Vector3 pointerCurrentPos = Input.mousePosition;
        RectTransform transform = UIRepository.instance.ocrScreenElements.regionOfInterest.GetComponent<RectTransform>();

        RectTransform dragArea = UIRepository.instance.ocrScreenElements.regionOfInterest.GetComponent<DragableUI>().dragAreaInternal;

        if (!RectTransformUtility.RectangleContainsScreenPoint(dragArea , pointerCurrentPos)) { return; }

        Vector2 newAnchorMax = new Vector2(pointerCurrentPos.x / Screen.width, transform.anchorMax.y);
        float diff = newAnchorMax.x - transform.anchorMin.x;



        if (newAnchorMax.x < transform.anchorMax.x)
        {

            if (diff > roiHorShrinkThreshold)
            {
                transform.anchorMax = newAnchorMax;
            }

            else
            {
                newAnchorMax.x = transform.anchorMin.x + roiHorShrinkThreshold;
                transform.anchorMax = newAnchorMax;
            }

        }

        else
        {
             transform.anchorMax = newAnchorMax;
        }

        AnchorsToCorners(transform);

   
    }





    /// <summary> Stretches or shrinks the ROI vertically. </summary>

    public void ManipulateRoiVertically()

    {

        Vector3 pointerCurrentPos = Input.mousePosition;
        RectTransform transform = UIRepository.instance.ocrScreenElements.regionOfInterest.GetComponent<RectTransform>();

        RectTransform dragArea = UIRepository.instance.ocrScreenElements.regionOfInterest.GetComponent<DragableUI>().dragAreaInternal;

        if (!RectTransformUtility.RectangleContainsScreenPoint(dragArea, pointerCurrentPos)) { return; }

        Vector2 newAnchorMax = new Vector2(transform.anchorMax.x, (pointerCurrentPos.y) / Screen.height);
        float diff = newAnchorMax.y - transform.anchorMin.y;


        if (diff > roiVerShrinkThreshold)
        {
            transform.anchorMax = newAnchorMax;
        }

        else
        {
            newAnchorMax.y = transform.anchorMin.y + roiVerShrinkThreshold;
            transform.anchorMax = newAnchorMax;
        }


        AnchorsToCorners(transform);

    }




    /// <summary> Moves the anchors of a UI element to its corners. </summary>

    public static void AnchorsToCorners(RectTransform elementTransform)

    {

        //System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();

        // Get calling method name
        //Debug.Log("Caller of AnchorsToCorners() is  " +stackTrace.GetFrame(1).GetMethod().Name+"()");

        RectTransform childTransform = elementTransform;
        RectTransform parentTransform = childTransform.parent as RectTransform;

        if (childTransform == null || parentTransform == null) return;


        Vector2 newAnchorsMin = new Vector2(childTransform.anchorMin.x + childTransform.offsetMin.x / parentTransform.rect.width,
                                            childTransform.anchorMin.y + childTransform.offsetMin.y / parentTransform.rect.height);
        Vector2 newAnchorsMax = new Vector2(childTransform.anchorMax.x + childTransform.offsetMax.x / parentTransform.rect.width,
                                            childTransform.anchorMax.y + childTransform.offsetMax.y / parentTransform.rect.height);


        childTransform.anchorMin = newAnchorsMin;
        childTransform.anchorMax = newAnchorsMax;

        childTransform.offsetMin = childTransform.offsetMax = new Vector2(0, 0);


    }








    /// <summary> This method can return the pixel coordinates(Un-normalized) of the four corners of a RectTransform, independent of its anchors. </summary>

    public static RectangleBounds GetRectTransformCornerCoordinates(RectTransform elementTransform)

    {

        RectangleBounds rectangleBounds = new RectangleBounds();


        RectTransform childTransform = elementTransform;
        RectTransform parentTransform = childTransform.parent as RectTransform;


        if (childTransform == null || parentTransform == null) return null;


        Vector2 lowerLeftCorner = new Vector2(childTransform.anchorMin.x + childTransform.offsetMin.x / parentTransform.rect.width,
                                            childTransform.anchorMin.y + childTransform.offsetMin.y / parentTransform.rect.height);
        Vector2 upperRightCorner = new Vector2(childTransform.anchorMax.x + childTransform.offsetMax.x / parentTransform.rect.width,
                                            childTransform.anchorMax.y + childTransform.offsetMax.y / parentTransform.rect.height);


        rectangleBounds.lowerLeft = new Vector2(lowerLeftCorner.x * Screen.width, lowerLeftCorner.y * Screen.height);
        rectangleBounds.upperRight = new Vector2(upperRightCorner.x * Screen.width, upperRightCorner.y * Screen.height);
        rectangleBounds.lowerRight = new Vector2(rectangleBounds.upperRight.x, rectangleBounds.lowerLeft.y);
        rectangleBounds.upperLeft = new Vector2(rectangleBounds.lowerLeft.x, rectangleBounds.upperRight.y);


        return rectangleBounds;

    }









    /// <summary>
    /// Shows the specified message appended with increasing dots("...") after the time interval specified in the parameter.
    /// </summary>
    /// <param name="waitInterval"> The time in seconds after which each successive message is shown.</param>
    /// <param name="message">The message to show.</param>
    /// <param name="fadeAwayEffect">If set to true the message will not be appended with increasing dots and will just fade away after the wait interval specified.</param>
    /// <param name="messageBox">The UI Text in which the message will be shown</param>

    public void ShowMessage(float waitInterval, string message, bool hide = false, bool fadeAwayEffect = false)
    {
        UIRepository uiRepository = UIRepository.instance;

        if (hide)
        {
            uiRepository.onScreenIndicators.onScreenMessage.gameObject.SetActive(false);
            hideOnScreenMessage = true; return;
        }

        hideOnScreenMessage = false;
        uiRepository.onScreenIndicators.onScreenMessage.gameObject.SetActive(true);
        StartCoroutine(MessageRoutine(waitInterval, message, uiRepository.onScreenIndicators.onScreenMessage , fadeAwayEffect));
    }






    private IEnumerator MessageRoutine(float waitInterval, string message, Text messageBox, bool fadeAwayEffect)
    {


        if(fadeAwayEffect)
        {
            messageBox.text = message;
            yield return new WaitForSeconds(waitInterval);
            messageBox.CrossFadeAlpha(0.0f, 1f, false);
            while(messageBox.color != Color.clear) { yield return new WaitForSeconds(0); }
            messageBox.CrossFadeAlpha(1.0f, 0.0f, false);
            UIRepository.instance.onScreenIndicators.onScreenMessage.gameObject.SetActive(false);
            hideOnScreenMessage = true;
        }


        else
        {

            while (!hideOnScreenMessage)
            {

                messageBox.text = message;
                yield return new WaitForSeconds(waitInterval);
                messageBox.text = message + ".";
                yield return new WaitForSeconds(waitInterval);
                messageBox.text = message + "..";
                yield return new WaitForSeconds(waitInterval);
                messageBox.text = message + "...";
                yield return new WaitForSeconds(waitInterval);

            }

        }
        

    }





    ///<Summary> Goes back to the previous screen.The behaviour is controlled by the backward transit screen of the current screen.</Summary>

    public void GoBackToPreviousScreen()
    {
        
        UIRepository uiRepository = UIRepository.instance;
        UIStates.ScreenSpace backTransitScreen = UIStates.ScreenSpace.NoScreen;

        UIStates.Screen currentScreen = GetScreenFromScreenSpace(currentScreenSpace);
        backTransitScreen = currentScreen.backTransitScreen;


        if (currentScreen.screenName == UIStates.ScreenSpace.ViewRecipes)
        {
            RecipeManager.instance.lastSearchEntry = null;
        }

        if (backTransitScreen != UIStates.ScreenSpace.NoScreen && currentScreenSpace != UIStates.ScreenSpace.Home)
        {
            GoToScreen(backTransitScreen);
        }
        
        
    }





    ///<Summary> Goes to the screen given its index.This is just a callback method the actual screen transition is controlled by this method's overloaded version.</Summary>

    public void GoToScreen(int index)
    {

        UIRepository uiRepository = UIRepository.instance;
        UIStates.Screen currentScreen = GetScreenFromScreenSpace(currentScreenSpace);


        if (index == 0) { GoToScreen(UIStates.ScreenSpace.Home); }


        else if (index == 1)
        {
            GoToScreen(UIStates.ScreenSpace.ARScreen);
            videoPanelHidden = false;
            ToggleVideoControls();

        }


        else if (index == 2) { GoToScreen(UIStates.ScreenSpace.AudioPlayer); }


        else if (index == 3)
        {

            GoToScreen(UIStates.ScreenSpace.CreateAndEditRecipes);
            bool prepareForEditingScreen = RecipeManager.instance.prepareForEditingScreen;

            SystemServices.instance.RunDelayedCommand(screenFadeDuration  - 0.3f , ()=> 
            {
                if (!prepareForEditingScreen) { RecipeManager.instance.PrepareSharingScreen(); }
            });
            
        }


        else if (index == 4) { GoToScreen(UIStates.ScreenSpace.Dictionary); }


        else if (index == 5) { GoToScreen(UIStates.ScreenSpace.OptionsMenu); }


        else if (index == 6)
        {
            //GoToScreen(UIStates.ScreenSpace.RecipeViewerPanel);        
        }


        else if (index == 7) { GoToScreen(UIStates.ScreenSpace.OCR); }


        else if (index == 8) { GoToScreen(UIStates.ScreenSpace.UserRegistration); }


        else if (index == 9) { GoToScreen(UIStates.ScreenSpace.VideoPlayer); }


        else if (index == 10)
        {


            SystemServices.instance.RunDelayedCommand(screenFadeDuration, () =>
            {
                bool isUnauthoredTileSelected   = UIRepository.instance.uiTiles.unAuthoredRecipesTile.GetComponent<UITileInfo>().isSelected;
                bool isAuthoredTileSelected     = UIRepository.instance.uiTiles.authoredRecipesTile.GetComponent<UITileInfo>().isSelected;


                if(isUnauthoredTileSelected)
                {
                    ChangeTileToSelected(UIRepository.instance.uiTiles.unAuthoredRecipesTile);
                } 
                
                else if(isAuthoredTileSelected)
                {
                    ChangeTileToSelected(UIRepository.instance.uiTiles.authoredRecipesTile);
                }
                    

            });


            GoToScreen(UIStates.ScreenSpace.ViewRecipes);

        }


        else if (index == 11) { GoToScreen(UIStates.ScreenSpace.FoodInformatics); }


        else { Debug.LogWarning("No screen matching index  " + index); }



    }





    ///<Summary> Transitions to the screen given in the paramter.</Summary>

    private void GoToScreen(UIStates.ScreenSpace moveToScreen)
    {


        UIStates.Screen transitionToScreen;
        UIStates.Screen currentScreen;
        UIRepository uiRepository = UIRepository.instance;

        //Debug.Log("Current screen is  " + uiRepository.currentScreenSpace)


        if (moveToScreen == currentScreenSpace) { return; }



        transitionToScreen = GetScreenFromScreenSpace(moveToScreen);
        currentScreen = GetScreenFromScreenSpace(currentScreenSpace);


        Transform crossFade = currentScreen.crossFadePanel.transform;
        Color oldColor = crossFade.GetComponent<Image>().color;
        crossFade.GetComponent<Image>().color = new Color(oldColor.r, oldColor.g, oldColor.b, 255);

        currentScreen.screen.SetActive(false);
        transitionToScreen.screen.SetActive(true);

        crossFade = transitionToScreen.crossFadePanel.transform;
        oldColor = crossFade.GetComponent<Image>().color;
        crossFade.GetComponent<Image>().color = new Color(oldColor.r, oldColor.g, oldColor.b, 255);
        crossFade.GetComponent<Image>().CrossFadeAlpha(0, screenFadeDuration, true);

        previousScreenSpace = currentScreenSpace;
        currentScreenSpace  = transitionToScreen.screenName;



        GameObject arBackground = arCamera.transform.GetChild(0).gameObject;
        arBackground.SetActive(false);
        //arBackground.SetActive(true);  baw did now

        if (moveToScreen == UIStates.ScreenSpace.OCR)
        {
            //arBackground.GetComponent<MeshRenderer>().enabled = false;
            //arCamera.SetActive(true);             //baw did
            //unityCamera.SetActive(false);

            SystemServices.instance.RunDelayedCommand(screenFadeDuration - 0.2f, () =>
            {
                // Null texture if the StopArCameraSafely() function doesn't work as expected
                arBackground.SetActive(true); // baw did now
                UIRepository.instance.ocrScreenElements.cameraFeed.texture = CameraServices.instance.GetARCameraBackgroundTexture();
                arBackground.SetActive(false);
            });

        }


        else if (moveToScreen == UIStates.ScreenSpace.ARScreen || moveToScreen == UIStates.ScreenSpace.VideoPlayer)
        {

            SystemServices.instance.RunDelayedCommand(screenFadeDuration - 0.2f, () =>
            {
                //arCamera.SetActive(true);   
                //unityCamera.SetActive(false);    //baw did
                arBackground.SetActive(true); // baw did now
            });

            UIRepository.instance.ocrScreenElements.cameraFeed.texture = null;


        }

        else
        {

            //arCamera.SetActive(false);  //baw did
            //unityCamera.SetActive(true);
            UIRepository.instance.ocrScreenElements.cameraFeed.texture = null;
        }




        if (currentScreenSpace == UIStates.ScreenSpace.Dictionary || currentScreenSpace == UIStates.ScreenSpace.CreateAndEditRecipes)
        {
            //Debug.Log("Changing to portrait");
            GetComponent<WordHandler>().ResetOCRParameters();
            Screen.orientation = ScreenOrientation.Portrait;
        }

        else
        {
            //Debug.Log("Changing to default orientation");
            Screen.orientation = defaultOrientation;
        }

    }





    ///<Summary> Goes to the recipe viewer if it is not currently in view and informs it to load the given recipe in the argument.If the recipe viewer panel is already active then this function goes back to the view recipes list panel.</Summary>

    public void ToggleRecipeViewerPanel(UnityEngine.Object recipe)
    {


        UIRepository uiRepository = UIRepository.instance;
        Vector3 screenCenter = GetScreenFromScreenSpace(UIStates.ScreenSpace.ViewRecipes).screen.transform.localPosition;

        uiRepository.recipeViewerPanel.recipeViewerScrollPanel.SetActive(true);  //baw did
        

        // This means that the search panel is active

        if (currentActivePanel == UIStates.Panels.SearchRecipesPanel)
        {


                Transform recipeViewerPanel = uiRepository.recipeViewerPanel.recipeViewerPanel.transform;
                Transform searchPanel = uiRepository.recipeSearchElements.searchedRecipesScrollPanel.transform;


                recipeViewerPanel.gameObject.SetActive(true);
                UIRepository.instance.uiTiles.authoredRecipesTile.SetActive(false);
                UIRepository.instance.uiTiles.unAuthoredRecipesTile.SetActive(false);



                  uiRepository.recipeSearchElements.searchedRecipesScrollPanel.SetActive(false);

                  float gapY = Vector3.Distance(uiRepository.recipeViewerPanel.recipeViewerPanel.transform.position, screenCenter);

                  recipeViewerPanel.DOMoveY(screenCenter.y, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);
                  searchPanel.DOMoveY(screenCenter.y - gapY, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);

                  recipeViewerPanel.gameObject.SetActive(true);
                  UIRepository.instance.uiTiles.authoredRecipesTile.SetActive(false);
                  UIRepository.instance.uiTiles.unAuthoredRecipesTile.SetActive(false);

                  RecipeManager.instance.ViewRecipe(recipe);

                  // set the scroll panel to be at the top
                  uiRepository.recipeViewerPanel.recipeViewerScrollPanel.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;



                SystemServices.instance.RunDelayedCommand(recipesViewPanelSwitchTime, () =>
                {
                    AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerPanel.transform as RectTransform);
                    AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerScrollPanel.transform as RectTransform);
                    AnchorsToCorners(uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.GetComponent<RectTransform>());
                    AnchorsToCorners(uiRepository.recipeListViewer.authoredRecipesScrollPanel.GetComponent<RectTransform>());
                    AnchorsToCorners(uiRepository.recipeSearchElements.searchedRecipesScrollPanel.GetComponent<RectTransform>());
                    AnchorsToCorners(uiRepository.recipeSearchElements.searchedRecipesListView.GetComponent<RectTransform>());
                });




        } 



        else if (currentActivePanel == UIStates.Panels.UnauthoredRecipesListPanel || currentActivePanel == UIStates.Panels.AuthoredRecipesListPanel)
        {


            Transform recipeViewerPanel = uiRepository.recipeViewerPanel.recipeViewerPanel.transform;
            Transform searchPanel = uiRepository.recipeSearchElements.searchedRecipesScrollPanel.transform;


            recipeViewerPanel.gameObject.SetActive(true);
            UIRepository.instance.uiTiles.authoredRecipesTile.SetActive(false);
            UIRepository.instance.uiTiles.unAuthoredRecipesTile.SetActive(false);
            uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(false);
            uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(false);


            float gapY = Vector3.Distance(uiRepository.recipeViewerPanel.recipeViewerPanel.transform.position, screenCenter);

            recipeViewerPanel.DOMoveY(screenCenter.y, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);


            if (uiRepository.uiTiles.unAuthoredRecipesTile.GetComponent<UITileInfo>().isSelected)
            {
                Transform scrollPanel = uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.transform;
                scrollPanel.DOMoveY(screenCenter.y - gapY, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
                searchPanel.DOMoveY(screenCenter.y - gapY, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
            }

            else
            {
                Transform scrollPanel = uiRepository.recipeListViewer.authoredRecipesScrollPanel.transform;
                scrollPanel.DOMoveY(screenCenter.y - gapY, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
                searchPanel.DOMoveY(screenCenter.y - gapY, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
            }


            SystemServices.instance.RunDelayedCommand(recipesViewPanelSwitchTime, () =>
            {             
                AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerPanel.transform as RectTransform);
                AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerScrollPanel.transform as RectTransform);
                AnchorsToCorners(uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeListViewer.authoredRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeSearchElements.searchedRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeSearchElements.searchedRecipesListView.GetComponent<RectTransform>());
            });



            RecipeManager.instance.ViewRecipe(recipe);

            // set the scroll panel to be at the top
            uiRepository.recipeViewerPanel.recipeViewerScrollPanel.GetComponent<ScrollRect>().verticalNormalizedPosition = 1;

        }






        else if (currentActivePanel == UIStates.Panels.RecipeViewer)
        {

            
            Transform recipeViewerPanel = uiRepository.recipeViewerPanel.recipeViewerPanel.transform;
            Transform searchPanel = uiRepository.recipeSearchElements.searchedRecipesScrollPanel.transform;


            recipeViewerPanel.gameObject.SetActive(false);
            UIRepository.instance.uiTiles.authoredRecipesTile.SetActive(true);
            UIRepository.instance.uiTiles.unAuthoredRecipesTile.SetActive(true);



            if (previousActivePanel != UIStates.Panels.SearchRecipesPanel)
            {

                float gapY = 0;

                if (uiRepository.uiTiles.unAuthoredRecipesTile.GetComponent<UITileInfo>().isSelected)
                {
                    uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(true);
                    Transform scrollPanel = uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.transform;
                    gapY = Vector3.Distance(scrollPanel.position, screenCenter);
                    scrollPanel.DOMoveY(screenCenter.y, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
                    searchPanel.DOMoveY(screenCenter.y, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
                    recipeViewerPanel.DOMoveY(screenCenter.y + gapY, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);
                }

                else
                {
                    uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(true);
                    Transform scrollPanel = uiRepository.recipeListViewer.authoredRecipesScrollPanel.transform;
                    gapY = Vector3.Distance(scrollPanel.position, screenCenter);
                    scrollPanel.DOMoveY(screenCenter.y, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
                    searchPanel.DOMoveY(screenCenter.y, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
                    recipeViewerPanel.DOMoveY(screenCenter.y + gapY, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);
                }

            }


            else if (previousActivePanel == UIStates.Panels.SearchRecipesPanel)
            {

                float gapY = 0;
                uiRepository.recipeSearchElements.searchedRecipesScrollPanel.SetActive(true);
                gapY = Vector3.Distance(searchPanel.position, screenCenter);
                searchPanel.DOMoveY(screenCenter.y, recipesViewPanelSwitchTime).SetEase(recipesViewPanelEasingFunction).SetDelay(0);
                recipeViewerPanel.DOMoveY(screenCenter.y + gapY, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);


                SystemServices.instance.RunDelayedCommand(recipesViewPanelSwitchTime, () =>
                {
                    uiRepository.recipeViewerPanel.recipeViewerScrollPanel.SetActive(false);
                });

            }




            SystemServices.instance.RunDelayedCommand(recipesViewPanelSwitchTime, () =>
            {
                AnchorsToCorners(uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeListViewer.authoredRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerPanel.transform as RectTransform);
                AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerScrollPanel.transform as RectTransform);
                AnchorsToCorners(uiRepository.recipeSearchElements.searchedRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeSearchElements.searchedRecipesListView.GetComponent<RectTransform>());

            });

        }

  

        UpdateCurrentActivePanel();
  

    }




    /// <summary>Tries to deactivate vuforia ARCamera when it fully initializes and loads the datasets.</summary>

    private IEnumerator StopARCameraSafely()
    {
       
        while (Vuforia.TrackerManager.Instance.GetTracker<Vuforia.ObjectTracker>() == null || RecipeManager.instance.isRecipeLoading)
        {
            yield return new WaitForEndOfFrame();
        }

        GameObject arBackground = arCamera.transform.GetChild(0).gameObject;
        arBackground.SetActive(false); // baw did now

        //yield return new WaitForSeconds(2f);
        // problem here

        //arCamera.SetActive(false);
        //unityCamera.SetActive(true);

    }




    /// <summary> Toggles between showing and hiding the video controls based on its current state of visibility. </summary>

    public void ToggleVideoControls()
    {

        UIRepository uiRepository = UIRepository.instance;
        RectTransform controlsPanel = uiRepository.videoControls.controlsPanel.GetComponent<RectTransform>();
        Transform toMove = uiRepository.videoControls.hidePanelButton.transform;
        Vector3 moveTo;

        if(RectTransformUtility.RectangleContainsScreenPoint(controlsPanel , Input.mousePosition))
        {
            if(!RectTransformUtility.RectangleContainsScreenPoint(toMove.GetComponent<RectTransform>(), Input.mousePosition))
            {
                return;
            }          
        }
        

        if (videoPanelHidden)
        {
            moveTo = uiRepository.videoControls.showPositionMarker.transform.position;
            videoPanelHidden = false;
        }

        else
        {
            moveTo = uiRepository.videoControls.hidePositionMarker.transform.position;
            videoPanelHidden = true;
        }


        toMove.DOMoveY(moveTo.y, videoPanelToggleTime).SetEase(videoPanelEasingFunction).SetDelay(0);

    }





    /// <summary> Completely show or hide the video controls based on the argument passed.</summary>

    public void ShowVideoControls(bool show)
    {

        UIRepository uiRepository = UIRepository.instance;
        GameObject toHide = uiRepository.videoControls.hidePanelButton.gameObject;



        if (show)
        {
            toHide.SetActive(true);
            videoPanelHidden = true;
            ToggleVideoControls();
        }

        else
        {

            videoPanelHidden = false;
            ToggleVideoControls();

            SystemServices.instance.RunDelayedCommand(videoPanelToggleTime, () =>
            {
                toHide.SetActive(false);

            });

        }

    }





    /// <summary> Show or hide the specified loading indicator based on the argument passed. The parameter "isBufferingIndicator" indicates whether this indication is for video buffering or not.
    /// <para> The optional parameter "isElementIndicator" should be set to true if the loading indicator is for a certain/specific UI element. </para>
    /// </summary>

    public void ShowOrHideLoadingIndicator(bool show, Animator indicator, bool isBufferingIndicator , bool isElementIndicator = false)
    {

        Animator loadingIndicator = indicator;

        if (show)
        {
            loadingIndicator.gameObject.SetActive(true);
            loadingIndicator.Play("loading", -1 , 0);
            if (isBufferingIndicator)    { bufferingIndicatorHidden = false; }
            else if(!isElementIndicator) { loadingIndicatorHidden = false; }
        }

        else
        {
            loadingIndicator.SetBool("isLoading", false);
            loadingIndicator.gameObject.SetActive(false);
            if (isBufferingIndicator)     { bufferingIndicatorHidden = true; }
            else if (!isElementIndicator) { loadingIndicatorHidden = true; }
        }
    }





    ///<summary> Marks a tile as selected and changes a tile appearance based on the skin index specified in the UITileInfo component attached to this gameobject.</summary>

    public void ChangeTileToSelected(UnityEngine.Object tile)
    {

        
        GameObject tileClicked = (GameObject)tile;
        UITileInfo tileInfo = tileClicked.GetComponent<UITileInfo>();
        UIRepository uiRepository = UIRepository.instance;

        if (tileInfo == null) { Debug.LogWarning("Won't change tile's appearance as there is no tileInfo component attached."); return; }


        if (tileInfo.tile == UITileInfo.TileTypes.unAuthoredRecipesTile)
        {

            if (tileInfo.isSelected) { return; }

            RecipeManager.instance.lastSearchEntry = null;
            RecipeManager.instance.CloseSearchResults();


            ChangeTileAppearance(uiRepository.uiTiles.authoredRecipesTile, true);
            MarkTileUnselected(uiRepository.uiTiles.authoredRecipesTile);
            ChangeTileAppearance(tileClicked, false);


            Vector3 screenCenter = GetScreenFromScreenSpace(UIStates.ScreenSpace.ViewRecipes).screen.transform.position;

            float gapX = Vector3.Distance(uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.transform.position, screenCenter);
            Transform scrollPanel = uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.transform;
            scrollPanel.DOMoveX(screenCenter.x, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);

            scrollPanel = uiRepository.recipeListViewer.authoredRecipesScrollPanel.transform;
            scrollPanel.DOMoveX(screenCenter.x + gapX, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);




            //uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(true);
            //uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(true);   //baw did
            uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(true);
            uiRepository.recipeViewerPanel.recipeViewerScrollPanel.SetActive(false);
            uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(false);   


            SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
            {
                AnchorsToCorners(uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeListViewer.authoredRecipesScrollPanel.GetComponent<RectTransform>());
                uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(true);
                uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(false);          //baw did
            });


            MarkTileSelected(tileClicked);

            RecipeManager recipeManager = RecipeManager.instance;

            // Unauthored recipes list is not already populated , either because of internet failure or because there were no recipes at all.
            if (recipeManager.unauthoredRecipesLoadedCount == 0)
            {

                // No unauthored recipes were loaded at all because of internet failure, so try to load them again.
                if (recipeManager.totalunauthoredRecipes == -1)
                {
    
                    SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
                    {
                        ToggleCurrentScreenInteractable(false);
                        recipeManager.FetchSuccessiveRecipes(false);
                    });
                }


                // No unauthored recipes were loaded at all because there are no unauthored recipes in the database, so just show the user appropriate dialog and no need to try to load unauthored recipes.
                else if (recipeManager.totalunauthoredRecipes == 0)
                {
                    Debug.LogWarning("Show modal window for no unauthored recipes found in database");
                    
                    Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;

                    SystemServices.instance.RunDelayedCommand(recipesListSwitchTime + 0.25f, ()=> 
                    {

                      ShowModalWindow("No recipes found", icon, "There are no recipes shared by users.Do you want to share the first one?.", true, "Yes", "No",
                      () => { GoToScreen(3); } // Go to the Create recipes screen    
                     ,() => { }
                      );

                    });
                    
                }


                // There are unauthored recipes in the database so load them.
                else
                {

                    SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
                    {
                        ToggleCurrentScreenInteractable(false);
                        recipeManager.FetchSuccessiveRecipes(false);
                    });
                }

            }


            // Unauthored recipes list is already populated with some recipes, don't load more until the user presses the load more button.
            else
            {

                /* Debug.Log("Unauthored recipes list is already populated, won't populate it again");*/
                Color color = Color.white;
                color.a = 0;
                ToggleCurrentScreenInteractable(false , color);

                SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () => 
                {
                    ToggleCurrentScreenInteractable(true);
                });

            }


        }


        if (tileInfo.tile == UITileInfo.TileTypes.authoredRecipesTile)
        {



            if (tileInfo.isSelected && !RecipeManager.instance.newRecipeShared) {  return; }
  

            RecipeManager.instance.lastSearchEntry = null;
            RecipeManager.instance.CloseSearchResults();


            ChangeTileAppearance(uiRepository.uiTiles.unAuthoredRecipesTile, true);
            MarkTileUnselected(uiRepository.uiTiles.unAuthoredRecipesTile);
            ChangeTileAppearance(tileClicked, false);



            Vector3 screenCenter = GetScreenFromScreenSpace(UIStates.ScreenSpace.ViewRecipes).screen.transform.position;

            float gapX = Vector3.Distance(uiRepository.recipeListViewer.authoredRecipesScrollPanel.transform.position, screenCenter);
            Transform scrollPanel = uiRepository.recipeListViewer.authoredRecipesScrollPanel.transform;
            scrollPanel.DOMoveX(screenCenter.x, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);

            scrollPanel = uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.transform;

            if(gapX > 0.05f)
            {
                scrollPanel.DOMoveX(screenCenter.x - gapX, recipesListSwitchTime).SetEase(recipesListEasingFunction).SetDelay(0);
            }


            //uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(true);
            //uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(true);   //baw did
            //uiRepository.recipeViewerPanel.recipeViewerScrollPanel.SetActive(false);
            uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(true);
            uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(false);
            uiRepository.recipeViewerPanel.recipeViewerScrollPanel.SetActive(false);


            SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
            {
                AnchorsToCorners(uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.GetComponent<RectTransform>());
                AnchorsToCorners(uiRepository.recipeListViewer.authoredRecipesScrollPanel.GetComponent<RectTransform>());
                uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(true);          //baw did
                uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(false);
            });


            MarkTileSelected(tileClicked);


            RecipeManager recipeManager = RecipeManager.instance;


            // Authored recipes list is not already populated , either because of internet failure or because there were no authored recipes at all or the user is unregistered.
            if (recipeManager.authoredRecipesLoadedCount == 0)
            {
                

                // No authored recipes were loaded at all because of internet failure, or the user is unregistered so try to load them again.      
                if (recipeManager.totalAuthoredRecipes == -1)
                {
                    SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
                    {
                        ToggleCurrentScreenInteractable(false);
                        recipeManager.FetchSuccessiveRecipes(true);
                    });
                }

                // User has shared a new recipe so recount the total authored recipes and reload the authored recipe into the authored recipes panel
                else if (RecipeManager.instance.newRecipeShared)
                {
                    recipeManager.totalAuthoredRecipes = -1;              // Recount total authored recipes

                    SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
                    {
                        ToggleCurrentScreenInteractable(false);
                        recipeManager.FetchSuccessiveRecipes(true);
                    });
                }

                // No authored recipes were loaded at all because there are no authored recipes in the database, so just show the user appropriate dialog and no need to try to load authored recipes.
                else if (recipeManager.totalAuthoredRecipes == 0)
                {
                    Debug.LogWarning("Show modal window for no authored recipes found in the database");
                    
                    Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;

                    SystemServices.instance.RunDelayedCommand(recipesListSwitchTime + 0.25f, () =>
                    {

                       ShowModalWindow("No authored recipes found", icon, "You don't have any recipes shared.Do you want to share a recipe?.", true, "Yes", "No",
                        () => { GoToScreen(3); } // Go to the Create recipes screen    
                       ,() => { }
                        );

                    });
           
                }


                // There are authored recipes in the database so load them.
                else
                {
                    ToggleCurrentScreenInteractable();

                    SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
                    {
                        ToggleCurrentScreenInteractable(false);
                        recipeManager.FetchSuccessiveRecipes(true);
                    });
                }
            }


            // Authored recipes list is already populated with some recipes, don't load more until the user presses the load more button.
            else
            {
                /*Debug.Log("Authored recipes list is already populated, won't populate it again");*/
                Color color = Color.white;
                color.a = 0;
                ToggleCurrentScreenInteractable(false, color);

                SystemServices.instance.RunDelayedCommand(recipesListSwitchTime, () =>
                {
                    ToggleCurrentScreenInteractable(true);
                });
            }

        }




        if (tileInfo.tile == UITileInfo.TileTypes.dictionaryTile) { }

        if (tileInfo.tile == UITileInfo.TileTypes.recipeTile) { }

        if (tileInfo.tile == UITileInfo.TileTypes.shareRecipesTile) { }

        if (tileInfo.tile == UITileInfo.TileTypes.startCookingTile) { }

        if (tileInfo.tile == UITileInfo.TileTypes.viewRecipesTile) { }

        UpdateCurrentActivePanel();

    }





    ///<summary> Marks a tile as unselected.</summary>

    public void MarkTileUnselected(GameObject tile)
    {
        UITileInfo tileInfo = tile.GetComponent<UITileInfo>();

        if (tileInfo == null) { Debug.LogWarning("The given tile has no \"TileInfo\" component attached."); }

        else { tileInfo.isSelected = false; }
    }




    ///<summary> Marks a tile as selected.</summary>

    public void MarkTileSelected(GameObject tile)
    {
        UITileInfo tileInfo = tile.GetComponent<UITileInfo>();

        if (tileInfo == null) { Debug.LogWarning("The given tile has no \"TileInfo\" component attached."); }

        else { tileInfo.isSelected = true; }
    }





    ///<summary> This method changes the tile specified and its elements colors either back to the original color or changed color. </summary>

    private void ChangeTileAppearance(GameObject tileClicked, bool changeToOriginal)
    {


           UITileInfo tileInfo = tileClicked.GetComponent<UITileInfo>();
          
           
           
           TileSkin skin = tileSkins[tileInfo.skinIndex];


            foreach (CanvasRenderer child in tileClicked.GetComponentsInChildren<CanvasRenderer>())
            {

               
               

            
               if(child.CompareTag("TileBackground"))
               {
                    Button tileButton = tileClicked.GetComponent<Button>();
                    ColorBlock colorBlock = tileButton.colors;
             
                    colorBlock.normalColor = changeToOriginal? skin.normalTileBackgroundColor : skin.pressedTileBackgroundColor;
                    tileButton.colors = colorBlock;           
                }


                else if (child.CompareTag("TileText"))
                {
                    Text tileText = child.GetComponent<Text>();
                    tileText.color = changeToOriginal? skin.normalTextColor : skin.pressedTextColor;   
                }


                else if (child.CompareTag("TileTextUnderline"))
                {
                    Image tileTextUnderline = child.GetComponent<Image>();
                    tileTextUnderline.color = changeToOriginal ? skin.normalTextUnderlineColor : skin.pressedTextUnderlineColor;
                }

            }

    }






    /// <summary> Toggle between making the current active screen interactable or uninteractable. If the optional "forceInteractable" argument is provided then the toggling behaviour will be overriden by this argument.Optionally you can also provide a color to use for the uninteractable panel.
    /// <para>  Note that if the "forceInteractable" argument is provided as true then the current screen will forcefully be made interactable and if it is provided as false it will forcefully be made uninteractable.</para>
    /// </summary>

    public void ToggleCurrentScreenInteractable(bool? forceInteractable = null, Color? color = null)
    {

        UIStates.Screen screen = GetScreenFromScreenSpace(currentScreenSpace);
        GameObject uninteractableScreen = screen.unInteractablePanel;

       
        if (color != null)
        {
            uninteractableOriginalColor = uninteractableScreen.GetComponent<Image>().color;
            uninteractableScreen.GetComponent<Image>().color = new Color(color.Value.r , color.Value.g , color.Value.b , color.Value.a);
        }


        if(forceInteractable != null)
        {

           if(forceInteractable == true)
           {
                uninteractableScreen.SetActive(false);

                if (uninteractableOriginalColor != null)
                {
                    uninteractableScreen.GetComponent<Image>().color = new Color(uninteractableOriginalColor.Value.r, uninteractableOriginalColor.Value.g, uninteractableOriginalColor.Value.b, uninteractableOriginalColor.Value.a);
                }
           }

           else
           {
                uninteractableScreen.SetActive(true); 
           }

        } 


        else if(uninteractableScreen.activeSelf)
        {
            uninteractableScreen.SetActive(false); 

            if (uninteractableOriginalColor != null)
            {
                uninteractableScreen.GetComponent<Image>().color = new Color(uninteractableOriginalColor.Value.r, uninteractableOriginalColor.Value.g, uninteractableOriginalColor.Value.b, uninteractableOriginalColor.Value.a);
            }
        }

        else
        {
            uninteractableScreen.SetActive(true); 
        }


    }




    /// <summary> Sets the appropriate active panel according to the current interface conditions.</summary>

    public void UpdateCurrentActivePanel()
    {


        UIRepository uiRepository  = UIRepository.instance;

        bool isUnauthoredTileSelected = uiRepository.uiTiles.unAuthoredRecipesTile.GetComponent<UITileInfo>().isSelected;
        bool isAuthoredTileSelected   = uiRepository.uiTiles.authoredRecipesTile.GetComponent<UITileInfo>().isSelected;

        ///previousActivePanel = currentActivePanel;

        if (uiRepository.recipeViewerPanel.recipeViewerPanel.activeSelf)
        {
            previousActivePanel = currentActivePanel;
            currentActivePanel = UIStates.Panels.RecipeViewer;        
        }

        else if (uiRepository.recipeSearchElements.searchedRecipesScrollPanel.activeSelf) 
        {
            previousActivePanel = currentActivePanel;
            currentActivePanel = UIStates.Panels.SearchRecipesPanel;
        }


        else if (isUnauthoredTileSelected)
        {
            previousActivePanel = currentActivePanel;
            currentActivePanel = UIStates.Panels.UnauthoredRecipesListPanel;
        }

        else if (isAuthoredTileSelected)
        {
            previousActivePanel = currentActivePanel;
            currentActivePanel = UIStates.Panels.AuthoredRecipesListPanel;
        }

        else
        {
            previousActivePanel = currentActivePanel;
            currentActivePanel = UIStates.Panels.None;
        }

        //Debug.Log("Current active panel becomes  " + currentActivePanel + " prevwas  "+previousActivePanel);


        System.Diagnostics.StackTrace stackTrace = new System.Diagnostics.StackTrace();
            // Get calling method name
            //Debug.Log("Caller is  " + stackTrace.GetFrame(1).GetMethod().Name+"()");
   

    }



    /// <summary> Recalculate the preffered width and height of a the specified UI element.</summary>

    public void RecalculatePrefferedDimentions(GameObject uiElement)
    {
        LayoutElement layoutElement = uiElement.GetComponent<LayoutElement>();
        RectTransform rectTransform = uiElement.GetComponent<RectTransform>();
        Transform transform = UIRepository.instance.onScreenIndicators.onScreenIndicatorsCanvas.transform;

        if (layoutElement)
        {
            Transform parent = rectTransform.parent;
            rectTransform.SetParent(transform, false);   // A recTransform rect always gives 0 values for width and height if it is not an indirect or direct child of a canvas.So parent this to some rectTransform first.
            layoutElement.preferredHeight = rectTransform.rect.height;
            layoutElement.preferredWidth  = rectTransform.rect.width;

            if(parent)
            {
                rectTransform.SetParent(parent, false);  // If the UIElement was already parented to some other element then retain it's previous parent.
            }
        }

        else
        {
            Debug.LogWarning($"Will not recalculate preffered dimentions.The UI-Element \"{uiElement.name}\" does not have a layoutElement attached.");
        }

    }





    /// <summary> Shows a modal window with a title, icon, message , positive and negative buttons and a close button.The parameter "makeScreenUninteractable" indicates whether to make the current screen uninteractable when the modal window is shown.
    /// <para>    The optional parameter "crossButtonAction" if passed a method then the cross button executes that method otherwise the cross button closes the modal window.Note that whenever a positive or a negative button is clicked the modal window will get closed automatically.</para>
    /// </summary>

    public void ShowModalWindow(string title , Sprite icon , string message , bool makeScreenUninteractable, string postiveButtonText , string negativeButtonText , Action positiveAction , Action negativeAction, Action crossButtonAction = null)
    {

        UIRepository.ModalWindowsElements modalWindowElements = UIRepository.instance.modalWindowElements;
        RemoveListenersOnModalWindow();


        modalWindowElements.title.text       = title.ToUpper();
        modalWindowElements.iconLabel.sprite = icon;
        modalWindowElements.message.text     = message;

        modalWindowElements.positiveButton.gameObject.SetActive(true);
        modalWindowElements.negativeButton.gameObject.SetActive(true);

        modalWindowElements.positiveButton.GetComponentInChildren<Text>().text = postiveButtonText;
        modalWindowElements.negativeButton.GetComponentInChildren<Text>().text = negativeButtonText;
        modalWindowElements.positiveButton.onClick.AddListener(() => { positiveAction(); CloseModalWindow(); });
        modalWindowElements.negativeButton.onClick.AddListener(() => { negativeAction(); CloseModalWindow(); });

        if (crossButtonAction == null) { modalWindowElements.closeWindowButton.onClick.AddListener(() => { CloseModalWindow(); }); }
        else { modalWindowElements.closeWindowButton.onClick.AddListener(() => { crossButtonAction(); }); }

        if (makeScreenUninteractable) { modalWindowElements.uninteractablePanel.gameObject.SetActive(true); }
        modalWindowElements.window.SetActive(true);

        isModalWindowActive = true;

    }




    /// <summary> Shows a modal window with a title, icon, message and a cross button..The parameter "makeScreenUninteractable" indicates whether to make the current screen uninteractable when the modal window is shown.
    /// <para>    The optional parameter "crossButtonAction" if passed a method then the cross button executes that method otherwise the cross button closes the modal window.Note that whenever a positive or a negative button is clicked the modal window will get closed automatically.</para>    
    /// /// </summary>

    public void ShowModalWindow(string title, Sprite icon, string message, bool makeScreenUninteractable, Action crossButtonAction = null)
    {

        UIRepository.ModalWindowsElements modalWindowElements = UIRepository.instance.modalWindowElements;
        RemoveListenersOnModalWindow();


        modalWindowElements.title.text = title.ToUpper();
        modalWindowElements.iconLabel.sprite = icon;
        modalWindowElements.message.text = message;
        modalWindowElements.positiveButton.gameObject.SetActive(false);
        modalWindowElements.negativeButton.gameObject.SetActive(false);


        if (crossButtonAction == null) { modalWindowElements.closeWindowButton.onClick.AddListener(() => { CloseModalWindow(); }); }
        else { modalWindowElements.closeWindowButton.onClick.AddListener(() => { crossButtonAction(); }); }


        if (makeScreenUninteractable) { modalWindowElements.uninteractablePanel.gameObject.SetActive(true); }
        modalWindowElements.window.SetActive(true);

        isModalWindowActive = true;

    }





    /// <summary> Removes all onClick listeners on the modal window.</summary>

    private void RemoveListenersOnModalWindow()
    {
        GameObject modalWindowElements = UIRepository.instance.modalWindowElements.window;
        Button[] buttons = modalWindowElements.GetComponentsInChildren<Button>(true);

        foreach(Button button in buttons) { button.onClick.RemoveAllListeners(); }
    }




    /// <summary> Closes the modal window.</summary>

    public void CloseModalWindow()
    {
        UIRepository.instance.modalWindowElements.uninteractablePanel.gameObject.SetActive(false);
        UIRepository.instance.modalWindowElements.window.SetActive(false);

        isModalWindowActive = false;
    }
    






    protected override void OnAwake()
    {
    }

}
