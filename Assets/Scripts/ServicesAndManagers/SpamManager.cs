﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;


/// <summary>
/// This class serves and initializes background services that manage user spam.
/// </summary>

public class SpamManager : Singleton<SpamManager>
{





    private int recipeID;
                                               ///<summary> Indicates the upper limit for the number of spams reported against the user before the system starts to warn him. </summary>
    public int warningThreshold;
                                               ///<summary> Indicates the upper limit for the number of spams reported against the user before the user gets banned. </summary>
    public int blockThreshold;    
                                               ///<summary> The number of spams this user did. Note that this value is updated once at the application startup. </summary>
    private int spamCount = -1;





    private void Start()
    {
        CheckSpam();
    }




    ///<summary> Reports spam against the recipe provided in the method argument.
    ///<para> If the spam was successfully reported the callback provided is passed with true, in case of failure to report spam it's passed false and if the user already reported spam against the recipe the callback is passed null.</para>
    ///</summary>

    public void ReportSpam(Recipe recipe , Action<bool?> callback)
    {
        string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ".000000"; // MYSQL date format '2018-09-12 00:00:00.000000'

        SystemServices systemServices = SystemServices.instance;
        WWWForm form = new WWWForm();
        form.AddField("function", "ReportSpam");
        form.AddField("recipeId", recipe.id);
        form.AddField("date", date);
        form.AddField("reportingDevice", SystemServices.instance.deviceUniqueIdentifier);
        //Debug.Log("Trying to report spam for recipeId: "+ recipe.id +  "  on date:  "+ date + "  using deviceId:  "+ SystemServices.instance.deviceUniqueIdentifier);


        Animator loadingAnim = UIRepository.instance.onScreenIndicators.ringLoading;


        InterfaceManager.instance.ShowMessage(0.6f, "PLEASE WAIT", false);
        InterfaceManager.instance.ToggleCurrentScreenInteractable(false);

        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) => 
        
        {
            InterfaceManager interfaceManager = InterfaceManager.instance;

            interfaceManager.ShowMessage(0.6f, "PLEASE WAIT", true);
            interfaceManager.ToggleCurrentScreenInteractable(true);

            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                callback(false);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.apiMistmatch))
            {
                Debug.Log(response.Split('+')[1]);
                callback(false);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
            {
                Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                interfaceManager.ShowModalWindow("Cannot report spam", icon, "You have already reported spam against this recipe, or the recipe has been deleted.", true);
                callback(null);
            }
   
            else
            {
                Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                InterfaceManager.instance.ShowModalWindow("Success", icon, "You have successfully reported spam against this recipe.", true);
                callback(true);
            }
        }
        , form.data));

    }



    ///<summary> Displays a warning message to the user when a certain number of spams have been reported against him. </summary>

    public void DisplayWarning()
    {
        Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
        InterfaceManager.instance.ShowModalWindow("Spams warning", icon, spamCount + " spams have been reported against you, beware of your activities you might get blocked.", true);
    }



    ///<summary> Blocks the device user from further using the application.Calls the given method in the callback paramter with true if user is successfully blocked, false if failed to blocked user or user isn't registered and null if internet failure occurs. </summary>

    private void BlockUser(Action<bool?> callback)
    {

        string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ".000000"; // MYSQL date format '2018-09-12 00:00:00.000000'

        SystemServices systemServices = SystemServices.instance;
        WWWForm form = new WWWForm();

        form.AddField("function", "BlockUser");
        form.AddField("deviceId", SystemServices.instance.deviceUniqueIdentifier);


        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

        {


            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                Debug.Log("Net error cant block user.");
                callback(null);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.apiMistmatch))
            {
                Debug.Log(response.Split('+')[1]);
                callback(false);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
            {
                Debug.Log("The user is either not registered or already blocked.");
                callback(false);
            }

            else
            {
                Debug.Log("User successfully blocked.");
                callback(true);
            }
        }
        , form.data));

    }





    ///<summary> Takes the appropriate action against the number of spams done by a user. </summary>

    private void CheckSpam()
    {


        SystemServices systemServices = SystemServices.instance;

        WWWForm form = new WWWForm();

        form.AddField("function", "CountSpams");
        form.AddField("deviceId", SystemServices.instance.deviceUniqueIdentifier);


        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

        {


            if (PlayerPrefs.HasKey("spamCount"))
            {
                spamCount = PlayerPrefs.GetInt("spamCount");
            }


            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                Debug.LogWarning("Net error can't check for spams.");
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.apiMistmatch))
            {
                Debug.LogWarning(response.Split('+')[1]);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
            {
                Debug.LogWarning(response.Split('+')[1]);
            }

            else
            {

                bool flag;
                int result;

                flag      = int.TryParse(response, out result);
                spamCount = flag ? result : spamCount;

                //Debug.Log("Successfully counted spams.");
                PlayerPrefs.SetInt("spamCount", spamCount);
                PlayerPrefs.Save();
            }




            if (spamCount >= warningThreshold && spamCount < blockThreshold)
            {
                DisplayWarning();
                //Debug.Log("Display warning");

            }

            else if (spamCount >= blockThreshold)
            {

                BlockUser((bool? didSucceed) =>
                {

                    if (didSucceed == null) { }

                    else if (didSucceed == false) { }

                    else
                    {
                        Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                        InterfaceManager.instance.ShowModalWindow("User blocked", icon, "You have been blocked to use our services.If you think this is a mistake contact us at saba@gmail.com", true, ()=> { });
                        SystemServices.instance.RunDelayedCommand(7 , ()=> { Application.Quit(); });
                    }

                });
                //Debug.Log("Block user");
            }


        }
        , form.data));


    }








    protected override void OnAwake()
    {

    }


}
