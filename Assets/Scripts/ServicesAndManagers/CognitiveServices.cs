﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>This class provides access to cognitive services like OCR, Entity search , Image search and Image classification </summary>

public class CognitiveServices : Singleton<CognitiveServices> {


    /// <summary> The BING search services subscription key. </summary>
    const string bingSearchKey = "477d590adb544fc48f5bf874d1e12a36";

    /// <summary> The BING search uri base. </summary>
    const string bingSearchUriBase = "https://api.cognitive.microsoft.com/bing/v7.0";

    /// <summary> The microsoft cognitive computer vision services subscription key. </summary>
    const string compVisionKey = "9624e070d9d34fe281e9e656ae3bd83d";

    /// <summary> The computer vision search uri base. </summary>
    const string compVisionUriBase = "https://westcentralus.api.cognitive.microsoft.com/vision/v1.0";

    /// <summary> The microsoft cognitive content moderator services subscription key. </summary>
    const string contentModKey = "57af7d53494c430487712c7b9a617bd0";

    /// <summary> The content moderator search uri base. </summary>
    const string contentModUriBase = "https://eastasia.api.cognitive.microsoft.com/contentmoderator/moderate/v1.0";
    public Texture2D tex;



    /// <summary> Uploads the texture provided in "textureToDecode parameter to the online service for OCR.The callback is passed the text detected if ocr succeeded, ""(empty) if failed to detect any text and null if internet or the system failed. </summary>

    public IEnumerator CarryOnlineOCR(Texture2D textureToDecode, Action<string> callback)
    {
        //textureToDecode = tex;
        if (!textureToDecode) { Debug.LogError("Warning!, textureToDecode field is null."); callback(null); yield return null; }
        
        string ocrBaseUrl = compVisionUriBase + "/ocr?language=en&detectOrientation=true";

        SystemServices systemServices = GetComponent<SystemServices>();


        byte[] data = textureToDecode.EncodeToJPG();


        Dictionary < string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "application/octet-stream");
        headers.Add("Ocp-Apim-Subscription-Key", compVisionKey);
 

        StartCoroutine(systemServices.AsyncPOSTRequest(ocrBaseUrl, (string response) =>
        {
         
            // This error can be due to other reasons too for example invalid image size or format etc
            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                Debug.LogWarning(netError);
                callback(null);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
            {
                Debug.LogWarning("Null or empty response from the server.");
                callback(null);
            }


            else
            {
                //Debug.Log("OCR response is   "+response);
                response = Regex.Replace(response, @"\t|\n|\r", "");
                response = Regex.Replace(response, @"\s+", " ");

                Match detectedText = Regex.Match(response, "\"text\":\"(.+?)\"", RegexOptions.Compiled);

                if(detectedText.Success) { callback(detectedText.Result("$1").Trim()); }
                else { callback(""); }
                
            }

        }
        , data , headers));
        

    }





    /// <summary> Returns the text recognized in the provided custom texture argument.Note that this is a blocking call </summary>

    public string CarryOfflineOCR(Texture2D image)
    {
        string word = SystemServices.instance.tesseract.RecognizeFromTexture(image, false);
        Regex.Replace(word, @"\t|\n|\r", "").Trim();
        return word;
    }







    /// <summary> Searches for the links to the most relevant images matching the search term given in the argument.Calls the provided callback with a string array of links if successful or null if failed.</summary>>

    public void GetImagesAgainstQuery(string searchTerm , Action<string[]> callback)
    {


        SystemServices systemServices = SystemServices.instance;

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Ocp-Apim-Subscription-Key", bingSearchKey);

        string encodedUrl = bingSearchUriBase + "/images/search" + "?q=" + Uri.EscapeDataString(searchTerm);


        StartCoroutine(systemServices.AsyncGETRequest(encodedUrl, (string response) =>

        {
            
            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                Debug.LogWarning(netError);
                callback(null);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
            {
                Debug.LogWarning("Null or empty response from the server.");
                callback(null);
            }

            else
            {

                response = Regex.Replace(response, @"\t|\n|\r", "");
                response = Regex.Replace(response, @"\s+", " ");

                MatchCollection thumbnailUrl = Regex.Matches(response, "\"thumbnailUrl\": \"(.+?)\"", RegexOptions.Compiled);
                string[] imageLinks = new string[thumbnailUrl.Count];


                if(thumbnailUrl.Count == 0) { callback(null); }
               
                else
                {

                    for(int a = 0; a < thumbnailUrl.Count; a++ )
                    {
                        string url = thumbnailUrl[a].Result("$1");
                        url = Regex.Replace(url, @"\/", "");
                        url = Regex.Replace(url, @"\\", "/");
                        imageLinks[a] = url;
                       //if (a == 2) { break; }   // Need only three links to the images
                    }

                    callback(imageLinks);

                }


            } 


     } , headers));


    }





    /// <summary> Analyzes the text given for any inappropriate content.Calls the method in the callback with true if the text is inappropriate, false if the text is appropriate and null if failed to analyze text. 
    /// <para>    The parameter "percentageThreshold" defines the threshold percentage above which the content will be deemed inappropriate</para>
    /// </summary>

    public IEnumerator TextModeration(string text, float percentageThreshold, Action<bool?> callback)
    {

        if (string.IsNullOrWhiteSpace(text)) { Debug.LogError("Warning!, Given null or empty text to analyze."); callback(null); yield return null; }

        string contentModBaseUrl = contentModUriBase + "/ProcessText/Screen?autocorrect=true&classify=true&language=eng";

        SystemServices systemServices = GetComponent<SystemServices>();
        byte[] data = Encoding.UTF8.GetBytes(text);

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Content-Type", "text/plain");
        headers.Add("Ocp-Apim-Subscription-Key", contentModKey);


        StartCoroutine(systemServices.AsyncPOSTRequest(contentModBaseUrl, (string response) =>
        {


            
            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                Debug.LogWarning(netError);
                callback(null);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
            {
                Debug.LogWarning("Null or empty response from the server.");
                callback(null);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
            {
                string[] splitResponse = response.Split('+');
                Debug.LogWarning(splitResponse[1]);
                callback(null);
            }

            
            else
            {
                bool isOffensive = false;

                response = Regex.Replace(response, @"\t|\n|\r", "");
                response = Regex.Replace(response, @"\s+", " ");

                Match detectedText          = Regex.Match(response, "\"text\":\"(.+?)\"", RegexOptions.Compiled);
                MatchCollection categoryScores  = Regex.Matches(response , "\"Score\":(.+?)\\}", RegexOptions.Compiled);

                foreach(Match scoreMatch in categoryScores)
                {
                    float score = float.Parse(scoreMatch.Result("$1"));

                    if(score > (percentageThreshold / 100f)) { isOffensive = true; }
                }

                if(isOffensive) { callback(true); }
                else { callback(false); }

            }
            
        }
        , data, headers));


    }


    protected override void OnAwake()
    {
    }


        
}
