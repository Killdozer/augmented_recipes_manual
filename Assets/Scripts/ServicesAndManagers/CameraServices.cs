﻿using UnityEngine;
using System.Collections;
using Vuforia;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


/// <summary> This class contains methods to provide and change device camera services.</summary>

public class CameraServices : Singleton<CameraServices>
{

                                                          ///<summary> The focus mode to request from the the camera device.</summary>
    public CameraFocusMode cameraFocusMode;
                                                          ///<summary> The pixel format for the camera frames.</summary>
    public Vuforia.Image.PIXEL_FORMAT pixelFormat;
                                                          ///<summary> Indicates whether the camera frame format be set at startup or not.</summary>
    [Tooltip("Should the camera frame format be set at startup or not.")] 
    [SerializeField]                                               
    private bool setFrameFormat;
                                                          ///<summary> The camera focus mode to be used.</summary>
    private CameraDevice.FocusMode focusMode;
   
                                                          ///<summary> Indicates if the TakePhoto coroutine is busy capturing the photo or not.</summary>
    public bool isPhotoCapturing { get; private set; }



    public enum CameraFocusMode
    {
        Default,
        ContinuousAutoFocus,
        Infinity,
        Macro,
        Normal  
    }



    [Serializable]
    public struct Resolution
    {
        public int width;
        public int height;

        public Resolution(int width , int height)
        {
            this.width  = width;
            this.height = height;
        }

        public static bool operator == (Resolution r1, Resolution r2)
        {
            return (r1.width == r2.width && r1.height == r2.height);
        }

        public static bool operator != (Resolution r1, Resolution r2)
        {
            return !(r1.width == r2.width && r1.height == r2.height);
        }
    }




    void Start()
    {
        
        
        switch(cameraFocusMode)
        {


            case CameraFocusMode.ContinuousAutoFocus:
                focusMode = CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO;
                break;

            case CameraFocusMode.Infinity:
                focusMode = CameraDevice.FocusMode.FOCUS_MODE_INFINITY;
                break;

            case CameraFocusMode.Macro:
                focusMode = CameraDevice.FocusMode.FOCUS_MODE_MACRO;
                break;

            case CameraFocusMode.Normal:
                focusMode = CameraDevice.FocusMode.FOCUS_MODE_NORMAL;
                break;

        }



        if(Application.platform == RuntimePlatform.Android)
        {
            // LAG MIGHT BE RELATED TO THE GRAPHICS API VERSION USED OR WITH THE PARAMETER "USER 32BIT DISPLAY BUFFER)

            //pixelFormat = Vuforia.Image.PIXEL_FORMAT.RGB888;                 // Severe Lag
            //pixelFormat = Vuforia.Image.PIXEL_FORMAT.UNKNOWN_FORMAT;       // no lag but returns null camera image
            //pixelFormat = Vuforia.Image.PIXEL_FORMAT.RGB565;               // Little lag but causes vuforia error on copyTexture
            //pixelFormat = Vuforia.Image.PIXEL_FORMAT.GRAYSCALE;            // Causes almost no lag, and no errors + the image size is very small, but OCR won't work on a gray scale image.          

            if (pixelFormat == Vuforia.Image.PIXEL_FORMAT.RGBA8888) { pixelFormat = Vuforia.Image.PIXEL_FORMAT.RGB888; }

        }


        VuforiaARController vuforia = VuforiaARController.Instance;
        vuforia.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        vuforia.RegisterOnPauseCallback(OnPaused);

    }





    private void OnVuforiaStarted()

    {


     if(setFrameFormat) {
           
        //Camera tax!  
        if (!CameraDevice.Instance.SetFrameFormat(pixelFormat, true))
        {
                        Debug.LogError(
            "Failed to register pixel format " + pixelFormat.ToString() +
            "\n the format may be unsupported by your device;" +
            "\n consider using a different pixel format.");
        }

        else
        {
                Debug.Log("Successfully set the frame format to  " + pixelFormat.ToString());
        }

                this.setFrameFormat = false;

                       }

       

        if (cameraFocusMode == CameraFocusMode.Default) { Debug.Log("Default focus mode"); return; }



        bool focusModeSet = CameraDevice.Instance.SetFocusMode(focusMode);


        if (!focusModeSet)
        {
            focusMode = CameraDevice.FocusMode.FOCUS_MODE_MACRO;
            focusModeSet = CameraDevice.Instance.SetFocusMode(focusMode);

            if(!focusModeSet)
            {
                focusMode = CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO;
                focusModeSet = CameraDevice.Instance.SetFocusMode(focusMode);


                if (!focusModeSet)
                {
                    focusMode = CameraDevice.FocusMode.FOCUS_MODE_NORMAL;
                    focusModeSet = CameraDevice.Instance.SetFocusMode(focusMode);
                }

            }

        }

    }





    private void OnPaused(bool paused)
    {

        if (!paused) 
        {
            if(cameraFocusMode == CameraFocusMode.Default) { return; }
            CameraDevice.Instance.SetFocusMode(focusMode);
        }

        else
        {
            if (!CameraDevice.Instance.SetFrameFormat(pixelFormat, false)) { Debug.LogError("Unable to unregister pixel format");}
        }

    }






    /// <summary> Registers or unregisters the device camera frame format.</summary>

    public void RegisterUnregisterFrameFormat(bool register)
    {
        if (register)
        {
            if (!CameraDevice.Instance.SetFrameFormat(pixelFormat, true)) { Debug.LogError("Unable to register pixel format"); }
        }

        else
        {
            if (!CameraDevice.Instance.SetFrameFormat(pixelFormat, false)) { Debug.LogError("Unable to unregister pixel format"); }
        }

    }





    /// <summary> Restarts the device camera.Additionally if setFrameFormat is true the camera frame format will also be changed.</summary>

    public void RestartCamera(bool setFrameFormat)
    {
        this.setFrameFormat = setFrameFormat;
        VuforiaARController vuforia = VuforiaARController.Instance;
        CameraDevice cameraDevice = Vuforia.CameraDevice.Instance;   
        

        VuforiaBehaviour.Instance.enabled = false;
        VuforiaBehaviour.Instance.enabled = true;



        //cameraDevice.Stop();
        //cameraDevice.Deinit();
        //cameraDevice.Init();
        //cameraDevice.Start();
    }




    /// <summary> Starts the device camera.</summary>

    public void StartCamera()
    {

        CameraDevice cameraDevice = Vuforia.CameraDevice.Instance;
        
        if (cameraDevice.IsActive()) { return; }
        cameraDevice.Start();
        cameraDevice.Init();

    }





    /// <summary> Stops the device camera.</summary>

    public void StopCamera()
    {

        CameraDevice cameraDevice = Vuforia.CameraDevice.Instance;

        if (!cameraDevice.IsActive()) { return; }

        cameraDevice.Stop();
        cameraDevice.Deinit();

    }




    /// <summary> Changes the camera focus mode to the specified mode.</summary>

    public void ChangeCameraFocus(CameraDevice.FocusMode cameraFocusMode)
    {
        this.focusMode = cameraFocusMode;
        bool focusModeSet = CameraDevice.Instance.SetFocusMode(cameraFocusMode);
        //RestartCamera();
    }





    /// <summary> Captures photo from the device camera and returns it as a Texture2D.If the optional parameter "captureAgainOnNull" is true the method tries to capture the image again until it is not null.Please note that this is an asynchronous call.Calls the given callback in the argument with the captured image as Texture2D.</summary>

    public IEnumerator TakePhoto(Action<Texture2D> callback , bool captureAgainOnNull = false)
    {

        isPhotoCapturing = true;

        RegisterUnregisterFrameFormat(true);

        CameraDevice cameraDevice = CameraDevice.Instance;
        Texture2D image = new Texture2D(1,1);
        Vuforia.Image vufImage;

        vufImage = cameraDevice.GetCameraImage(pixelFormat);

        if(vufImage != null)
        {
            image = new Texture2D(vufImage.Width, vufImage.Height);
            vufImage.CopyToTexture(image);                                          // The image returned is both flipped sideways and rotated by 180 deg.
        }


        // After setting the frame format the camera image might not be readily available so try again if it's null

        else if (captureAgainOnNull)
        {

            for (int a = 0; a < 10; a++)
            {
                yield return new WaitForEndOfFrame();

                vufImage = cameraDevice.GetCameraImage(pixelFormat);

                if (vufImage != null)
                {
                    image = new Texture2D(vufImage.Width, vufImage.Height);
                    vufImage.CopyToTexture(image);                                          // The image returned is both flipped sideways and rotated by 180 deg.
                    break;
                }

            }

        }



        //Color32[] colors = image.GetPixels32();
        //System.Array.Reverse(colors, 0, colors.Length);
        //image.SetPixels32(colors);
        //image.Apply();
        //image = FlipTexture(image);
        //if (!CameraDevice.Instance.SetFrameFormat(pixelFormat, false)) { Debug.LogError("Unable to unregister pixel format"); return null; }


        RegisterUnregisterFrameFormat(false);

        isPhotoCapturing = false;
        callback(image);

    }







    /// <summary>
    ///  Crops the part of the Texture provided in the argument that comes within the ROI.(Please note that the Texture must be shown correctly so that that user can't see the flipped and rotated effects.Otherwise the algorithm will provide incorrect results.)
    /// </summary>
    /// <param name="toCrop">The texture to crop.</param>
    /// <param name="photoPanel"> The panel on which the texture is applied.</param>
    /// <param name="isFlipped"> Pass true in this argument if the image given is flipped sideways.</param>
    /// <param name="isInverted">Pass true in this argument if the image given is inverted / rotated by 180 deg.</param>
    /// <param name="minDimentions">Pass the minimum dimensions that the cropped image must have,the method then appends gray pixels to increase the dimentions or pass min dimensions as (0,0) to keep the original size of the cropped image.</param>
    /// <returns>The bytes of the cropped texture.</returns>

        
    public Texture2D CropPhoto(Texture2D toCrop, RectTransform photoPanel, Resolution minDimentions, bool isFlipped = false, bool isInverted = false)
    {

        
        RectTransform roiRect = UIRepository.instance.ocrScreenElements.regionOfInterest.GetComponent<RectTransform>();


        InterfaceManager.RectangleBounds roiBounds = InterfaceManager.GetRectTransformCornerCoordinates(roiRect);
        InterfaceManager.RectangleBounds photoTBounds = InterfaceManager.GetRectTransformCornerCoordinates(photoPanel);



        if (roiBounds.lowerLeft.x < photoTBounds.lowerLeft.x || roiBounds.lowerLeft.y < photoTBounds.lowerLeft.y)
        {
            Debug.LogWarning("Too near the edge or out of bounds!"); return null;
        }

        if (roiBounds.upperRight.x > photoTBounds.upperRight.x || roiBounds.upperRight.y > photoTBounds.upperRight.y)
        {
            Debug.LogWarning("Too near the edge or out of bounds!"); return null;
        }




        float xFactor = toCrop.width / photoPanel.rect.width;
        float yFactor = toCrop.height / photoPanel.rect.height;

        int x = (int)Mathf.Round(((roiBounds.lowerLeft.x - photoTBounds.lowerLeft.x) * xFactor));
        int y = (int)Mathf.Round(((roiBounds.lowerLeft.y - photoTBounds.lowerLeft.y) * yFactor));
        int width = (int)(Mathf.Round((roiBounds.lowerRight.x - roiBounds.lowerLeft.x) * xFactor));
        int height = (int)(Mathf.Round((roiBounds.upperRight.y - roiBounds.lowerRight.y) * yFactor));




        //Texture2D tempTexture = new Texture2D(width, height);
        //Texture2D tempTexture = new Texture2D(width, height , TextureFormat.Alpha8 , false);
        //tempTexture.SetPixels(toCrop.GetPixels(x, y, width, height));
        Texture2D tempTexture;


        Resolution correctedDimensions = new Resolution();
        correctedDimensions.width  = width;
        correctedDimensions.height = height;

        Resolution zeroRes = new Resolution();

        if (minDimentions != zeroRes)
        {
            
            if(minDimentions.width  > width)  { correctedDimensions.width  = minDimentions.width;  }
            if(minDimentions.height > height) { correctedDimensions.height = minDimentions.height; }
        }


         if (pixelFormat == Vuforia.Image.PIXEL_FORMAT.GRAYSCALE) { tempTexture = new Texture2D(correctedDimensions.width, correctedDimensions.height, TextureFormat.Alpha8, false); }
         else { tempTexture = new Texture2D(correctedDimensions.width, correctedDimensions.height); }
        



        int rWidth = x + width;
        int rHeight = y + height;
        Color pixelColor = new Color();



        for (int a = y; a < rHeight; a++)
        {

            for (int b = x; b < rWidth; b++)
            {

                     if (isFlipped  && isInverted)  { pixelColor = toCrop.GetPixel(b, (toCrop.height - 1) - a );  }

                else if(!isInverted && isFlipped )  { pixelColor = toCrop.GetPixel((toCrop.width - 1) - b, a); }

                else if(isInverted  && !isFlipped)  { pixelColor = toCrop.GetPixel((toCrop.width - 1) - b, (toCrop.height - 1) - a); }

                else if(!isInverted && !isFlipped)  { pixelColor = toCrop.GetPixel(b, a); }

                tempTexture.SetPixel(b - x, a - y, pixelColor);

            }

        }



        tempTexture.Apply();
        
        return tempTexture;
  
    }


   



    /// <summary> Returns a reference to the ARCamera's background texture.</summary>

    public Texture GetARCameraBackgroundTexture()
    {
        GameObject arCamera = InterfaceManager.instance.arCamera;
        GameObject arBackground = arCamera.transform.GetChild(0).gameObject;
        return arBackground.GetComponent<MeshRenderer>().material.mainTexture;
    }




    /// <summary> Flips a texture sideways by default or upside down if the given argument is true.</summary>

    private Texture2D FlipTexture(Texture2D original, bool upSideDown = false)
    {

        Texture2D flipped = new Texture2D(original.width, original.height);

        int width  = original.width;
        int height = original.height;
     

        for (int col = 0; col < width; col++)
        {
            for (int row = 0; row < height; row++)
            {
                if (upSideDown)
                {
                    flipped.SetPixel(row, (width - 1) - col, original.GetPixel(row, col));
                }
                else
                {
                    flipped.SetPixel((width - 1) - col, row, original.GetPixel(col, row));
                }
            }
        }

        flipped.Apply();

        return flipped;
    }





    /// <summary>Triggers a single camera focus operation.</summary>
    
    public void FocusCamera()
    {
        CameraDevice camera = CameraDevice.Instance;
        if (camera != null) { camera.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO); }
    }



    protected override void OnAwake()
    {
        
    }
}