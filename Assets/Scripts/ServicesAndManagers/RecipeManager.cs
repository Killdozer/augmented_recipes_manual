﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;
using Vuforia;

/// <summary>
/// Class contains behaviours for dealing with users recipes. 
/// </summary>

public class RecipeManager : Singleton<RecipeManager>{




    [Tooltip("Populate with the preloaded recipes in sequential order")]
                                                           ///<summary> The list of preloaded recipes in order. </summary>
    public GameObject[] preloadedRecipes;
                                                           ///<summary> The preloaded recipe that has currently been loaded into the system. </summary>
    public Recipe activeRecipe { set;  get; }

    [HideInInspector]
                                                           ///<summary> Flag that tells if a recipe is actively being loaded.</summary>
    public bool isRecipeLoading;
                                                           ///<summary> The list of loaded recipes shared by other users.</summary>
    private Dictionary<int, Recipe> unauthoredRecipes;
                                                           ///<summary> The list of loaded recipes authored by this user.</summary>
    private Dictionary<int, Recipe> authoredRecipes  ;

    [HideInInspector]
    public bool isBackgroundBuffering;

                                                           /// <summary> Indicates how many unauthored recipes have been fetched from the database and loaded into the interface. </summary>
    [HideInInspector]
    public uint unauthoredRecipesLoadedCount;
                                                           /// <summary> Indicates how many unauthored recipes to fetch from the database at each call to FetchSuccessiveOtherUsersRecipes() . </summary>
    [Tooltip("Set how many unauthored recipes to fetch from the database at each call to FetchSuccessiveOtherUsersRecipes()")]
    [SerializeField]
    private uint unauthoredRecipesFetchThreshold;

                                                           /// <summary> Indicates how many authored recipes have been fetched from the database and loaded into the interface. </summary>
    [HideInInspector]
    public uint authoredRecipesLoadedCount;
                                                           /// <summary> Indicates how many authored recipes to fetch from the database at each call to FetchSuccessiveOtherUsersRecipes() . </summary>
    [Tooltip("Set how many authored recipes to fetch from the database at each call to FetchSuccessiveAuthoredRecipes()")]
    [SerializeField]
    private uint authoredRecipesFetchThreshold;
      
                                                           /// <summary> The total number of authored recipes in the database for this user.If the system failed to get the value then it will be -1. </summary>
    [HideInInspector]
    public int totalAuthoredRecipes = -1;
                                                           /// <summary> The total number of recipes in the database excluding this users recipes.If the system failed to get the value then it will be -1. </summary>
    [HideInInspector]
    public int totalunauthoredRecipes = -1;

                                                           /// <summary> The last recipe that the user viewed in the recipe viewer panel.</summary>
    [HideInInspector]
    public Recipe lastViewedRecipe;

                                                           /// <summary> Defines how much similarity should exist between the user searched recipe title and the title of recipe in the database to deem it as a search result. </summary>
    [Tooltip("Set the percentage similarity that should exist between the user searched recipe title and the title of recipe in the database to deem it as a search result.")]
    [Range(10f , 100f)]
    public float searchSimilarityPercentage;

                                                           /// <summary> Defines the percentage threshold above which a recipe content is considered inappropriate. </summary>
    [Tooltip("Set the percentage threshold above which a recipe content is considered inappropriate.")]
    [Range(10f , 100f)]
    public float indecentContentThreshold;
                                                           /// <summary> The last search entry that the user made for a recipe. </summary>
    [HideInInspector]
    public string lastSearchEntry;
                                                           /// <summary> Flag indicates whether to move the recipes list view anchors to corners when the lists have been added with new content or not. </summary>
    private bool updateListsAnchors;
                                                           /// <summary> Flag indicates to the interface manager whether to open the create recipe screen for creating a recipe or for editing a recipe. </summary>
    [HideInInspector]
    public bool prepareForEditingScreen;
                                                           /// <summary> Defines the minimum number of ingredients that must exist in a recipe before it can be shared.</summary>
    [Tooltip("Set the minimum number of ingredients that must exist in a recipe before it can be shared.")]
    public int minIngredientsCount;
                                                           /// <summary> Defines the minimum number of instructions that must exist in a recipe before it can be shared.</summary>
    [Tooltip("Set the minimum number of instructions that must exist in a recipe before it can be shared.")]
    public int minInstructionsCount;

    private bool firstRecipeLoad = true;

    /// <summary> Flag indicates whether the user has shared a new recipe or not, so that we should refresh the authored recipes viewing panel.</summary>
    public bool newRecipeShared { private set; get; }


    private void Start()
    {


        SystemServices systemServices = SystemServices.instance;

        if (preloadedRecipes == null || preloadedRecipes.Length == 0)
        {
            Debug.LogError("The list of preloaded recipes is empty, please populate it.");
        }

        CountTotalAuthoredRecipes  ((bool? didSucceed) =>  
        {
            if(didSucceed == null || didSucceed == false) { Debug.LogWarning("Failed to count total number of authored recipes"); }        
        } );
        CountTotalunauthoredRecipes((bool? didSucceed) =>  
        {
            if(didSucceed == null || didSucceed == false) { Debug.LogWarning("Failed to count total number of authored recipes"); }
        });


        

        LoadPreloadedRecipe(preloadedRecipes[0].name);                          // Load the first recipe when the application starts.


        InterfaceManager interfaceManager = InterfaceManager.instance;
        
        GameObject gameObject = Instantiate(UIRepository.instance.usefulPrefabs.topGapPrefab);
        interfaceManager.RecalculatePrefferedDimentions(gameObject); //baw added
        gameObject.transform.SetParent(UIRepository.instance.recipeListViewer.authoredRecipesListView.transform, false);
        
        gameObject = Instantiate(UIRepository.instance.usefulPrefabs.topGapPrefab);
        interfaceManager.RecalculatePrefferedDimentions(gameObject); //baw added
        gameObject.transform.SetParent(UIRepository.instance.recipeListViewer.unauthoredRecipesListView.transform, false);

        gameObject = Instantiate(UIRepository.instance.usefulPrefabs.topGapPrefab);
        interfaceManager.RecalculatePrefferedDimentions(gameObject); //baw added
        gameObject.transform.SetParent(UIRepository.instance.recipeSearchElements.searchedRecipesListView.transform, false);

        gameObject = Instantiate(UIRepository.instance.recipeViewerPanel.recipeHeaderPrefab);
        interfaceManager.RecalculatePrefferedDimentions(gameObject); //baw added
        gameObject.transform.SetParent(UIRepository.instance.recipeViewerPanel.recipeViewerListView.transform, false);
        updateListsAnchors = true;

    }




    private void OnGUI()
    {

        // Move the Recipes lists anchors to corners when they are resized with the addition of new content
        if (Event.current.type == EventType.Layout && updateListsAnchors)
        {

            // will be executed only after layout has been re-computed

            // AnchorsToCorners can cause NaN and infinities on disabled UIElements so temporary enable them before doing AnchorsToCorners.
            var screen = InterfaceManager.instance.GetScreenFromScreenSpace(UIStates.ScreenSpace.ViewRecipes);
            bool prevActiveState = screen.screen.activeSelf;

            screen.screen.SetActive(true);

            InterfaceManager.AnchorsToCorners(UIRepository.instance.recipeListViewer.authoredRecipesListView.GetComponent<RectTransform>());
            InterfaceManager.AnchorsToCorners(UIRepository.instance.recipeListViewer.unauthoredRecipesListView.GetComponent<RectTransform>());
            InterfaceManager.AnchorsToCorners(UIRepository.instance.recipeSearchElements.searchedRecipesScrollPanel.GetComponent<RectTransform>());
            InterfaceManager.AnchorsToCorners(UIRepository.instance.recipeSearchElements.searchedRecipesListView.GetComponent<RectTransform>());
            updateListsAnchors = false;

            screen.screen.SetActive(prevActiveState);
        }
    }




    /// <summary> Compiles user's custom recipe procedure into a new recipe object and assigns it internally to the active recipe object. </summary>

    public void CompileRecipe()
    {

        // should be replaced with appropriate logic when implemented.
        throw new NotImplementedException();

    }



    /// <summary> Returns true if the active recipe being managed by this manager object contains some inappropriate content otherwise returns false.</summary> 

    public bool CheckInAppropriateContent(string toAnalyze)
    {

        // should be replaced with appropriate logic when implemented.
        throw new NotImplementedException();

    }



    /// <summary> Shares the recipe in creational process. </summary>

    public void ShareRecipe()
    {


        InterfaceManager interfaceManager = InterfaceManager.instance;
        UIRepository uiRepository         = UIRepository.instance;
        SystemServices systemServices     = SystemServices.instance;
        User user                         = User.instance;
        Animator loading                  = uiRepository.onScreenIndicators.boxLoading; 


        interfaceManager.ToggleCurrentScreenInteractable();
        interfaceManager.ShowOrHideLoadingIndicator(true , loading , false);




        user.InitializeUserInfo((bool? didSucceed) =>
        {

            if(didSucceed == null)
            {
                Sprite icon = uiRepository.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
            }


            else if(didSucceed == false)
            {

                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;

                interfaceManager.ShowModalWindow("Unregistered User", icon, "You need to register yourself to use all of our services, do you want to register now?", true, "Yes", "No",
                  () => { interfaceManager.GoToScreen(8); } // Go to the registrations screen     
                , () => { }
                );

                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);

            }


            else
            {


                Transform recipeListView = uiRepository.shareAndEditRecipeElements.recipeListView.transform;

                InputField[] inputFields = recipeListView.GetComponentsInChildren<InputField>();

                string title = inputFields[0].text;
                string prepTime = inputFields[1].text;
                string sharedBy = User.instance.userInfo.name;
                string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ".000000"; // MYSQL date format '2018-09-12 00:00:00.000000'
                string deviceId = SystemInfo.deviceUniqueIdentifier;
                string instructions = "";
                string ingredients = "";
                bool emptyFields = false;
                int ingredientsCount = 0;
                int instructionsCount = 0;



                for (int a = 0; a < inputFields.Length; a++)
                {

                    InputField field = inputFields[a];

                    if (field.CompareTag("Ingredients"))
                    {
                        if (!string.IsNullOrWhiteSpace(field.text))
                        {
                            ingredientsCount++;
                            ingredients += field.text + "<#>";
                        }

                        else { emptyFields = true; break; }
                    }

                    else if (field.CompareTag("Instructions"))
                    {
                        if (!string.IsNullOrWhiteSpace(field.text))
                        {
                            instructionsCount++;          
                            instructions += field.text + "<#>";
                        }

                        else { emptyFields = true; break; }
                    }
                }



                if(!emptyFields)
                {
                    // remove the last <#>
                    ingredients = ingredients.Remove(ingredients.Length - 3);
                    instructions = instructions.Remove(instructions.Length - 3);
                }


                if (emptyFields)
                {
                    Sprite icon = uiRepository.modalWindowElements.warningIcon;
                    interfaceManager.ShowModalWindow("Empty fields", icon, "You have left some fields empty, please complete them before sharing your recipe.", true);
                    interfaceManager.ToggleCurrentScreenInteractable();
                    interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
                    return;
                }


                else if (ingredientsCount < minIngredientsCount)
                {
                    Sprite icon = uiRepository.modalWindowElements.warningIcon;
                    interfaceManager.ShowModalWindow("Incomplete ingredients", icon, $"You have specified a few ingredients.A recipe must have atleast {minIngredientsCount} ingredients.", true);
                    interfaceManager.ToggleCurrentScreenInteractable();
                    interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
                    return;
                }


                else if (instructionsCount < minInstructionsCount)
                {
                    Sprite icon = uiRepository.modalWindowElements.warningIcon;
                    interfaceManager.ShowModalWindow("Incomplete instructions", icon, $"You have specified a few instructions.A recipe must have atleast {minInstructionsCount} instructions.", true);
                    interfaceManager.ToggleCurrentScreenInteractable();
                    interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
                    return;
                }




                string toAnalyze = title + " " + ingredients + " " + instructions;



                StartCoroutine(CognitiveServices.instance.TextModeration(toAnalyze, indecentContentThreshold, (bool? isOffensive) =>
                {

                    if (isOffensive == null)
                    {
                        Sprite icon = uiRepository.modalWindowElements.warningIcon;
                        interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                        interfaceManager.ToggleCurrentScreenInteractable();
                        interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
                    }

                    else if ((bool)isOffensive)
                    {
                        Sprite icon = uiRepository.modalWindowElements.warningIcon;
                        interfaceManager.ShowModalWindow("Offensive content", icon, "Inappropriate content detected, please correct it before sharing a recipe", true);
                        interfaceManager.ToggleCurrentScreenInteractable();
                        interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
                    }


                    else
                    {
                        string escapedTitle        = systemServices.EscapeMYSQLString(title);
                        string escapedIngredients  = systemServices.EscapeMYSQLString(ingredients);
                        string escapedInstructions = systemServices.EscapeMYSQLString(instructions);


                        WWWForm form = new WWWForm();
                        form.AddField("function", "CreateRecipe");
                        form.AddField("deviceId", deviceId);
                        form.AddField("title", escapedTitle);
                        form.AddField("ingredients", escapedIngredients);
                        form.AddField("procedure", escapedInstructions);
                        form.AddField("date", date);
                        form.AddField("sharedBy", sharedBy);
                        form.AddField("preparationTime", prepTime);


                        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

                        {

                            Debug.Log("Server res against share recipe is    "+response);
                            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                            {
                                string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                                Sprite icon = uiRepository.modalWindowElements.warningIcon;
                                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                            }

                            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
                            {
                                Sprite icon = uiRepository.modalWindowElements.errorIcon;
                                interfaceManager.ShowModalWindow("server error", icon, "Null or empty response from the server.", true);
                            }

                            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
                            {
                                // Not registered
                                string[] splitResponse = response.Split('+');
                                string generalError = splitResponse[1].Trim();
                                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;

                                interfaceManager.ShowModalWindow("Unregistered User", icon, "You need to register yourself to use all of our services, do you want to register now?", true, "Yes", "No",
                                  () => { interfaceManager.GoToScreen(8); } // Go to the registrations screen     
                                , () => { }
                                );
                            }

                            else if (Regex.IsMatch(response, systemServices.regexPatterns.apiMistmatch))
                            {
                                string[] splitResponse = response.Split('+');
                                Sprite icon = uiRepository.modalWindowElements.errorIcon;
                                interfaceManager.ShowModalWindow("Application error", icon, splitResponse[1].Trim(), true);
                            }


                            else
                            {

                                newRecipeShared = true;

                                Sprite icon = uiRepository.modalWindowElements.infoIcon;
                                interfaceManager.ShowModalWindow("Success", icon, "Recipe successfully shared.", true , () => 
                                {
                                    interfaceManager.CloseModalWindow();
                                    interfaceManager.OnBackButtonPress();
                                });

                            }


                            interfaceManager.ToggleCurrentScreenInteractable();
                            interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);

                        }

                        , form.data));

                    }

                }));


            }

        });


    }



    ///<summary> Save and update the changes made to given recipe in edit mode (This will only work for authored recipes). </summary>

    public void UpdateRecipe(Recipe recipe)
    {

        InterfaceManager interfaceManager = InterfaceManager.instance;
        UIRepository uiRepository = UIRepository.instance;
        SystemServices systemServices = SystemServices.instance;
        Animator loading = uiRepository.onScreenIndicators.boxLoading;

        interfaceManager.ToggleCurrentScreenInteractable();
        interfaceManager.ShowOrHideLoadingIndicator(true, loading, false);

        Transform recipeListView = uiRepository.shareAndEditRecipeElements.recipeListView.transform;

        InputField[] inputFields = recipeListView.GetComponentsInChildren<InputField>();


        string prepTime = inputFields[1].text;
        string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ".000000"; // MYSQL date format '2018-09-12 00:00:00.000000'
        string recipeId = recipe.id+"";
        string instructions = "";
        string ingredients = "";
        bool emptyFields = false;
        int ingredientsCount = 0;
        int instructionsCount = 0;



        for (int a = 0; a < inputFields.Length; a++)
        {

            InputField field = inputFields[a];

            if (field.CompareTag("Ingredients"))
            {
                if (!string.IsNullOrWhiteSpace(field.text))
                {
                    ingredientsCount++;
                    ingredients += field.text + "<#>";
                }

                else { emptyFields = true; break; }
            }

            else if (field.CompareTag("Instructions"))
            {
                if (!string.IsNullOrWhiteSpace(field.text))
                {
                    instructionsCount++;
                    instructions += field.text + "<#>";
                }

                else { emptyFields = true; break; }
            }
        }


        // remove the last <#>
        ingredients = ingredients.Remove(ingredients.Length - 3);
        instructions = instructions.Remove(instructions.Length - 3);


        if (emptyFields)
        {
            Sprite icon = uiRepository.modalWindowElements.warningIcon;
            interfaceManager.ShowModalWindow("Empty fields", icon, "You have left some fields empty, please complete them before sharing your recipe.", true);
            interfaceManager.ToggleCurrentScreenInteractable();
            interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
            return;
        }


        else if (ingredientsCount < minIngredientsCount)
        {
            Sprite icon = uiRepository.modalWindowElements.warningIcon;
            interfaceManager.ShowModalWindow("Incomplete ingredients", icon, $"You have specified a few ingredients.A recipe must have atleast {minIngredientsCount} ingredients.", true);
            interfaceManager.ToggleCurrentScreenInteractable();
            interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
            return;
        }


        else if (instructionsCount < minInstructionsCount)
        {
            Sprite icon = uiRepository.modalWindowElements.warningIcon;
            interfaceManager.ShowModalWindow("Incomplete instructions", icon, $"You have specified a few instructions.A recipe must have atleast {minInstructionsCount} instructions.", true);
            interfaceManager.ToggleCurrentScreenInteractable();
            interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
            return;
        }



        string toAnalyze = recipe.title + " " + ingredients + " " + instructions;


        StartCoroutine(CognitiveServices.instance.TextModeration(toAnalyze, indecentContentThreshold, (bool? isOffensive) =>
        {

            if (isOffensive == null)
            {
                Sprite icon = uiRepository.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
            }

            else if ((bool)isOffensive)
            {
                Sprite icon = uiRepository.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Offensive content", icon, "Inappropriate content detected, please correct it before sharing a recipe", true);
                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);
            }


            else
            {

                string escapedIngredients  = systemServices.EscapeMYSQLString(ingredients);
                string escapedInstructions = systemServices.EscapeMYSQLString(instructions);


                WWWForm form = new WWWForm();
                form.AddField("function", "UpdateRecipe");
                form.AddField("procedure", escapedInstructions);
                form.AddField("ingredients", escapedIngredients);
                form.AddField("date", date);
                form.AddField("recipeId", recipeId);
                form.AddField("preparationTime", prepTime);


                StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

                {


                    if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                    {
                        string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                        Sprite icon = uiRepository.modalWindowElements.warningIcon;
                        interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!," + netError, true);
                    }

                    else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
                    {
                        Sprite icon = uiRepository.modalWindowElements.errorIcon;
                        interfaceManager.ShowModalWindow("server error", icon, "Null or empty response from the server.", true);
                    }

                    else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
                    {
                        string[] splitResponse = response.Split('+');
                        Sprite icon = uiRepository.modalWindowElements.warningIcon;
                        interfaceManager.ShowModalWindow("General error", icon, splitResponse[1], true);
                    }

                    else if (Regex.IsMatch(response, systemServices.regexPatterns.apiMistmatch))
                    {
                        string[] splitResponse = response.Split('+');
                        Sprite icon = uiRepository.modalWindowElements.errorIcon;
                        interfaceManager.ShowModalWindow("Application error", icon, splitResponse[1], true);
                    }


                    else
                    {
                        Sprite icon = uiRepository.modalWindowElements.infoIcon;
                        interfaceManager.ShowModalWindow("Success", icon, "Recipe successfully updated", true);
                    }


                    interfaceManager.ToggleCurrentScreenInteractable();
                    interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);

                }

                , form.data));

            }

        }));

    }



    ///<summary> Deletes the recipe provided in the argument (This will only work for authored recipes).The parameter recipeObject is the gameObject prefab instantiated for the recipe. </summary>

    public void DeleteRecipe(Recipe recipe , GameObject recipeObject )
    {


        InterfaceManager interfaceManager = InterfaceManager.instance;
        UIRepository uiRepository = UIRepository.instance;
        SystemServices systemServices = SystemServices.instance;
        Animator loading = uiRepository.onScreenIndicators.boxLoading;

        WWWForm form = new WWWForm();
        form.AddField("function", "DeleteRecipe");
        form.AddField("recipeId", recipe.id);


        interfaceManager.ToggleCurrentScreenInteractable();
        interfaceManager.ShowOrHideLoadingIndicator(true, loading, false);

        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

        {
            //Debug.Log("Server response is:  "+ response);

            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                Sprite icon = uiRepository.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!," + netError, true);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
            {
                Sprite icon = uiRepository.modalWindowElements.errorIcon;
                interfaceManager.ShowModalWindow("server error", icon, "Null or empty response from the server.", true);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
            {
                string[] splitResponse = response.Split('+');
                Sprite icon = uiRepository.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("General error", icon, splitResponse[1], true);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.apiMistmatch))
            {
                string[] splitResponse = response.Split('+');
                Sprite icon = uiRepository.modalWindowElements.errorIcon;
                interfaceManager.ShowModalWindow("Application error", icon, splitResponse[1], true);
            }


            else
            {
                Sprite icon = uiRepository.modalWindowElements.infoIcon;
                GameObject.Destroy(recipeObject);

                interfaceManager.ShowModalWindow("Success", icon, "Recipe successfully deleted.", true , () => 
                {
                    interfaceManager.CloseModalWindow();
                    interfaceManager.OnBackButtonPress();
                });
                              
            }


            interfaceManager.ToggleCurrentScreenInteractable();
            interfaceManager.ShowOrHideLoadingIndicator(false, loading, false);

        }

        , form.data));


    }




    ///<summary> Opens the given recipe in edit mode (This will only work for authored recipes).The parameter recipeObject represents the actual gameObject of the recipe that is being edited. </summary>

    public void OpenInEditMode(Recipe recipe , GameObject recipeObject)
    {


        UIRepository uiRepository = UIRepository.instance;
        SystemServices systemServices = SystemServices.instance;
      

        Transform recipeListView = uiRepository.shareAndEditRecipeElements.recipeListView.transform;

        for (int a = 0; a < recipeListView.childCount; a++)
        {
            Destroy(recipeListView.GetChild(a).gameObject);
        }


        GameObject recipeHeader = Instantiate(uiRepository.shareAndEditRecipeElements.recipeHeader2Prefab);
        recipeHeader.transform.GetChild(0).GetComponentInChildren<Text>().text = User.instance.userInfo.name;
        DateTime dateTime = (DateTime)recipe.date;
        string date = dateTime.ToString("yyyy-MM-dd HH:mm"); // MYSQL date format '2018-09-12 00:00:00.000000'
        string[] splitDate = date.Split(' ');
        recipeHeader.transform.GetChild(1).GetComponentInChildren<Text>().text = splitDate[0] + "\n" + splitDate[1];

        
        InputField[] inputFields = recipeHeader.transform.GetComponentsInChildren<InputField>();
        inputFields[0].text = recipe.title;               // Title field
        inputFields[0].interactable = false;              // Title cannot be updated
        inputFields[1].text = recipe.preparationTime+"";  // Preparation time field


        InterfaceManager.instance.RecalculatePrefferedDimentions(recipeHeader);

        recipeHeader.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);


        for (int a = 0; a < recipe.ingredients.Length; a++)
        {
            GameObject editableInstruction = Instantiate(uiRepository.shareAndEditRecipeElements.editableInstructionPrefab);
            InterfaceManager.instance.RecalculatePrefferedDimentions(editableInstruction);
            editableInstruction.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);


            Button deleteButton = editableInstruction.GetComponentInChildren<Button>(true);
            InputField instruction = editableInstruction.GetComponentInChildren<InputField>(true);
            instruction.text = recipe.ingredients[a];

            instruction.transform.GetChild(0).GetComponent<Text>().text = "Ingredient";
            instruction.tag = "Ingredients";


            deleteButton.onClick.AddListener(() =>
            {
                Destroy(editableInstruction);
            });
        }


        GameObject addButtonPrefab = Instantiate(uiRepository.shareAndEditRecipeElements.addMoreButtonPrefab);
        InterfaceManager.instance.RecalculatePrefferedDimentions(addButtonPrefab);
        addButtonPrefab.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);

        Button addButton = addButtonPrefab.GetComponentInChildren<Button>();

        addButton.onClick.AddListener(() =>
        {
            AddStepOrIngredient(addButton.transform.parent.transform.GetSiblingIndex() , false);
        });


        GameObject procedureHeading = Instantiate(uiRepository.shareAndEditRecipeElements.procedureHeading2Prefab);
        InterfaceManager.instance.RecalculatePrefferedDimentions(procedureHeading);
        procedureHeading.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);




        for (int a = 0; a < recipe.instructions.Length; a++)
        {
            GameObject editableInstruction = Instantiate(uiRepository.shareAndEditRecipeElements.editableInstructionPrefab);
            InterfaceManager.instance.RecalculatePrefferedDimentions(editableInstruction);
            editableInstruction.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);


            Button deleteButton = editableInstruction.GetComponentInChildren<Button>(true);
            InputField instruction = editableInstruction.GetComponentInChildren<InputField>(true);
            instruction.text = recipe.instructions[a];

            instruction.transform.GetChild(0).GetComponent<Text>().text = "Instruction";
            instruction.tag = "Instructions";


            deleteButton.onClick.AddListener(() =>
            {
                Destroy(editableInstruction);
            });
        }


        addButtonPrefab = Instantiate(uiRepository.shareAndEditRecipeElements.addMoreButtonPrefab);
        InterfaceManager.instance.RecalculatePrefferedDimentions(addButtonPrefab);
        addButtonPrefab.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);

        Button addButton2 = addButtonPrefab.GetComponentInChildren<Button>();

        addButton2.onClick.AddListener(() =>
        {
            AddStepOrIngredient(addButton2.transform.parent.transform.GetSiblingIndex() , true);
        });


        GameObject updateDeletePrefab = Instantiate(uiRepository.shareAndEditRecipeElements.updateAndDeletePrefab);
        InterfaceManager.instance.RecalculatePrefferedDimentions(updateDeletePrefab);
        updateDeletePrefab.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);

        Button[] buttons = updateDeletePrefab.GetComponentsInChildren<Button>();
        Button deleteBtn= buttons[0];
        Button updateBtn = buttons[1];


        deleteBtn.onClick.AddListener(() =>
        {
            InterfaceManager interfaceManager = InterfaceManager.instance;
            Sprite icon = uiRepository.modalWindowElements.warningIcon;
            interfaceManager.ShowModalWindow("Delete recipe", icon, "Are you sure you want to delete this recipe?" ,true ,"Yes" ,"No",
             ()=> { DeleteRecipe(recipe , recipeObject); }
            ,()=> {  }
            );
           
        });

        updateBtn.onClick.AddListener(() =>
        {
            UpdateRecipe(recipe);
        });

        // anchors to corners maybe

    }




    ///<summary> Prepares the recipes sharing screen for sharing. </summary>

    public void PrepareSharingScreen()
    {


        UIRepository uiRepository         = UIRepository.instance;
        SystemServices systemServices     = SystemServices.instance;
        InterfaceManager interfaceManager = InterfaceManager.instance;
        User user                         = User.instance;

        Animator boxLoading = uiRepository.onScreenIndicators.boxLoading;

        interfaceManager.ToggleCurrentScreenInteractable();
        interfaceManager.ShowOrHideLoadingIndicator(true, boxLoading, false);



        user.InitializeUserInfo((bool? didSucceed) =>
        {

            if (didSucceed == null)
            {
                // The system failed to initialize user info
                Sprite icon = uiRepository.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
            }


            else if( didSucceed == false)
            {
                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;

                interfaceManager.ShowModalWindow("Unregistered User", icon, "You need to register yourself to use all of our services, do you want to register now?", true, "Yes", "No",
                  () => { interfaceManager.GoToScreen(8); } // Go to the registrations screen     
                , () => {  }
                );
            }




            Transform recipeListView = uiRepository.shareAndEditRecipeElements.recipeListView.transform;

            for (int a = 0; a < recipeListView.childCount; a++)
            {
                Destroy(recipeListView.GetChild(a).gameObject);
            }


            GameObject recipeHeader = Instantiate(uiRepository.shareAndEditRecipeElements.recipeHeader2Prefab);
            string name = (didSucceed == true) ? user.userInfo.name : "Unregistered User";
            recipeHeader.transform.GetChild(0).GetComponentInChildren<Text>().text = name.ToUpper();
            string date = DateTime.Now.ToString("yyyy-MM-dd HH:mm"); // MYSQL date format '2018-09-12 00:00:00.000000'
            string[] splitDate = date.Split(' ');
            recipeHeader.transform.GetChild(1).GetComponentInChildren<Text>().text = splitDate[0] + "\n" + splitDate[1];


            InterfaceManager.instance.RecalculatePrefferedDimentions(recipeHeader);

            recipeHeader.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);


            for (int a = 0; a < minIngredientsCount; a++)
            {
                GameObject editableInstruction = Instantiate(uiRepository.shareAndEditRecipeElements.editableInstructionPrefab);
                InterfaceManager.instance.RecalculatePrefferedDimentions(editableInstruction);
                editableInstruction.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);


                Button deleteButton = editableInstruction.GetComponentInChildren<Button>(true);
                InputField instruction = editableInstruction.GetComponentInChildren<InputField>(true);

                instruction.transform.GetChild(0).GetComponent<Text>().text = "Ingredient";
                instruction.tag = "Ingredients";

                deleteButton.onClick.AddListener(() =>
                {
                    Destroy(editableInstruction);
                });
            }


            GameObject addButtonPrefab = Instantiate(uiRepository.shareAndEditRecipeElements.addMoreButtonPrefab);
            InterfaceManager.instance.RecalculatePrefferedDimentions(addButtonPrefab);
            addButtonPrefab.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);

            Button addButton = addButtonPrefab.GetComponentInChildren<Button>();

            addButton.onClick.AddListener(() =>
            {
                AddStepOrIngredient(addButton.transform.parent.transform.GetSiblingIndex(), false);
            });


            GameObject procedureHeading = Instantiate(uiRepository.shareAndEditRecipeElements.procedureHeading2Prefab);
            InterfaceManager.instance.RecalculatePrefferedDimentions(procedureHeading);
            procedureHeading.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);




            for (int a = 0; a < minInstructionsCount; a++)
            {
                GameObject editableInstruction = Instantiate(uiRepository.shareAndEditRecipeElements.editableInstructionPrefab);
                InterfaceManager.instance.RecalculatePrefferedDimentions(editableInstruction);
                editableInstruction.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);


                Button deleteButton = editableInstruction.GetComponentInChildren<Button>(true);
                InputField instruction = editableInstruction.GetComponentInChildren<InputField>(true);

                instruction.transform.GetChild(0).GetComponent<Text>().text = "Instruction";
                instruction.tag = "Instructions";

                deleteButton.onClick.AddListener(() =>
                {
                    Destroy(editableInstruction);
                });
            }


            addButtonPrefab = Instantiate(uiRepository.shareAndEditRecipeElements.addMoreButtonPrefab);
            InterfaceManager.instance.RecalculatePrefferedDimentions(addButtonPrefab);
            addButtonPrefab.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);

            Button addButton2 = addButtonPrefab.GetComponentInChildren<Button>();

            addButton2.onClick.AddListener(() =>
            {
                AddStepOrIngredient(addButton2.transform.parent.transform.GetSiblingIndex(), true);
            });


            GameObject shareButtonPrefab = Instantiate(uiRepository.shareAndEditRecipeElements.shareButtonPrefab);
            InterfaceManager.instance.RecalculatePrefferedDimentions(shareButtonPrefab);
            shareButtonPrefab.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);

            Button shareButton = shareButtonPrefab.GetComponentInChildren<Button>();

            shareButton.onClick.AddListener(() =>
            {
                ShareRecipe();
            });


            // anchors to corners maybe

            interfaceManager.ToggleCurrentScreenInteractable();
            interfaceManager.ShowOrHideLoadingIndicator(false , boxLoading , false);

        });

    }





    ///<summary> Adds another step or ingredient to the share or edit recipe list and makes it the child of recipe list at the given index.The parameter isStep indicates whether to add a new step or an ingredient</summary>

    private void AddStepOrIngredient(int index , bool isStep)
    {
        UIRepository uiRepository = UIRepository.instance;

        GameObject editableInstruction = Instantiate(uiRepository.shareAndEditRecipeElements.editableInstructionPrefab);
        InterfaceManager.instance.RecalculatePrefferedDimentions(editableInstruction);
        editableInstruction.transform.SetParent(uiRepository.shareAndEditRecipeElements.recipeListView.transform, false);
        editableInstruction.transform.SetSiblingIndex(index);

        Button deleteButton = editableInstruction.GetComponentInChildren<Button>(true);
        InputField instruction = editableInstruction.GetComponentInChildren<InputField>(true);

        if (isStep)
        {
            instruction.transform.GetChild(0).GetComponent<Text>().text = "Instruction"; instruction.tag = "Instructions";
            uiRepository.shareAndEditRecipeElements.recipeScrollPanel.GetComponent<ScrollRect>().verticalNormalizedPosition = 0;
        }
        else
        {
            instruction.transform.GetChild(0).GetComponent<Text>().text = "Ingredient";  instruction.tag = "Ingredients";
        }

        deleteButton.onClick.AddListener(() =>
        {
            Destroy(editableInstruction);
        });

    }



    ///<summary> Load the recipe viewer panel with the recipe provided in the argument. </summary>

    public void ViewRecipe(UnityEngine.Object recipe)
    {

        UIRepository uiRepository = UIRepository.instance;

        Transform recipeHeader = uiRepository.recipeViewerPanel.recipeViewerListView.transform.GetChild(0);


        Recipe recipeToLoad = null;
        GameObject recipeTileClicked = recipe as GameObject;
        recipeToLoad = recipeTileClicked.GetComponent<RecipePointer>().recipePointer;

        // Don't load the same recipe in the viewer.
        if (lastViewedRecipe != null)
        {
            if(lastViewedRecipe.id == recipeToLoad.id) { return; }
        }


        // Clear all previous data before moving any further.(Destroy all children except the first 1(RecipeHeader))

        Transform recipeViewerListView = uiRepository.recipeViewerPanel.recipeViewerListView.transform;

        for (int a = 1; a < recipeViewerListView.childCount; a++)
        {
            Destroy(recipeViewerListView.GetChild(a).gameObject);
        }


        InterfaceManager.AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerPanel.transform as RectTransform);
        InterfaceManager.AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerScrollPanel.transform as RectTransform);

      
        recipeHeader = uiRepository.recipeViewerPanel.recipeViewerListView.transform.GetChild(0);


        Transform child = recipeHeader.GetChild(0);    
        child.GetComponent<Text>().text = recipeToLoad.title.ToUpper();

        child = recipeHeader.GetChild(1).GetChild(0);
        child.GetComponent<Text>().text = recipeToLoad.authorName.ToUpper();

        child = recipeHeader.GetChild(2).GetChild(0);    
        child.GetComponent<Text>().text = recipeToLoad.date.Value.ToString("yyyy-MM-dd") + "\n" + recipeToLoad.date.Value.ToString("HH:mm");

        child = recipeHeader.GetChild(3).GetChild(0);
        child.GetComponent<Text>().text = recipeToLoad.preparationTime + " m";


        foreach(string ingredient in recipeToLoad.ingredients)
        {
            GameObject ingredientText = Instantiate(uiRepository.recipeViewerPanel.instructionPrefab);
            ingredientText.transform.GetChild(0).GetComponent<Text>().text = ingredient;

            InterfaceManager.instance.RecalculatePrefferedDimentions(ingredientText);

            ingredientText.transform.SetParent(uiRepository.recipeViewerPanel.recipeViewerListView.transform, false);
        }


        GameObject procedureHeadingLabel = Instantiate(uiRepository.recipeViewerPanel.procedureHeadingPrefab);
        InterfaceManager.instance.RecalculatePrefferedDimentions(procedureHeadingLabel);
        procedureHeadingLabel.transform.SetParent(uiRepository.recipeViewerPanel.recipeViewerListView.transform, false);


        foreach (string instruction in recipeToLoad.instructions)
        {
            GameObject instructionText = Instantiate(uiRepository.recipeViewerPanel.instructionPrefab);
            instructionText.transform.GetChild(0).GetComponent<Text>().text = instruction;
            InterfaceManager.instance.RecalculatePrefferedDimentions(instructionText);
            instructionText.transform.SetParent(uiRepository.recipeViewerPanel.recipeViewerListView.transform, false);
        }


        lastViewedRecipe = recipeToLoad;

        InterfaceManager.AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerScrollPanel.transform as RectTransform);
        InterfaceManager.AnchorsToCorners(uiRepository.recipeViewerPanel.recipeViewerListView.transform as RectTransform);

    }





    ///<summary> Sets the list of recipes shared by this or other users and loads them in the relevant interface. The optional parameter "loadMoreButton" indicates whether this call is made by the loadMoreButton or not so that it will be destroyed when more recipes are successfully loaded.
    ///<para> Note that this function successively fetches number of recipes specified by "unauthoredRecipesFetchThreshold" or "authoredRecipesFetchThreshold" field (depending upon whether "fetchAuthored" parameter is true or not) from the database at each call. </para>
    /// </summary>

    public void FetchSuccessiveRecipes(bool fetchAuthored , GameObject loadMoreButton = null)
    {

        InterfaceManager interfaceManager = InterfaceManager.instance;
        SystemServices systemServices     = SystemServices.instance;

        interfaceManager.ToggleCurrentScreenInteractable(false);
        interfaceManager.ShowOrHideLoadingIndicator(true, UIRepository.instance.onScreenIndicators.boxLoading, false);
       

        if(!fetchAuthored && totalunauthoredRecipes == -1)
        {
            CountTotalunauthoredRecipes((bool? didSucceed) => 
            {
                if(didSucceed == null)
                {
                    Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                    interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                    interfaceManager.ToggleCurrentScreenInteractable(true);
                    interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                    return;
                }

                else if(didSucceed == false) { /*system failed to count totalunauthoredRecipes for any other reason*/}

                else
                {
                    interfaceManager.ToggleCurrentScreenInteractable(true);
                    interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                    FetchSuccessiveRecipes(fetchAuthored, loadMoreButton);
                    return;
                }
      
            });

            return;
        }


        if (fetchAuthored && totalAuthoredRecipes == -1)
        {
            CountTotalAuthoredRecipes((bool? didSucceed) => 
            {
                if (didSucceed == null)
                {
                    Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                    interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                    interfaceManager.ToggleCurrentScreenInteractable(true);
                    interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                    return;
                }

                else if (didSucceed == false)
                {
                    /*system failed to count totalunauthoredRecipes because the user is unregsitered*/
                    Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                    interfaceManager.ShowModalWindow("Unregistered User", icon, "You need to register yourself to use all of our services, do you want to register now?", true, "Yes", "No",
                      () => { interfaceManager.GoToScreen(8); /*Go to the registrations screen*/ }
                    , () => { interfaceManager.GoToScreen(0); /*Go to the home screen*/}
                    );

                    interfaceManager.ToggleCurrentScreenInteractable(true);
                    interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                    return;
                }

                else
                {
                    interfaceManager.ToggleCurrentScreenInteractable();
                    interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                    FetchSuccessiveRecipes(fetchAuthored, loadMoreButton);
                    return;
                }
            });


            return;

        }



        if (fetchAuthored)
        {
            Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;

            if (totalAuthoredRecipes == 0)
            {
                interfaceManager.ShowModalWindow("No authored recipes found", icon, "You do not have any recipes shared.Do you want to share a recipe?.", true, "Yes", "No",
                  () => { interfaceManager.GoToScreen(3); } // Go to the Create recipes screen    
                , () => { }
                );
            }

            else if (totalAuthoredRecipes == authoredRecipesLoadedCount)
            {
                interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                interfaceManager.ToggleCurrentScreenInteractable();
                Debug.Log("No more authored recipes to fetch.");
                return;
            }

        }


        else
        {
            Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;

            if (totalunauthoredRecipes == 0)
            {
                interfaceManager.ShowModalWindow("No recipes found", icon, "There are no recipes shared by users.Do you want to share the first one?.", true, "Yes", "No",
                    () => { interfaceManager.GoToScreen(3); } // Go to the Create recipes screen    
                , () => { }
                );
            }

            else if (totalunauthoredRecipes == unauthoredRecipesLoadedCount)
            {
                interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                interfaceManager.ToggleCurrentScreenInteractable();
                Debug.Log("No more unauthored recipes to fetch.");
                return;
            }
        }



        bool? flag = User.instance.CheckRegistrationStatusBlockingMode(false);

        bool recheck = (flag == null) ? true : false;

        User.instance.CheckRegistrationStatusAsyncMode(recheck, (bool? registerationStatus) =>

        {

            if (registerationStatus == null)
            {
                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                //Debug.LogWarning("Display modal window for internet failure and maybe go to the home screen");

                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);

            }

            else if(registerationStatus == false)
            {
                Debug.LogWarning("Display modal window for not registered do you want to register? if yes go to registration screen if no then go to home screen.");
                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);

                Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                interfaceManager.ShowModalWindow("Unregistered User", icon, "You need to register yourself to use all of our services, do you want to register now?", true, "Yes", "No",
                  () => {  interfaceManager.GoToScreen(8); /*Go to the registrations screen*/ }
                , () => {  interfaceManager.GoToScreen(0); /*Go to the home screen*/}
                );
            }


            else
            {


                WWWForm form = new WWWForm();

                form.AddField("function", "FetchRecipes");
                form.AddField("deviceId", SystemServices.instance.deviceUniqueIdentifier);

                if (fetchAuthored)
                {
                    form.AddField("authoredOnly", "true");
                    form.AddField("startRow", (authoredRecipesLoadedCount + 1) + "");
                    form.AddField("count", (authoredRecipesFetchThreshold) + "");
                }
                else
                {
                    form.AddField("authoredOnly", "false");
                    form.AddField("startRow", (unauthoredRecipesLoadedCount + 1) + "");
                    form.AddField("count", (unauthoredRecipesFetchThreshold) + "");
                }


                StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

                {

                    if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                    {
                        string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                        //Debug.LogWarning(netError);
                        interfaceManager.ToggleCurrentScreenInteractable();
                        interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);

                        Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                        interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                    }

                    else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
                    {
                        string error = Regex.Replace(response, systemServices.regexPatterns.nullOrEmpty, "").ToString();
                        Debug.LogWarning(error);
                        interfaceManager.ToggleCurrentScreenInteractable();
                        interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);
                    }

                    else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
                    {

                        newRecipeShared = false;          

                        // No authored or unauthored recipes in the database.
                        string[] splitResponse = response.Split('+');
                        string generalError = splitResponse[1].Trim();
                        //Debug.LogWarning(generalError);

                        interfaceManager.ToggleCurrentScreenInteractable();
                        interfaceManager.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);

                        Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;

                        if (fetchAuthored)
                        {
                            interfaceManager.ShowModalWindow("No authored recipes found", icon, "You do not have any recipes shared.Do you want to share a recipe?.", true, "Yes", "No",
                              () => { interfaceManager.GoToScreen(3); } // Go to the Create recipes screen    
                            , () => { }
                            );
                          
                        }

                        else
                        {
                            interfaceManager.ShowModalWindow("No recipes found", icon, "There are no recipes shared by users.Do you want to share the first one?.", true, "Yes", "No",
                              () => { interfaceManager.GoToScreen(3); } // Go to the Create recipes screen    
                            , () => { }
                            );
                        }

                    }


                    else
                    {
                        newRecipeShared = false;
                        StartCoroutine(LoadFetchedRecipes(response, fetchAuthored , loadMoreButton));
                    }

                }

                , form.data));

            }

        });

    }






    ///<summary> Parses the server response for the fetched recipes and initializes the relevant recipes list and loads them in the relavant interface.The "areAuthored parameter dictates the function whether to load the recipes in authored recipes list or unauthored recipes list and interfaces."
    /// <para> The parameter "loadMoreButton" indicates whether this call is made by the loadMoreButton or not so that it will be destroyed when more recipes are successfully loaded. If the argument is null then it will be assumed that the call is not made by the loadMoreButton.</para>
    /// </summary>

    private IEnumerator LoadFetchedRecipes(string recipes , bool areAuthored , GameObject loadMoreButton)
    {

        yield return new WaitForSeconds(0);

        if(loadMoreButton != null) { DestroyImmediate(loadMoreButton); }    // Destroy this load more button from the list view to create space for new entries.


        string[] rows = Regex.Split(recipes, SystemServices.instance.regexPatterns.tableRowsDelimitter , RegexOptions.Compiled);     // Rows will be one more than the actual rows.

        if (areAuthored) { authoredRecipesLoadedCount += ((uint)rows.Length - 1); }
        else { unauthoredRecipesLoadedCount += ((uint)rows.Length - 1); }



        if (areAuthored)
        {
           if(authoredRecipes == null)   { authoredRecipes = new Dictionary<int, Recipe>(); }
        }

        else
        {
           if(unauthoredRecipes == null) { unauthoredRecipes = new Dictionary<int, Recipe>(); }
        }



        for(int a = 0; a < rows.Length - 1; a++)
        {
            
            string row = rows[a];
            
            string[] columns = Regex.Split(row, SystemServices.instance.regexPatterns.tableColumnsDelimitter, RegexOptions.Compiled);
            string[] instructions =  Regex.Split(columns[6], SystemServices.instance.regexPatterns.recipesStepsDelimitter, RegexOptions.Compiled);
            string[] ingredients  =  Regex.Split(columns[8], SystemServices.instance.regexPatterns.recipesStepsDelimitter, RegexOptions.Compiled);

            string title = columns[2];
            bool isPreloaded = false;
            int preparationTime = int.Parse(columns[4]);
            int id = int.Parse(columns[0]);
            string authorName = columns[3];      
            DateTime? date = DateTime.ParseExact(columns[5].Split('.')[0], "yyyy-MM-dd HH:mm:ss" , CultureInfo.InvariantCulture);

            Recipe recipe = new Recipe(instructions , ingredients , title , isPreloaded , preparationTime , id ,authorName , date);

            if (areAuthored) { authoredRecipes.Add(recipe.id, recipe); }
            else { unauthoredRecipes.Add(recipe.id, recipe); }


            GameObject recipeTile = Instantiate(UIRepository.instance.recipeListViewer.recipeTilePrefab);
            
            recipeTile.transform.GetChild(0).GetComponent<Text>().text = recipe.title.ToUpper();
            recipeTile.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = recipe.authorName.ToUpper();
            recipeTile.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = recipe.date.Value.ToString("yyyy-MM-dd") + "\n" + recipe.date.Value.ToString("HH:mm"); 
            recipeTile.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = recipe.preparationTime + " m";

            // Set this recipe tile to point to the corresponding recipe object
            recipeTile.GetComponent<RecipePointer>().recipePointer = recipe;

            Button[] buttons         = recipeTile.GetComponentsInChildren<Button>(true);
            Button recipeTileButton  = buttons[0];
            Button loadAudioPlayer   = buttons[1];
            Button reportSpamButton  = buttons[2];
            Button editRecipeButton  = buttons[3];

            // When the recipe tile is clicked it should go to view that recipe
            recipeTileButton.onClick.AddListener(() => { InterfaceManager.instance.ToggleRecipeViewerPanel(recipeTile); });

            loadAudioPlayer.onClick.AddListener(() =>
            {
                GetComponent<AudioPlayer>().LoadForRecipe(recipe);
            });


            if (!areAuthored)
            {
                // When the report spam button is clicked it should report spam against that recipe
                reportSpamButton.onClick.AddListener(() =>
                {
                    SpamManager.instance.ReportSpam(recipe, (bool? successfullyReported) =>
                    {
                        // If spam was successfully reported from this button click then make it uninteractable so that the user can't report spam against the same recipe multiple times.
                        if (successfullyReported == true) { reportSpamButton.interactable = false; }

                        // If the user had already reported spam against this recipe so make the button uninteractable.
                        if (successfullyReported == null) { reportSpamButton.interactable = false; }
                    });

                });
            }

            else {

                    editRecipeButton.gameObject.SetActive(true);

                    // When the edit recipe button is clicked open the recipe in edit mode.
                    editRecipeButton.onClick.AddListener(() =>
                    {
                        prepareForEditingScreen = true;
                        InterfaceManager.instance.GoToScreen(3);
                        SystemServices.instance.RunDelayedCommand(InterfaceManager.instance.screenFadeDuration - 0.3f , ()=> 
                        {
                            prepareForEditingScreen = false;
                            GameObject recipeObjectToEdit = editRecipeButton.transform.parent.gameObject;
                            OpenInEditMode(recipe , recipeObjectToEdit);
                        });


                    });

                    reportSpamButton.gameObject.SetActive(false);

                 }


            InterfaceManager.instance.RecalculatePrefferedDimentions(recipeTile);


            if (areAuthored)
            {
                recipeTile.transform.SetParent(UIRepository.instance.recipeListViewer.authoredRecipesListView.transform, false);
                updateListsAnchors = true;
            } 

            else
            {
                recipeTile.transform.SetParent(UIRepository.instance.recipeListViewer.unauthoredRecipesListView.transform, false);
                updateListsAnchors = true;
            }

        }



        // If there are more authored recipes to load then append the loadMoreButton at the end of the listView.

        if (areAuthored)
        {

            if (totalAuthoredRecipes != authoredRecipesLoadedCount)
            {
                GameObject loadMoreButton1 = Instantiate(UIRepository.instance.recipeListViewer.loadMoreButtonContainerPrefab);
                loadMoreButton1.SetActive(true);
                Button button1 = loadMoreButton1.GetComponentInChildren<Button>();
                button1.onClick.AddListener(() => { LoadMoreRecipes(loadMoreButton1); });

                InterfaceManager.instance.RecalculatePrefferedDimentions(loadMoreButton1);
                loadMoreButton1.transform.SetParent(UIRepository.instance.recipeListViewer.authoredRecipesListView.transform, false);

                updateListsAnchors = true;
            }
        }


        // If there are more unauthored recipes to load then append the loadMoreButton at the end of the listView.

        else
        {

            if (totalunauthoredRecipes != unauthoredRecipesLoadedCount)
            {

                GameObject loadMoreButton1 = Instantiate(UIRepository.instance.recipeListViewer.loadMoreButtonContainerPrefab);
                loadMoreButton1.SetActive(true);
                Button button1 = loadMoreButton1.GetComponentInChildren<Button>();
                button1.onClick.AddListener(() => { LoadMoreRecipes(loadMoreButton1); });

                InterfaceManager.instance.RecalculatePrefferedDimentions(loadMoreButton1);
                loadMoreButton1.transform.SetParent(UIRepository.instance.recipeListViewer.unauthoredRecipesListView.transform, false);

                updateListsAnchors = true;
            }
        }



        InterfaceManager.instance.ToggleCurrentScreenInteractable();
        InterfaceManager.instance.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.boxLoading, false);

    }






    
        ///<summary> Loads a preloaded recipe specified by the name.Note this function highly depends upon the name of the recipe.
        ///<para>Note that the instructions for a recipe should be written in a gameObject tagged "Instructions" with a text component attached having the instructions specified in the format(inst1\ninstr2\n etc). </para>
        ///<para>Also the ingredients marker should be tagged as "Ingredients" and having a text component with ingredients specified in the format (ingredient1\ningredient2\nIngredient3 etc). </para>
        ///</summary>

    public void LoadPreloadedRecipe(string name)
    {

   
        isRecipeLoading = true;

        if (activeRecipe != null)
        {
            if (activeRecipe.title.Equals(name)) { isRecipeLoading = false; return; }
        }


        //Debug.Log("Load recipe.  " + name);

        string title;
        string videoLink;
        string[] instructions = null;
        string[] ingredients = null;
        int preparationTime = 0;
        VideoPlayer videoPlayer = null;
        GameObject recipeToLoad = new GameObject();





        GameObject recipeToDestroy = GameObject.FindGameObjectWithTag("Recipe");

        if(recipeToDestroy)
        {

            ImageTargetBehaviour[] imageTargets = recipeToDestroy.GetComponentsInChildren<ImageTargetBehaviour>();

            foreach(ImageTargetBehaviour imageTarget in imageTargets)
            {
                Trackable trackable = imageTarget.Trackable;
                if(trackable == null) { continue; }
                TrackerManager.Instance.GetStateManager().DestroyTrackableBehavioursForTrackable(trackable, true);
            }


            Destroy(recipeToDestroy);

        }




        foreach (GameObject recipe in preloadedRecipes)
        {

            if (recipe.name.Equals(name))
            {
                recipeToLoad      = Instantiate(recipe) ;
                recipeToLoad.name = name;
                break;
            }

        }

        
        for (int a = 0; a < recipeToLoad.transform.childCount; a++)
        {

            Transform child = recipeToLoad.transform.GetChild(a);

            if (instructions != null && ingredients != null && videoPlayer != null) { break; }


            if (child.CompareTag("Instructions")) { instructions = child.GetComponent<Text>().text.Split('\n'); }

            if (child.CompareTag("VideoMarker")) { videoPlayer = child.GetComponentInChildren<VideoPlayer>(); }

            if (child.CompareTag("Ingredients"))
            {
                ingredients = child.GetComponent<Text>().text.Split('\n');

                Transform[] ingredientsModels = new Transform[child.childCount];

                
                for(int b = 0; b < ingredientsModels.Length; b++)
                {
                    ingredientsModels[b] = child.transform.GetChild(b);
                }

                GetComponent<IngredientsAnimator>().ingredientsModels = ingredientsModels;
                //GetComponent<IngredientsAnimator>().ingredientsModels = child.GetComponentsOnlyInChildren<Transform>(true);     
            }
            
            if(child.CompareTag("PreparationTime"))
            {
                string time = child.GetComponent<Text>().text;
                bool result;

                result = int.TryParse(time, out preparationTime);
                preparationTime = result ? preparationTime : -1;

                //Debug.LogWarning("The preparation time specified for  \"" + recipeToLoad.name + "\" is not a valid integer");

            }


        }



        title = recipeToLoad.name;


        activeRecipe = new Recipe(instructions, ingredients, title, true , preparationTime);

        GetComponent<AugVideoPlayer>().SetVideoPlayer(videoPlayer);
        videoLink = SystemServices.instance.videoLoaderUrl + title + ".mp4";
        videoPlayer.url = videoLink;

        
        StartCoroutine(SystemServices.instance.AsyncInternetConnectivityCheck((bool isConnected) =>
        {
            if(isConnected)
            {
                videoPlayer.Prepare();                    // Prepare the video in the background to prevent buffering later on.
                isBackgroundBuffering = true;
            }

            else
            {
                isBackgroundBuffering = false;
            }
        }
        ));
        


        //GetComponent<AudioPlayer>().PrepareForRecipe(activeRecipe);
        //SystemServices.instance.SwitchDatasetByName(name.Replace(' ','_'));
        
        //Debug.Log(link);
        TrackerManager.Instance.GetStateManager().ReassociateTrackables();
        isRecipeLoading = false;


        if(!firstRecipeLoad)
        {
            Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
            InterfaceManager.instance.ShowModalWindow("Success", icon, $"You have loaded recipe: \"{name}\".", true);         
        }


        firstRecipeLoad = false;
    }






    /// <summary> Finds the total number of authored in the database seperately and initializes the "totalAauthoredRecipes" field accordingly.In any kind of failure the "totalAuthoredRecipes" is set to -1.
    /// <para> The callback is passed true if the system successfully counted the Authored recipes, false if the system fails to count recipes, and null if internet fails.</para>
    /// </summary>

    private void CountTotalAuthoredRecipes(Action<bool?> callback)
    {

        SystemServices systemServices = SystemServices.instance;

        WWWForm form = new WWWForm();
        form.AddField("function", "CountRecipes");
        form.AddField("deviceId", SystemServices.instance.deviceUniqueIdentifier);
        form.AddField("authored", true + "");
        form.AddField("countAll", false + "");

        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

        {

            if (Regex.IsMatch(response, systemServices.regexPatterns.netError)) { totalAuthoredRecipes = -1; callback(null); }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty)) { totalAuthoredRecipes = -1; callback(false); }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError)) { totalAuthoredRecipes = -1; callback(false); }

            else
            {

                bool result;
                result = int.TryParse(response, out totalAuthoredRecipes);

                if(result == false)
                {
                    totalAuthoredRecipes = -1;
                    callback(false);
                }

                else { callback(true); }
            }

        }

        , form.data));

    }






    /// <summary> Finds the total number of unauthored in the database seperately and initializes the "totalunauthoredRecipes" field accordingly.In any kind of failure the "totalunauthoredRecipes" is set to -1.
    /// <para> The callback is passed true if the system successfully counted the unauthored recipes, false if the system fails to count recipes, and null if internet fails.</para>
    /// </summary>

    private void CountTotalunauthoredRecipes(Action<bool?> callback)
    {

        SystemServices systemServices = SystemServices.instance;

        WWWForm form = new WWWForm();
       
        form.AddField("function", "CountRecipes");
        form.AddField("deviceId", SystemServices.instance.deviceUniqueIdentifier);
        form.AddField("authored", false + "");
        form.AddField("countAll", false + "");

        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

        {

            if (Regex.IsMatch(response, systemServices.regexPatterns.netError)) { totalunauthoredRecipes = -1; callback(null); }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty)) { totalunauthoredRecipes = -1; callback(false); }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError)) { totalunauthoredRecipes = -1; callback(false); }

            else
            {

                bool result;
                result = int.TryParse(response, out totalunauthoredRecipes);

                if (result == false)
                {
                    totalAuthoredRecipes = -1;
                    callback(false);
                }

                else { callback(true); }
            }

        }

        , form.data));

    }




    /// <summary> Listens for the onclick events on load more buttons on the recipes list. </summary>

    public void LoadMoreRecipes(UnityEngine.Object loadMoreButton)
    {

        GameObject loadMore = (GameObject)loadMoreButton;
        

        if(UIRepository.instance.uiTiles.unAuthoredRecipesTile.GetComponent<UITileInfo>().isSelected)
        {
            FetchSuccessiveRecipes(false , loadMore);
        } 
        
        else
        {
            FetchSuccessiveRecipes(true , loadMore);
        }  
    }





    /// <summary> Finds the closest recipe matching the given title in the database. </summary>

    public void SearchRecipe(UnityEngine.Object inputField)
    {

        InterfaceManager interfaceManager = InterfaceManager.instance;

        string searchEntry = ((GameObject)inputField).GetComponent<InputField>().text;

        if(string.IsNullOrWhiteSpace(searchEntry))
        {
            Debug.Log("Nothing to search for"); return;
        }

        UIRepository.instance.recipeSearchElements.searchedRecipesScrollPanel.SetActive(true);
        UIRepository.instance.recipeListViewer.authoredRecipesScrollPanel.SetActive(false);
        UIRepository.instance.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(false);


        interfaceManager.ToggleCurrentScreenInteractable(false);
        Animator boxLoading = UIRepository.instance.onScreenIndicators.boxLoading;

        interfaceManager.ShowOrHideLoadingIndicator(true, boxLoading, false);


        // If the user searched for the exact same title again then don't search in the database again and retaint he previous results.
        if (lastSearchEntry != null)
        {

            if (lastSearchEntry == searchEntry)
            {
                interfaceManager.ToggleCurrentScreenInteractable(true);
                interfaceManager.ShowOrHideLoadingIndicator(false, boxLoading, false);
                Debug.Log("Won't search for the same thing again.");
                interfaceManager.UpdateCurrentActivePanel();
                return;
            }

        }



        // Clear all previous results in the search panel.(Destroy all children except the first child which is the gap element)

        Transform listView  = UIRepository.instance.recipeSearchElements.searchedRecipesListView.transform;

        for (int a = 1; a < listView.childCount; a++)
        {
            Destroy(listView.GetChild(a).gameObject);
        }



        UITileInfo tileInfo = UIRepository.instance.uiTiles.unAuthoredRecipesTile.GetComponent<UITileInfo>();

        bool authoredOnly = tileInfo.isSelected ? false : true;
        SystemServices systemServices = SystemServices.instance;


        bool? flag = User.instance.CheckRegistrationStatusBlockingMode(false);
        bool recheck = (flag == null) ? true : false;


        User.instance.CheckRegistrationStatusAsyncMode(recheck, (bool? registerationStatus) =>

        {

            if (registerationStatus == null)
            {
                lastSearchEntry = null;
                //Debug.LogWarning("Display modal window for internet failure and maybe go to the home screen");
                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, boxLoading, false);

                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true , ()=> 
                {
                    interfaceManager.CloseModalWindow();
                    CloseSearchResults();
                });

            }

            else if (registerationStatus == false)
            {
                lastSearchEntry = null;
                //Debug.LogWarning("Display modal window for not registered do you want to register? if yes go to registration screen if no then go to home screen.");
                interfaceManager.ToggleCurrentScreenInteractable();
                interfaceManager.ShowOrHideLoadingIndicator(false, boxLoading, false);

                Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                interfaceManager.ShowModalWindow("Unregistered User", icon, "You need to register yourself to use all of our services, do you want to register now?", true, "Yes", "No",
                  () => { interfaceManager.GoToScreen(8); /*Go to the registrations screen*/ }
                , () => { interfaceManager.GoToScreen(0); /*Go to the home screen*/}
                );
            }




            else
            {


                WWWForm form = new WWWForm();

                form.AddField("function", "SearchRecipe");
                form.AddField("deviceId", SystemServices.instance.deviceUniqueIdentifier);
                form.AddField("authoredOnly", "" + authoredOnly);
                form.AddField("searchEntry", searchEntry);
                form.AddField("similarityPercentage", searchSimilarityPercentage + "");

                StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>

                {

                    if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                    {
                        string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                        //Debug.LogWarning(netError);
                        lastSearchEntry = null;
                        interfaceManager.ToggleCurrentScreenInteractable(true);
                        interfaceManager.ShowOrHideLoadingIndicator(false, boxLoading, false);

                        Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                        interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true, () =>
                        {
                            interfaceManager.CloseModalWindow();
                            CloseSearchResults();
                        });
                    }


                    else if (Regex.IsMatch(response, systemServices.regexPatterns.apiMistmatch))
                    {
                        lastSearchEntry = null;
                        string[] splitResponse = response.Split('+');
                        Debug.LogWarning(splitResponse[1]);
                    }


                    else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
                    {
                        lastSearchEntry = null;
                        string netError = Regex.Replace(response, systemServices.regexPatterns.nullOrEmpty, "").ToString();
                        Debug.LogWarning(netError);
                
                        interfaceManager.ToggleCurrentScreenInteractable(true);
                        interfaceManager.ShowOrHideLoadingIndicator(false, boxLoading, false);
                    }


                    else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
                    {
                        lastSearchEntry = searchEntry;
                        string[] splitResponse = response.Split('+');
                        string generalError = splitResponse[1].Trim();
                        //Debug.LogWarning(generalError);

                        interfaceManager.ToggleCurrentScreenInteractable(true);
                        interfaceManager.ShowOrHideLoadingIndicator(false, boxLoading, false);

                        Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                        InterfaceManager.instance.ShowModalWindow("No results", icon, generalError, true, () =>
                        {
                            interfaceManager.CloseModalWindow();
                            CloseSearchResults();
                        });
                    }


                    else
                    {
                        lastSearchEntry = searchEntry;
                        StartCoroutine(LoadSearchResults(response, authoredOnly));
                        UIRepository uiRepository = UIRepository.instance;

                        interfaceManager.ToggleCurrentScreenInteractable();
                        interfaceManager.ShowOrHideLoadingIndicator(false, boxLoading, false);
                    }

                    interfaceManager.UpdateCurrentActivePanel();

                }

                , form.data));


            }


        });


    }




    /// <summary> Closes the search panel. </summary>

    public void CloseSearchResults()
    {

        UIRepository uiRepository         = UIRepository.instance;
        InterfaceManager interfaceManager = InterfaceManager.instance;

        //lastSearchEntry = null;
        uiRepository.recipeSearchElements.searchField.text = "";
        uiRepository.recipeSearchElements.searchedRecipesScrollPanel.SetActive(false);

        UITileInfo authoredTileInfo   = uiRepository.uiTiles.authoredRecipesTile.GetComponent<UITileInfo>();
        UITileInfo unauthoredTileInfo = uiRepository.uiTiles.unAuthoredRecipesTile.GetComponent<UITileInfo>();


        if(authoredTileInfo.isSelected)   { uiRepository.recipeListViewer.authoredRecipesScrollPanel.SetActive(true); }
        if(unauthoredTileInfo.isSelected) { uiRepository.recipeListViewer.unauthoredRecipesScrollPanel.SetActive(true); }


        InterfaceManager.instance.UpdateCurrentActivePanel();

    }




    ///<summary> Parses the server response against the user search query and loads the search results into the search results scroll panel.
    ///<para>    The parameter "authoredOnly" dictates whether to load the recipes with the spam button or not. </para>
    /// </summary>

    private IEnumerator LoadSearchResults(string recipes , bool authoredOnly)
    {


        yield return new WaitForSeconds(0);

        string[] rows = Regex.Split(recipes, SystemServices.instance.regexPatterns.tableRowsDelimitter, RegexOptions.Compiled);     // Rows will be one more than the actual rows.


        for (int a = 0; a < rows.Length - 1; a++)
        {

            string row = rows[a];

            string[] columns = Regex.Split(row, SystemServices.instance.regexPatterns.tableColumnsDelimitter, RegexOptions.Compiled);
            string[] instructions = Regex.Split(columns[6], SystemServices.instance.regexPatterns.recipesStepsDelimitter, RegexOptions.Compiled);
            string[] ingredients = Regex.Split(columns[8], SystemServices.instance.regexPatterns.recipesStepsDelimitter, RegexOptions.Compiled);

            string title = columns[2];
            bool isPreloaded = false;
            int preparationTime = int.Parse(columns[4]);
            int id = int.Parse(columns[0]);
            string authorName = columns[3];
            DateTime? date = DateTime.ParseExact(columns[5].Split('.')[0], "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

            Recipe recipe = new Recipe(instructions, ingredients, title, isPreloaded, preparationTime, id, authorName, date);


            GameObject recipeTile = Instantiate(UIRepository.instance.recipeListViewer.recipeTilePrefab);

            recipeTile.transform.GetChild(0).GetComponent<Text>().text = recipe.title.ToUpper();
            recipeTile.transform.GetChild(1).GetChild(0).GetComponent<Text>().text = recipe.authorName.ToUpper();
            recipeTile.transform.GetChild(2).GetChild(0).GetComponent<Text>().text = recipe.date.Value.ToString("yyyy-MM-dd") + "\n" + recipe.date.Value.ToString("HH:mm");
            recipeTile.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = recipe.preparationTime + " m";

            // Set this recipe tile to point to the corresponding recipe object
            recipeTile.GetComponent<RecipePointer>().recipePointer = recipe;


            Button recipeTileButton = recipeTile.GetComponentsInChildren<Button>()[0];
            Button loadAudioPlayer = recipeTile.GetComponentsInChildren<Button>()[1];
            Button reportSpamButton = recipeTile.GetComponentsInChildren<Button>()[2];

            // When the recipe tile is clicked it should go to view that recipe
            recipeTileButton.onClick.AddListener(() => { InterfaceManager.instance.ToggleRecipeViewerPanel(recipeTile); });

            loadAudioPlayer.onClick.AddListener(() =>
            {
                GetComponent<AudioPlayer>().LoadForRecipe(recipe);
            });


            // The spam button should only show in unAuthored recipes
            if(!authoredOnly)
            {
                // When the report spam button is clicked it should report spam against that recipe
                reportSpamButton.onClick.AddListener(() =>
                {
                    SpamManager.instance.ReportSpam(recipe, (bool? successfullyReported) =>
                    {
                        // If spam was successfully reported from this button click then make it uninteractable so that the user can't report spam against the same recipe multiple times.
                        if (successfullyReported == true) { reportSpamButton.interactable = false; }

                        // If the user had already reported spam against this recipe so make the button uninteractable.
                        if (successfullyReported == null) { reportSpamButton.interactable = false; }
                    });

                });
            }

            else
            {
                reportSpamButton.gameObject.SetActive(false);
            }


            InterfaceManager.instance.RecalculatePrefferedDimentions(recipeTile);
            recipeTile.transform.SetParent(UIRepository.instance.recipeSearchElements.searchedRecipesListView.transform, false);
            updateListsAnchors = true; 
           

        }


        InterfaceManager.instance.ToggleCurrentScreenInteractable(true);
        InterfaceManager.instance.ShowOrHideLoadingIndicator(false, UIRepository.instance.onScreenIndicators.blobLoading, false);

    }




    
    protected override void OnAwake()
    {
        
    }
}
