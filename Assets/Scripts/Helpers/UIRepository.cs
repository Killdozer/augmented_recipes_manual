﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;
using UnityEngine.Video;

/// <summary>
/// This class contains grouped references for various interface elements in the form of structures.
/// </summary>


public class UIRepository : Singleton<UIRepository>  {





    protected override void OnAwake()
    {

    }





    [System.Serializable]

    public struct VideoControlsGroup
    {

        public GameObject  controlsPanel;
        public Slider      seekBar;
        public Slider      volumeSlider;
        public Button      playButton;
        public Button      pauseButton;
        public Button      forwardButton;
        public Button      rewindButton;
        public Button      hidePanelButton;
        public Button      muteButton;
        public Button      unmuteButton;
        public GameObject  showPositionMarker;
        public GameObject  hidePositionMarker;
        public Text        time;
        
    }



    [System.Serializable]

    public struct AudioControlsGroup
    {

        public Button playButton;
        public Button stopButton;
        public Button nextButton;
        public Button previousButton;
        public Button repeatButton;
        public Sprite repeatCheckedSprite;
        public Sprite repeatUncheckedSprite;
        public UI_Knob volumeKnob;
        public Slider speedSlider;
        public Slider delaySlider;
        public Slider pitchSlider;
        public Text recipeTitle;
        public Text instructionLabel;

    }



    [System.Serializable]

    public struct InputFieldsGroup
    {

        public InputField searchMeaning;

    }



    [System.Serializable]

    public struct OCRScreenElements
    {
        public RawImage cameraFeed;
        public Image capturedPhoto;
        public Image croppedSelection;
        public Button capturePhoto;
        public Button cropSelection;
        public Button regionOfInterest;
        public Button roiStretchVertically;
        public Button roiStretchHorizontally;
        public Text detectedText;
        public Button searchDetectedText;
        public Button TouchToFocus;
    }




    [System.Serializable]

    public struct RegistrationForm
    {

        public InputField nameField;
        public InputField emailField;
        public Button registerButton;

    }



    [System.Serializable]

    public struct OnScreenIndicatorsGroup
    {
        public Canvas onScreenIndicatorsCanvas;
        public Text onScreenMessage;
        public Animator ringLoading;
        public Animator boxLoading;
        public Animator blobLoading;

    }



    [System.Serializable]

    public struct UITiles
    {
        public GameObject startCookingTile;
        public GameObject viewRecipesTile;
        public GameObject shareRecipesTile;
        public GameObject dictionaryTile;
        public GameObject authoredRecipesTile;
        public GameObject unAuthoredRecipesTile; 
    }




    [System.Serializable]

    public struct RecipesListViewer
    {
        public GameObject unauthoredRecipesListView;
        public GameObject unauthoredRecipesScrollPanel;
        public GameObject authoredRecipesListView;
        public GameObject authoredRecipesScrollPanel;
        public GameObject recipeTilePrefab;
        public GameObject loadMoreButtonContainerPrefab;
    }



    [System.Serializable]

    public struct RecipeViewerPanel
    {
        public GameObject instructionPrefab;
        public GameObject procedureHeadingPrefab;
        public GameObject recipeHeaderPrefab;
        public GameObject recipeViewerPanel;
        public GameObject recipeViewerScrollPanel;
        public GameObject recipeViewerListView;
    }





    [System.Serializable]

    public struct UsefulPrefabs
    {
        public GameObject topGapPrefab;
    }



    [System.Serializable]

    public struct SearchControlsAndPanels
    {
        public GameObject searchedRecipesScrollPanel;
        public GameObject searchedRecipesListView;
        public InputField searchField;
        public Button searchButton;
        public Button closeSearchButton;
    }



    [System.Serializable]

    public struct DictionaryElements
    {
        public Text wordTitle;
        public Image[] wordImages;
        public Button nextImageButton;
        public Button prevImageButton;
        public Button pronunciationButton;
        public Text etymologies;
        public ScrollRect wordMeaningsScrollPanel;
        public Text wordMeaningsTextArea;
        public InputField searchField;
        public Button searchButton;
        public Button closeSearchButton;
    }




    [System.Serializable]

    public struct ModalWindowsElements
    {
        public GameObject window;
        public GameObject uninteractablePanel;
        public Image iconLabel;
        public Button positiveButton;
        public Button negativeButton;
        public Button closeWindowButton;
        public Text message;
        public Text title;
        public Sprite warningIcon;
        public Sprite infoIcon;
        public Sprite errorIcon;
    }




    [System.Serializable]

    public struct ShareEditRecipeElements
    {
        public GameObject  recipeScrollPanel;
        public GameObject  recipeListView;
        public GameObject  recipeHeader2Prefab;
        public GameObject  editableInstructionPrefab;
        public GameObject  procedureHeading2Prefab;
        public GameObject  addMoreButtonPrefab; 
        public GameObject  shareButtonPrefab;
        public GameObject  updateAndDeletePrefab;
    }





    /// <summary> Contains references to the UI screens in the application.  </summary>
    [Tooltip("Define a relationship between each screen and its backward transitionable screen")]   
    public UIStates.Screen[] screens = new UIStates.Screen[Enum.GetValues(typeof(UIStates.ScreenSpace)).Length];
   
                                                      /// <summary> Contains references to the UI tiles in the application.  </summary>
    [Tooltip("Set references to the UI tiles.")]
    public UITiles uiTiles;

                                                      /// <summary> Contains references to the objects that constitute the recipes list viewer panel.</summary>
    [Tooltip("Set references to the objects that constitute the recipes list viewer panel.")]
    public RecipesListViewer recipeListViewer;

                                                      /// <summary> Contains references to the objects that constitute the recipe viewer panel.</summary>
    [Tooltip("Set references to the objects that constitute the recipe viewer panel.")]
    public RecipeViewerPanel recipeViewerPanel;

                                                      /// <summary> Contains references to the objects that are invloved in user searches and viewing the search results.</summary>
    [Tooltip("Set references to the objects that are invloved in user searches and viewing the search results.")]
    public SearchControlsAndPanels recipeSearchElements;

    /// <summary> Contains references to the objects that constitute the dictionary panel.</summary>
    [Tooltip("Set references to the objects that constitute the dictionary panel.")]
    public DictionaryElements dictionaryElements;
    
                                                      /// <summary> Contains references to the UI controls for the video player.  </summary>
    [Tooltip("Set references to the UI controls for the video player.")] 
    public VideoControlsGroup videoControls;
    
                                                      /// <summary> Contains references to the UI controls for the audio player.  </summary>
    [Tooltip("Set references to the UI controls for the audio player.")] 
    public AudioControlsGroup audioControls;

                                                      /// <summary> Contains references to the UI input fields in the application.  </summary>
    [Tooltip("Set references to the UI input fields in the application.")]
    public InputFieldsGroup inputFields;

                                                      /// <summary> Contains references to the interface elements on the ocr screen.  </summary>
    [Tooltip("Set references to the interface elements on the ocr screen.")]
    public OCRScreenElements ocrScreenElements;
 
                                                      /// <summary> Contains references to the UI elements that indicate different messages.  </summary>
    [Tooltip("Set references to the UI elements that display different onscreen messages.")]
    public OnScreenIndicatorsGroup onScreenIndicators;

                                                      /// <summary> Contains references to some of the useful prefabs reused in the application.  </summary>
    [Tooltip("Set references to some of the useful prefabs reused in the application.")]
    public UsefulPrefabs usefulPrefabs;
  
                                                      /// <summary> Contains references to the registration form elements.  </summary>
    [Tooltip("Set references to the registration form elements.")]
    public RegistrationForm registrationForm;

                                                      /// <summary> Contains references to the modal window elements. </summary>
    [Tooltip("Set references to the modal window elements.")]
    public ModalWindowsElements modalWindowElements;

                                                      /// <summary>references to the recipe edit and sharing screen's elements. </summary>
    [Tooltip("Set references to the recipe edit and sharing screen's elements.")]
    public ShareEditRecipeElements shareAndEditRecipeElements;
  
  


}
