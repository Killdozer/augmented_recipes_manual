﻿

/// <summary>
/// This is a container only class which contains an enum type for all possible application behaviour/interface generated events.
/// </summary>


public static class AppEvents

{

    
    /// <summary> All defined application events. </summary>

    public enum events: byte {

                                /// <summary>  Event indicates that a trackable object is found.  </summary>
    TRACKABLE_FOUND = 0,        
                                /// <summary>  Event indicates that a previously found trackable object is now lost.  </summary> 
    TRACKABLE_LOST,
                                /// <summary>  Event indicates that the user has been banned.  </summary>  
    USER_BANNED,


    // Interface events 
                                /// <summary>  Event indicates that the user successfully navigated back through the interface.  </summary>
    NAVIGATE_BACK,                
                                /// <summary>  Event indicates that the user chose the back button on their device.  </summary> 
    BACK_BUTTON,
                                /// <summary>  Event indicates that the user chose the play speech option.  </summary> 
    PLAY_SPEECH,
                                /// <summary>  Event indicates that the user chose the stop speech option.  </summary> 
    STOP_SPEECH

                             };



}