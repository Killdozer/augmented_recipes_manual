﻿using UnityEngine;
using UnityEngine.EventSystems;



/// <summary> This script can be attached to a UI element to make it draggable within a specified RectTransform. </summary>

public class DragableUI : UIBehaviour, IBeginDragHandler, IDragHandler
{
    /// <summary>
    /// The RectTransform that we are able to drag around.
    /// if null: the transform this Component is attatched to is used.
    /// </summary>
    public RectTransform dragObject;

    /// <summary>
    /// The area in which we are able to move the dragObject around.
    /// if null: canvas is used
    /// </summary>
    public RectTransform dragArea;

    private Vector2 originalLocalPointerPosition;
    private Vector3 originalPanelLocalPosition;
    private Vector3[] corners1;
    private Vector3[] corners2;
    private Vector3 prevPosition;
    private Vector3 updatedPos;

    private RectTransform dragObjectInternal
    {
        get
        {
            if (dragObject == null)
                return (transform as RectTransform);
            else
                return dragObject;
        }
    }

    public RectTransform dragAreaInternal
    {
        get
        {
            if (dragArea == null)
            {
                RectTransform canvas = transform as RectTransform;
                while (canvas.parent != null && canvas.parent is RectTransform)
                {
                    canvas = canvas.parent as RectTransform;
                }
                return canvas;
            }
            else
                return dragArea;
        }
    }



    public void OnBeginDrag(PointerEventData data)
    {
        
        if (!(data.selectedObject == this.gameObject)) { return; }  
        originalPanelLocalPosition = dragObjectInternal.localPosition;
        RectTransformUtility.ScreenPointToLocalPointInRectangle(dragAreaInternal, data.position, data.pressEventCamera, out originalLocalPointerPosition);
    }

    public void OnDrag(PointerEventData data)
    {


        if (!(data.selectedObject == this.gameObject)) { return; }
        Vector2 localPointerPosition;

        prevPosition = dragObjectInternal.localPosition;

        if (RectTransformUtility.ScreenPointToLocalPointInRectangle(dragAreaInternal, data.position, data.pressEventCamera, out localPointerPosition))
        {
            Vector3 offsetToOriginal = localPointerPosition - originalLocalPointerPosition;
            updatedPos = originalPanelLocalPosition + offsetToOriginal;
            dragObjectInternal.localPosition = updatedPos;
        }


        //ClampToArea();
    }



    private void OnGUI()
    {


        corners1 = new Vector3[4];
        dragObjectInternal.GetWorldCorners(corners1);

        corners2 = new Vector3[4];
        dragAreaInternal.GetWorldCorners(corners2);

        if (corners1 == null || corners2 == null) { return; }

        if     (corners1[0].x < corners2[0].x)  { dragObjectInternal.localPosition = prevPosition; }
        else if(corners1[1].x < corners2[1].x)  { dragObjectInternal.localPosition = prevPosition; }
        else if(corners1[2].x > corners2[2].x)  { dragObjectInternal.localPosition = prevPosition; }
        else if(corners1[3].x > corners2[3].x)  { dragObjectInternal.localPosition = prevPosition; }

        if      (corners1[0].y < corners2[0].y) { dragObjectInternal.localPosition = prevPosition; }
        else if (corners1[1].y > corners2[1].y) { dragObjectInternal.localPosition = prevPosition; }
        else if (corners1[2].y > corners2[2].y) { dragObjectInternal.localPosition = prevPosition; }
        else if (corners1[3].y < corners2[3].y) { dragObjectInternal.localPosition = prevPosition; }

        InterfaceManager.AnchorsToCorners(dragObjectInternal);

    }



    // Clamp panel to dragArea
    private void ClampToArea()
    {

        Vector3 pos = dragObjectInternal.localPosition;


        Vector3 minPosition = dragAreaInternal.rect.min - dragObjectInternal.rect.min;
        Vector3 maxPosition = dragAreaInternal.rect.max - dragObjectInternal.rect.max;

        pos.x = Mathf.Clamp(dragObjectInternal.localPosition.x, minPosition.x, maxPosition.x) ;
        pos.y = Mathf.Clamp(dragObjectInternal.localPosition.y, minPosition.y, maxPosition.y);

        dragObjectInternal.localPosition = pos;

    }
}