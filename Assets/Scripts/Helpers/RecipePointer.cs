﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary> A helper class used to map a recipe tile to the corresponding entry in unauthored or authored recipes dictionary in RecipeManager class. </summary>

public class RecipePointer : MonoBehaviour {

    /// <summary> The actual recipe object that this recipe tile represents. </summary>

    public Recipe recipePointer;

}
