﻿/// <summary>
/// This is a container only class which defines enumerations for different system types and behaviours.
/// </summary>


public static  class SystemTypes

{

    /// <summary>  Defines possible application media types.  </summary>

    public enum MediaType : byte
    {
                                                /// <summary>  Identifies a video media.       </summary>
        Video = 0,
                                                /// <summary>  Identifies an audio media.      </summary>   
        Audio,
                                                /// <summary>  Identifies an animation media.  </summary>  
        Animation                           

    };





    /// <summary>  Defines the types of animations used in the application.  </summary>

    public enum AnimationType : byte
    {
                                                /// <summary>  Identifies a simple transform based keyframe animation.       </summary>
        SimpleKeyFrame = 0,
                                                /// <summary>  Identifies a complex point or vertex level animation.    </summary>   
        PointOrVertex,
                                                /// <summary>  Identifies a mixture of transform and point or vertex based animation.  </summary>  
        Mixed
                                       
    };




    /// <summary>  Defines the categories of target markers used in the application.Used to associate appropriate behaviour with a target marker. </summary>

    public enum TargetType : byte
    {
                                                /// <summary>  Identifies a recipe scene selection target used to select the appropriate scene. </summary>
        SceneSelectionMarker = 0,
                                                /// <summary>  Identifies a recipe video target used to play a video. </summary>   
        VideoMarker,
                                                /// <summary>  Identifies an instruction target used to play an animation or audio. </summary>  
        Instruction
                                       
    };



}