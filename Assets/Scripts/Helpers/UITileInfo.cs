﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary> This class is attached to the UI tiles and used to identify their type and the skin to use when selected and deselected.</summary>

public class UITileInfo : MonoBehaviour {




    ///<summary>The tile name that identifies this tile</summary>
    [Tooltip("The tile name that identifies this tile")]
    public TileTypes tile;

    ///<summary>Set the skin index in the InterfaceManager class to identify which skin to use with this tile.</summary>
    [Tooltip("The tile name that identifies this tile")]
    public int skinIndex;

    ///<summary>Is the tile currently selected or not.</summary>
    [HideInInspector]
    public bool isSelected;



    /// <summary> An enumeration that identifies all possible UI tile types.  </summary>

    public enum TileTypes : byte

    {

        startCookingTile,
        viewRecipesTile,
        shareRecipesTile,
        dictionaryTile,
        authoredRecipesTile,
        unAuthoredRecipesTile,
        recipeTile
   
    };

}
