﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This class can be used to apply filters on an image, like sharpening, deblurring and black and white filters.Used in preprocessing for OCR.
/// </summary>


public class ImageEffects : MonoBehaviour {




    /// <summary>Returns the black and white version of the specified image in the argument.</summary>
    /// <param name="image">The image of which the black and white version is to be returned.</param>
    /// <param name="blackThreshold">Any pixel value less than or equal to this value will be considered black otherwise it will be considered white.</param>
    /// <returns>The black and white of the specified image.</returns>


      
    public static Texture2D BlackAndWhite(Texture2D image , float blackThreshold = 100)

    {

        Texture2D resultantImage = new Texture2D(image.width , image.height);
        Color32[] pixelArray = image.GetPixels32();



        for(int a = 0; a < pixelArray.Length; a++)
        {

            Color32 pixelColor      = pixelArray[a];
            float averageColorValue = (pixelColor.r + pixelColor.g + pixelColor.b) / 3f;

            if(averageColorValue <= blackThreshold) { pixelArray[a] = Color.black; }
            else { pixelArray[a] = Color.white; }

        }


        resultantImage.SetPixels32(pixelArray);
        resultantImage.Apply();
        return resultantImage;

    }
    

}
