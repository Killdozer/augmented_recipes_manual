﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputFieldEvents : MonoBehaviour ,ISelectHandler, IDeselectHandler
{

    private InputField thisField;

	// Use this for initialization
	void Start () {

        thisField = GetComponent<InputField>();

    }

   
 
    void ISelectHandler.OnSelect(BaseEventData eventData)
    {
        Button deleteButton = transform.parent.GetComponentInChildren<Button>(true);
        deleteButton.gameObject.SetActive(true);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        Button deleteButton = transform.parent.GetComponentInChildren<Button>(true);

        SystemServices.instance.RunDelayedCommand(0.15f , ()=> 
        {
            if(deleteButton) { deleteButton.gameObject.SetActive(false); }
        });
    }
}
