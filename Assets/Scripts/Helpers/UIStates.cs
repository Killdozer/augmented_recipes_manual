﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// Contains all possible states between which the UI can transition.
/// </summary>

public class UIStates : MonoBehaviour {



                                                 /// <summary> A structure that represents a screen by holding it's reference, name and to which screen it can backward transit to. </summary>
    [System.Serializable]

    public struct Screen
    {

        public GameObject  screen;
        public GameObject  crossFadePanel;
        public GameObject  unInteractablePanel;
        public ScreenSpace screenName;
        public ScreenSpace backTransitScreen;

    }




                                                 /// <summary> An enumeration that identifies all possible UI screens.  </summary>

    public enum ScreenSpace : byte

    {

        Home,
        ARScreen,
        UserRegistration,
        AudioPlayer,
        VideoPlayer,
        OCR,
        Dictionary,
        ViewRecipes,
        CreateAndEditRecipes,
        NoScreen,
        OptionsMenu,
        FoodInformatics

    };


                                                     /// <summary> An enumeration that identifies all possible UI panels.  </summary>

    public enum Panels : byte

    {

       VideoPlayer,
       AuthoredRecipesListPanel,
       UnauthoredRecipesListPanel,
       RecipeViewer,
       SearchRecipesPanel,
       None

    };


}
