﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




/// <summary>
/// An instance of this class represents a trackable target.
/// </summary>


public class TargetInfo : MonoBehaviour {





                                                                          ///<summary> Unique identity of this target.</summary>
    public int targetID                       { private set; get; }
	                                                                      ///<summary> Name of this target as defined in the targets database.Might not be unique. </summary>
    public string targetName                  { private set; get; }
        	                                                              ///<summary> The instruction if any associated with this target marker. </summary>
    public string instruction                 { private set; get; }
            	                                                          ///<summary> The GameObject to which this target marker is parented. </summary>
    public GameObject rootObject              { private set; get; }
	                                                                      ///<summary> The system defined type of this target marker. </summary>
    public SystemTypes.TargetType targetType  { private set; get; }
	                                                                      ///<summary> The type of animation if any associated with this target marker. </summary>
    public SystemTypes.AnimationType animType { private set; get; }






	void Start () {
		
      
        // Must initialize all the private properties here at startup.

	}
	

}
