﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

/// <summary>
/// This class contains extension methods to add additional functions on top of unity classes.
/// </summary>
public static class ExtensionMethods
{


    /// <summary> Traverse up the hierarchy to find first parent of this gameObject with the specified tag. </summary>

    public static GameObject FindFirstParentWithTag(this GameObject childObject, string tag)
    {
        Transform t = childObject.transform;

        while (t.parent != null)
        {
            if (t.parent.tag == tag)
            {
                return t.parent.gameObject;
            }
            t = t.parent.transform;
        }

        return null; // Could not find a parent with given tag.
    }





    /// <summary>Finds and returns all the components of the type specified within the children of this gameObject that have the the specified tag. If "includeInactive" is true then inActive children will also be searched. </summary>

    public static T[] FindComponentsInChildrenWithTag<T>(this GameObject parent, string tag, bool includeInactive = false) where T : Component
    {

        if (parent == null) { throw new System.ArgumentNullException(); }

        if (string.IsNullOrEmpty(tag) == true) { throw new System.ArgumentNullException(); }

        List<T> list = new List<T>(parent.GetComponentsInChildren<T>(includeInactive));

        if (list.Count == 0) { return null; }

        for (int i = list.Count - 1; i >= 0; i--)
        {
            if (list[i].CompareTag(tag) == false)
            {
                list.RemoveAt(i);
            }
        }

        return list.ToArray();
    }




    /// <summary> Returns a new color object from this color. </summary>

    public static Color32 CreateClone(this Color32 color)
    {
        Color32 newColor = new Color32(color.r, color.g , color.b , color.a);
        return newColor;
    }




    /// <summary> Sets the request headers from a dictionary object</summary>

    public static void SetRequestHeaders(this UnityWebRequest uwr, Dictionary<string, string> headers)
    {
        
        if(headers == null || headers.Count == 0) { throw new System.ArgumentNullException(); }   

        foreach(KeyValuePair<string,string> header in headers)
        {
            uwr.SetRequestHeader(header.Key, header.Value);
        }

    }


}