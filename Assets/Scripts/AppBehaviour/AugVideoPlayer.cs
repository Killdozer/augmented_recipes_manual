﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Video;



/// <summary>
/// This class is used to play the video associated with a recipe and provides mechanisms to alter the video playback behaviour. 
/// </summary>



public class AugVideoPlayer : AugmentedMediaBehaviour {





                                                  [Tooltip("The speed with which to forward or rewind.")]                                                                                                                                                                                           
    public float seekSpeed = 10f;     
                                                  /// <summary> Reference to the video player component that will play the video. </summary>                                                                                           
    public VideoPlayer videoPlayer { get; set; }
                                                  /// <summary> Is the video player currently muted. </summary>                                                                                           
    [HideInInspector]
    public bool isMuted { get; private set; }           
                                                  /// <summary> The current volume of the video player. </summary>                                                                                           
    [HideInInspector]
    public float volume { get; private set; }           
                                                  /// <summary> Is the videoPlayer currently buffering. </summary>                                                                                           
    private bool isBuffering;
                                                  /// <summary> Indicates whether to rewind the video or not. </summary>                                                                                           
    private bool rewind;
                                                  /// <summary> Indicates whether to forward the video or not. </summary>                                                                                           
    private bool forward;
                                                  /// <summary> The time by which to forward or rewind the video. </summary>                                                                                           
    private double seekTime;







    public AugVideoPlayer(VideoPlayer videoPlayer)
    {
        SetVideoPlayer(videoPlayer);
    } 






    private void Awake()
    {

        this.mediaType = SystemTypes.MediaType.Video;
        //if(videoPlayer == null) { videoPlayer = UIRepository.instance.videoControls.videoPlayer; }
        SetVideoPlayer(videoPlayer);
        DisableOrEnableVideoControls(false);
    }





    ///<summary>Set the videoPlayer reference and event callbacks, and set the video controls to default values.</summary>
   
    public void SetVideoPlayer(VideoPlayer videoPlayer)
    {
     
        if(videoPlayer == null) { Debug.LogWarning("Null reference passed to SetVideoPlayer"); return; }
        this.videoPlayer = videoPlayer;
        //videoPlayer.sendFrameReadyEvents = true;    //cpu tax!
        //videoPlayer.frameReady += FrameReady;
        videoPlayer.time = 0;
        UIRepository.instance.videoControls.time.text = "0:00:00" + " / " + "0:00:00";
        UIRepository.instance.videoControls.seekBar.Set((float)videoPlayer.time, false);

        videoPlayer.prepareCompleted -= VideoPrepared;          videoPlayer.prepareCompleted += VideoPrepared;   //baw added
        videoPlayer.errorReceived -= VideoPlayerError;          videoPlayer.errorReceived += VideoPlayerError;
        videoPlayer.loopPointReached -= LoopPointReached;       videoPlayer.loopPointReached += LoopPointReached;
        videoPlayer.started -= StartedPlayBack;                 videoPlayer.started += StartedPlayBack;
        videoPlayer.clockResyncOccurred -= ClockResyncOccurred; videoPlayer.clockResyncOccurred += ClockResyncOccurred;
        videoPlayer.seekCompleted -= SeekCompleted;             videoPlayer.seekCompleted += SeekCompleted;
    }




    private void Update()
    {

        InterfaceManager interfaceManager = InterfaceManager.instance;


        if (isBuffering && interfaceManager.bufferingIndicatorHidden)
        {
            //Debug.Log("Buffering");
            //interfaceManager.ShowOrHideLoadingIndicator(true  , UIRepository.instance.onScreenIndicators.ringLoading , true);
            interfaceManager.ShowOrHideLoadingIndicator(true, UIRepository.instance.onScreenIndicators.boxLoading, true);
        }

        else if(!isBuffering && !interfaceManager.bufferingIndicatorHidden)
        {
            //interfaceManager.ShowOrHideLoadingIndicator(false , UIRepository.instance.onScreenIndicators.ringLoading , true);
            interfaceManager.ShowOrHideLoadingIndicator(false , UIRepository.instance.onScreenIndicators.boxLoading , true);
        }



        if (forward)
        {
            seekTime += (Time.deltaTime * seekSpeed);
        }

        else if(rewind)
        {
            seekTime -= (Time.deltaTime * seekSpeed);
        }


        if(IsCurrentlyPlaying())
        {
            
            TimeSpan timeSpan = TimeSpan.FromSeconds(videoPlayer.time);
            string time = timeSpan.ToString(@"hh\:mm\:ss"); 
            string totalTime = UIRepository.instance.videoControls.time.text.Split('/')[1];
            UIRepository.instance.videoControls.time.text = time + " /" + totalTime;
            UIRepository.instance.videoControls.seekBar.Set((float)videoPlayer.time , false);
            
        }
        
    }


    /*
    internal class MySlider : Slider
    {

        public void SetValue(float value, bool callback)
        {
            Set(value, callback);
        }
    }
    */



    ///<summary> Forward video playback. </summary>

    public override void Forward()
    {
        //if(!IsCurrentlyPlaying()) { return; }
        isBuffering = true;
        forward = true;
        rewind  = false;
        seekTime = videoPlayer.time;
        videoPlayer.Pause();
    }





    ///<summary> Rewind video playback. </summary>

    public override void Rewind()
    {
        //if (!IsCurrentlyPlaying()) { return; }
        isBuffering = true;
        rewind = true;
        forward = false;
        seekTime = videoPlayer.time;
        videoPlayer.Pause();
    }





    ///<summary> Listens to the event when the forward button is released after being held down. </summary>

    public void ForwardButtonReleased()
    {       
        //if (!IsCurrentlyPlaying()) { return; }
        forward = false;
        videoPlayer.time = seekTime;
    }





    ///<summary> Listens to the event when the rewind button is released after being held down. </summary>

    public void RewindButtonReleased()
    {
        //if (!IsCurrentlyPlaying()) { return; }
        rewind = false;
        videoPlayer.time = seekTime;
    }






    ///<summary> Seek to a specific location in the video. </summary>

    public void SeekVideo()
    {
        //if (!IsCurrentlyPlaying()) { return; }  
        videoPlayer.time = UIRepository.instance.videoControls.seekBar.value;
        //Debug.Log("Seek to  "+ UIRepository.instance.videoControls.seekBar.value);
    }





    ///<summary> Start video playback. </summary>

    public override void PlayMedia()
    {

        if (videoPlayer.isPrepared) { videoPlayer.Play(); }

        else {

            videoPlayer.prepareCompleted -= VideoPrepared;
            videoPlayer.prepareCompleted += VideoPrepared;
            videoPlayer.Prepare();

             }

    }




    ///<summary> Pause video playback. </summary>

    public override void PauseMedia()
    {
        UIRepository.instance.videoControls.pauseButton.gameObject.SetActive(false);
        UIRepository.instance.videoControls.playButton.gameObject.SetActive(true);
        videoPlayer.Pause();
    }





    ///<summary> Show video playback options. If the parameter passed is false then the playback options will be hidden. </summary>

    public override void ShowPlayBackOptions(bool show)
    {
        if (InterfaceManager.instance) { InterfaceManager.instance.ShowVideoControls(show); }
    }




    ///<summary> Disables and make the video control panel invisible or visible depending on the argument passed </summary>

    public void DisableOrEnableVideoControls(bool enable)
    {
        if (enable)
        {
            //UIRepository.instance.videoControls.hidePanelButton.interactable = true;
            UIRepository.instance.videoControls.hidePanelButton.gameObject.SetActive(true);
        }
        else
        {
            //UIRepository.instance.videoControls.hidePanelButton.interactable = false;
            UIRepository.instance.videoControls.hidePanelButton.gameObject.SetActive(false);
        }
    }





    ///<summary> Returns true if the video is currently playing.Note that during a buffering event this method might return true at first and then fall back to false. </summary>

    public override bool IsCurrentlyPlaying()
    {
        if (isBuffering) { return false; }
        return videoPlayer.isPlaying;  
    }








    ///<summary> Listens for the event when a new frame is ready. </summary>

    private void FrameReady(VideoPlayer player, long frameIdx)
    {
        Debug.Log("Frame ready");
    }





    ///<summary> Listens for the event when the VideoPlayer preparation is complete. </summary>

    private void VideoPrepared(VideoPlayer player)
    {

        //Debug.Log("Someone tried to prepare video.");

        if (videoPlayer)
        {

             float videoLength = (videoPlayer.frameCount / videoPlayer.frameRate);
             UIRepository.instance.videoControls.seekBar.maxValue = videoLength;
             TimeSpan timeSpan = TimeSpan.FromSeconds(videoLength);
             string time = timeSpan.ToString(@"hh\:mm\:ss");
             UIRepository.instance.videoControls.time.text = "00:00:00 / " + time;

        }

        else
        {
            Debug.Log("VideoPlayer is not referenced.");
        }

        videoPlayer.prepareCompleted -= VideoPrepared;

        if (!RecipeManager.instance.isBackgroundBuffering) { videoPlayer.Play(); }    // Don't play video when the recipe video is initially just loaded by the recipe manager.

        RecipeManager.instance.isBackgroundBuffering = false;

    }





    ///<summary> Listens for the event when Errors such as HTTP connection problems are received. </summary>

    private void VideoPlayerError(VideoPlayer player, string error)
    {
        PauseMedia();
        Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
        InterfaceManager.instance.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
    }




    
    ///<summary> Listens for the event when the video has reached the end. </summary>

    private void LoopPointReached(VideoPlayer player)
    {
        UIRepository.instance.videoControls.pauseButton.gameObject.SetActive(false);
        UIRepository.instance.videoControls.playButton.gameObject.SetActive(true);
        Debug.Log("Loop point reached");
    }





    ///<summary> Listens for the event when the video has started playing the video. </summary>

    private void StartedPlayBack(VideoPlayer player)
    {
        UIRepository.instance.videoControls.pauseButton.gameObject.SetActive(true);
        UIRepository.instance.videoControls.playButton.gameObject.SetActive(false);
        Debug.Log("Started video playback");
    }






    ///<summary> Listens for the event when the video has started playing the video. </summary>

    private void ClockResyncOccurred(VideoPlayer player, double sec)
    {
        Debug.Log("Resync occurred");
    }





    ///<summary> Listens for the event when the video seek operation gets completed. </summary>

    private void SeekCompleted(VideoPlayer player)
    {
        isBuffering = false;
        UIRepository.instance.videoControls.seekBar.Set((float)videoPlayer.time , false);
        if (!IsCurrentlyPlaying()) videoPlayer.Play();
    }





    ///<summary> Listens for the event when the volume control slider is interacted with.</summary>

    public void ChangeVolume()
    {


        Slider volumeSlider = UIRepository.instance.videoControls.volumeSlider;
        float volume        = volumeSlider.value;


        if(volume == 0)
        {
           Mute(false);
        }

        else
        {
            if(isMuted)
            {
                Mute(true);
            }
        }

        if (videoPlayer)
        {
            videoPlayer.gameObject.GetComponent<AudioSource>().volume = volume;
            this.volume = volume;
        }

        else { Debug.LogWarning("Can't change volume, video player reference not set"); }
    }







    ///<summary> Mute or unmute the video player.if the unmute argument is true the video player will be unmuted, otherwise it will be muted.</summary>

    public void Mute(bool unmute)
    {

        
        Slider volumeSlider = UIRepository.instance.videoControls.volumeSlider;


        if (!unmute)
        {
            videoPlayer.gameObject.GetComponent<AudioSource>().volume = 0;
            this.volume = 0;
            //volumeSlider.value = 0;
            volumeSlider.Set(0 , false);
            UIRepository.instance.videoControls.muteButton.gameObject.SetActive(false);
            UIRepository.instance.videoControls.unmuteButton.gameObject.SetActive(true);
            isMuted = true;
        }

        else
        {
            videoPlayer.gameObject.GetComponent<AudioSource>().volume = 1;
            this.volume = 1;
            //volumeSlider.value = (volumeSlider.value == 0)? 1: volumeSlider.value;
            volumeSlider.Set(((volumeSlider.value == 0) ? 1 : volumeSlider.value), false);
            UIRepository.instance.videoControls.muteButton.gameObject.SetActive(true);
            UIRepository.instance.videoControls.unmuteButton.gameObject.SetActive(false);
            isMuted = false;
        }
   
    }




}
