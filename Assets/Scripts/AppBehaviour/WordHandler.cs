﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This class is used to initiate the meaning inquiry process for a word by initializing a dictionary class instance.It also contains methods for optical character recognition.
/// </summary>

public class WordHandler : MonoBehaviour
{


    /// <summary> The base URL to the OCR Web API.</summary>
    public readonly string ocrBaseUrl;
    /// <summary> This is the key used for authentication of requests made to the OCR server.</summary>
    public readonly string apiKey;
    /// <summary> The minimum image resolution that microsoft OCR service expects.If the resolution of the cropped image is lower than this resolution, then this resolution can be passed to the CropPhoto() to append gray pixels to compensate with low resolution.</summary>
    [Tooltip("Set the minimum image resolution that microsoft OCR service expects.If the resolution of the cropped image is lower than this resolution, then this resolution can be passed to the CropPhoto() to append gray pixels to compensate with low resolution. ")]
    [SerializeField]
    public CameraServices.Resolution minDimentions;
    /// <summary> The word that was detected by OCR process.</summary>
    private string detectedWord;
    /// <summary> The image that will be decoded by the OCR process.</summary>
    private Texture2D textureToDecode;
    /// <summary> The text parsed by the online OCR service.</summary>
    private string parsedText;
    /// <summary> The dictionary object that contains information for the current inquiry word.</summary>
    private Dictionary dictionary;
    /// <summary> The last successfully searched word.</summary>
    private string lastSearchedEntry = null;
    /// <summary> Flag indicates whether the user has captured a photo or not.</summary>
    private bool photoCaptured;
    /// <summary> Flag indicates whether the user has cropped a selection or not.</summary>
    private bool regionCropped;
    /// <summary> Flag indicates whether the user has changed his last cropped selection or not.</summary>
    private bool sameCroppedRegion;
    /// <summary> Indicates how many images have been successfully loaded for the current searched word.</summary>
    private int successfullyLoadedImages;
    /// <summary> Indicates which image the user currently is viewing.</summary>
    private int currentImagePointer;
    /// <summary> Indicates whether the word images are currently being loaded or not.</summary>
    private bool areImagesLoading;



    private bool updateAnchors;

    public Texture2D testTexture;

    public WordHandler()
    {
        ocrBaseUrl = "https://api.ocr.space/parse/image";
        apiKey = "57324571ac88957";
    }




    private void OnGUI()
    {

        // Move the anchors to corners when elements are resized with the addition of new content
        if (Event.current.type == EventType.Layout && updateAnchors)
        {
            UIRepository uiRepository = UIRepository.instance;

            // will be executed only after layout has been re-computed

            InterfaceManager.AnchorsToCorners(uiRepository.dictionaryElements.wordMeaningsScrollPanel.GetComponent<RectTransform>());
            InterfaceManager.AnchorsToCorners(uiRepository.dictionaryElements.wordMeaningsTextArea.GetComponent<RectTransform>());
            InterfaceManager.AnchorsToCorners(uiRepository.dictionaryElements.wordTitle.GetComponent<RectTransform>());
            InterfaceManager.AnchorsToCorners(uiRepository.dictionaryElements.etymologies.GetComponent<RectTransform>());

            // Set the scroll panel to be at the top
            uiRepository.dictionaryElements.wordMeaningsScrollPanel.verticalNormalizedPosition = 1;
            updateAnchors = false;
        }
    }




    /*
    /// <summary> Uploads the texture provided in "textureToDecode parameter to the online service for OCR.If ocr succeeded then the callback is passed the text detected otherwise it is passed null. </summary>

    private void CarryOnlineOCR(Texture2D textureToDecode, Action<string> callback)
    {

        if (!textureToDecode) { Debug.LogError("Warning!, textureToDecode field is null."); callback(null); return; }


        string base64Img = "data:image/jpg;base64," + System.Convert.ToBase64String(textureToDecode.EncodeToJPG());
        ASCIIEncoding encoding = new ASCIIEncoding();
        Byte[] bytes = encoding.GetBytes(base64Img);
        Debug.Log("Base64 encoded image size:  " + bytes.Length / 1000f + " KB");

        SystemServices systemServices = GetComponent<SystemServices>();
        WWWForm form = new WWWForm();
        form.AddField("apikey", apiKey);
        form.AddField("base64Image", base64Img);
        form.AddField("language", "eng");
        



        StartCoroutine(systemServices.AsyncPOSTRequest(ocrBaseUrl, (string response) =>
        {


            if (!Regex.IsMatch(response, SystemServices.instance.regexPatterns.netError) || !Regex.IsMatch(response, SystemServices.instance.regexPatterns.nullOrEmpty))
            {
                //Debug.Log("Server response is:   " + response);
            }


            response = Regex.Replace(response, @SystemServices.instance.regexPatterns.tabsNewLinesQuotes, "");
            Match errorMatch = Regex.Match(response, "\"ErrorMessage\":\\[\"(.+?)\\\"]", RegexOptions.Singleline);
            Match textMatch = Regex.Match(response, "\"ParsedText\":(.+?),", RegexOptions.Singleline);
            string error = "noerror";
            parsedText = "";



            if (errorMatch.Success)
            {
                error = errorMatch.Result("$1");
                Debug.Log(error);
                parsedText = null;
            }

            if (textMatch.Success)
            {
                parsedText = textMatch.Result("$1");
            }




            if (Regex.IsMatch(response, SystemServices.instance.regexPatterns.nullOrEmpty))
            {
                string netError = Regex.Replace(response, SystemServices.instance.regexPatterns.nullOrEmpty, "").ToString();
                Debug.LogWarning(netError);
                parsedText = null;
            }



            else if (Regex.IsMatch(response, SystemServices.instance.regexPatterns.netError) && !Regex.IsMatch(response, SystemServices.instance.regexPatterns.resourceNotFound404))
            {

                string netError = Regex.Replace(response, SystemServices.instance.regexPatterns.netError, "").ToString();
                Debug.LogError("Couldn't communicate with the server because the following network error has occured:  " + "\"" + netError + "\"");
                parsedText = null;
            }




            else if (Regex.IsMatch(response, SystemServices.instance.regexPatterns.resourceNotFound404) || !error.Equals("noerror"))
            {
                Debug.Log(error);
                parsedText = null;
            }


            else
            {

                parsedText = Regex.Replace(parsedText, SystemServices.instance.regexPatterns.tabsNewLinesQuotes, "").Split('\\')[0];
                parsedText = parsedText.Substring(0, parsedText.Length - 1);
                Debug.Log("Detected text is  " + parsedText);
                
            }

            detectedWord = parsedText;
            callback(parsedText);

        }
        , form.data));
        

    }
    */







    /// <summary> Sets the "detectedWord" field to the custom text provided in the inputfield. </summary>

    public void SetWordFromInput()
    {
        detectedWord = UIRepository.instance.inputFields.searchMeaning.text;
    }







    /// <summary> Instantiates a dictionary object and search for the meaning of the detected or typed word. 
    /// <para> If the argument given to "searchByText" parameter is true then the word specified in the searchField will be searched, otherwise the word detected in OCR will be searched.</para>
    /// </summary>

    public void SearchForMeaning(bool searchByText)
    {


        Animator loading = UIRepository.instance.onScreenIndicators.boxLoading;

        InterfaceManager.instance.ToggleCurrentScreenInteractable(false);
        InterfaceManager.instance.ShowOrHideLoadingIndicator(true, loading, false);



        string searchFor = "";



        searchFor = (searchByText) ? UIRepository.instance.dictionaryElements.searchField.text : detectedWord;

        
        if (string.IsNullOrWhiteSpace(searchFor) || string.IsNullOrEmpty(searchFor))
        {
            Debug.Log("Won't search for empty strings");
            InterfaceManager.instance.ToggleCurrentScreenInteractable(true);
            InterfaceManager.instance.ShowOrHideLoadingIndicator(false, loading, false);
            return;
        }

        if (!searchByText) { UIRepository.instance.dictionaryElements.searchField.text = detectedWord; }


        searchFor = searchFor.ToLower();




        if (lastSearchedEntry == searchFor)
        {
            InterfaceManager.instance.ToggleCurrentScreenInteractable(true);
            InterfaceManager.instance.ShowOrHideLoadingIndicator(false, loading, false);
            Debug.Log("Won't search for the same word again");
        }


        else
        {
            UIRepository uiRepository = UIRepository.instance;

            dictionary = GetComponent<Dictionary>();
            dictionary.inquiryWord = searchFor;

            dictionary.SearchMeaning((bool? didSucceed) =>
            {


                Dictionary.WordEntry[] wordEntries = dictionary.wordEntries;

                // If the search failed due to internet failure then didSucceed will be null.

                if (didSucceed == null)
                {
                    InterfaceManager.instance.ToggleCurrentScreenInteractable(true);
                    InterfaceManager.instance.ShowOrHideLoadingIndicator(false, loading, false);
                    Sprite icon = uiRepository.modalWindowElements.warningIcon;
                    InterfaceManager.instance.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                }


                // no meaning found for the inquiry word

                else if (didSucceed == false)
                {
                    lastSearchedEntry = searchFor;
                    InterfaceManager.instance.ToggleCurrentScreenInteractable(true);
                    InterfaceManager.instance.ShowOrHideLoadingIndicator(false, loading, false);
                    Sprite icon = uiRepository.modalWindowElements.infoIcon;
                    InterfaceManager.instance.ShowModalWindow("nothing found", icon, $"No meaning found for the word \"{searchFor}\".", true);
                }


                // one or more meanings were found for the inquiry word

                else
                {


                    // Clear previously loaded word images and parameters
                    Image[] wordImages = uiRepository.dictionaryElements.wordImages;

                    for (int a = 0; a < wordImages.Length; a++)
                    {
                        wordImages[a].sprite = null;

                        // Set the first image to be active
                        if (a == 0) { wordImages[a].gameObject.SetActive(true); }
                        else { wordImages[a].gameObject.SetActive(false); }

                    }

                    successfullyLoadedImages = 0;
                    currentImagePointer = 0;



                    //Load the word images in the background

                    Transform imgTransform = uiRepository.dictionaryElements.wordImages[0].transform;
                    Animator animator = imgTransform.GetChild(imgTransform.childCount - 1).GetComponent<Animator>();

                    InterfaceManager.instance.ShowOrHideLoadingIndicator(true, animator, false, true);


                    CognitiveServices.instance.GetImagesAgainstQuery(searchFor, (string[] imageLinks) =>
                    {

                        if (imageLinks == null)
                        {
                            Debug.Log("Failed to fetch image links");
                            InterfaceManager.instance.ShowOrHideLoadingIndicator(false, animator, false, true);
                        }

                        else { StartCoroutine(LoadWordImages(imageLinks)); }

                    });



                    lastSearchedEntry = searchFor;

                    uiRepository.dictionaryElements.wordTitle.text = searchFor;
                    uiRepository.dictionaryElements.etymologies.text = Dictionary.WordEntry.etymologies;

                    string newText = "";

                    for (int a = 0; a < wordEntries.Length; a++)
                    {


                        Dictionary.WordEntry wordInfo = wordEntries[a];

                        string wordDomain = ((wordInfo.wordDomain != null) ? ($"({wordInfo.wordDomain})") : "").ToLower();


                        if (a > 0) { newText += "\n\n"; }

                        newText += $"<size=22>|  </size><color=#283E53ff><size=21>{searchFor}{wordDomain}</size></color>";
                        newText += $"<color=#008080ff><i>  {wordInfo.wordMeaning}</i></color>";

                        if ((wordInfo.exampleSentence != null))
                        {
                            newText += "\n" + $"<color=#654A50B1><size=15>{wordInfo.exampleSentence}</size></color>";
                        }

                    }


                    uiRepository.dictionaryElements.wordMeaningsTextArea.text = newText;

                    updateAnchors = true;

                    InterfaceManager.instance.ToggleCurrentScreenInteractable(true);
                    InterfaceManager.instance.ShowOrHideLoadingIndicator(false, loading, false);


                }
            });


        }


    }






    /// <summary> Loads the word images given the urls array. </summary>

    private IEnumerator LoadWordImages(string[] links)
    {

        yield return new WaitForSeconds(0);

        UIRepository uiRepository = UIRepository.instance;
        Image[] wordImages = uiRepository.dictionaryElements.wordImages;


        int iterations = (links.Length >= wordImages.Length) ? wordImages.Length : links.Length;
        bool makeFalse = false;

        for (int a = 0; a < iterations; a++ )
        {

            
            Image wordImage = wordImages[a];
            Transform imgTransform = wordImage.transform;
            Animator animator = imgTransform.GetChild(imgTransform.childCount - 1).GetComponent<Animator>();


            //InterfaceManager.instance.ShowOrHideLoadingIndicator(true, animator, false, true);

            // Debug.Log("trying to download:  "+ links[a] + "  and setting it onto  "+ wordImage.name);

            areImagesLoading = true;
            

            if(a == iterations - 1) { makeFalse = true; }

            StartCoroutine(SystemServices.instance.AsyncImageDownload(links[a], (Texture2D texture) =>
            {

                if (texture != null)
                {
                    //Debug.Log("TEX WIDTH:  " + texture.width + "  texture height:  " + texture.height);
                    Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                    wordImage.sprite = sprite;
                    successfullyLoadedImages++;
                }

                else
                {
                    Debug.Log($"Failed to download image from:  \"{links[a]}\"");
                }


                // when the last image is loaded then set the flag to false.
                if(makeFalse)
                {
                    InterfaceManager.instance.ShowOrHideLoadingIndicator(false, animator, false, true);
                    areImagesLoading = false;
                }

                //InterfaceManager.instance.ShowOrHideLoadingIndicator(false, animator, false, true);

            })); 

        }

    }





    /// <summary> Shows the next word image for the searched word if any. </summary>

    public void ShowNextWordImage()
    {

        if(successfullyLoadedImages == 0 || areImagesLoading) { return; }
        Image[] wordImages = UIRepository.instance.dictionaryElements.wordImages;


        if (currentImagePointer == successfullyLoadedImages - 1)
        {
            wordImages[currentImagePointer].gameObject.SetActive(false);
            currentImagePointer = 0;
            wordImages[currentImagePointer].gameObject.SetActive(true);
        }

        else
        {
            wordImages[currentImagePointer].gameObject.SetActive(false);
            currentImagePointer++;
            wordImages[currentImagePointer].gameObject.SetActive(true);
        }
    }





    /// <summary> Shows the previous word image for the searched word if any. </summary>

    public void ShowPrevWordImage()
    {

        if (successfullyLoadedImages == 0 || areImagesLoading) { return; }
        Image[] wordImages = UIRepository.instance.dictionaryElements.wordImages;

        if (currentImagePointer == 0)
        {
            wordImages[currentImagePointer].gameObject.SetActive(false);
            currentImagePointer = successfullyLoadedImages - 1;
            wordImages[currentImagePointer].gameObject.SetActive(true);
        }

        else
        {
            wordImages[currentImagePointer].gameObject.SetActive(false);
            currentImagePointer--;
            wordImages[currentImagePointer].gameObject.SetActive(true);
        }

    }






    /// <summary> Pronounces the searched word. </summary>

    public void PronounceWord()
    {

        string word = UIRepository.instance.dictionaryElements.wordTitle.text;

        SystemServices.instance.textToSpeech.Speak(word, (string msg) =>
        {
            Debug.LogError(msg);
            SystemServices.instance.ShowToast(msg + " Failed to speak.Please enable Text-to-speech output in Settings>Accessibility.", TextToSpeech.ToastLength.LENGTH_LONG);
        });
    }





    /// <summary> Clears the search field. </summary>

    public void ClearSearchField()
    {
        UIRepository.instance.dictionaryElements.searchField.text = ""; 
    }




    /// <summary> Captures the camera shot to the OCR screen's camera feed panel. </summary>

    public void CaptureCameraShotToPanel()
    {

        CameraServices cameraServices = CameraServices.instance;

        if(cameraServices.isPhotoCapturing) { return; }


        if (!photoCaptured)
        {
            InterfaceManager.instance.ToggleCurrentScreenInteractable();
            InterfaceManager.instance.ShowMessage(0.6f, "PLEASE WAIT...", false);


            StartCoroutine(cameraServices.TakePhoto( (Texture2D texture) => 
            {


                Sprite sprite = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
                UIRepository.instance.ocrScreenElements.capturedPhoto.gameObject.SetActive(true);
                UIRepository.instance.ocrScreenElements.cameraFeed.gameObject.SetActive(false);

                //UIRepository.instance.ocrScreenElements.capturedPhoto.preserveAspect = true;   //The cropping algo doesn't work with preserve aspect images

                UIRepository.instance.ocrScreenElements.TouchToFocus.gameObject.SetActive(false);
                UIRepository.instance.ocrScreenElements.capturedPhoto.sprite = sprite;
                photoCaptured = true;
                InterfaceManager.instance.ShowRoi(true);

                InterfaceManager.instance.ShowMessage(0.6f, "PLEASE WAIT...", true);
                InterfaceManager.instance.ToggleCurrentScreenInteractable();


            }, true));


        }

        else
        {
            ChangePhoto();
        }


    }




    /// <summary> Changes the camera photo previously captured by the user. </summary>

    public void ChangePhoto()
    {
        ResetOCRParameters();
        UIRepository.instance.ocrScreenElements.cameraFeed.texture = CameraServices.instance.GetARCameraBackgroundTexture();
        UIRepository.instance.ocrScreenElements.TouchToFocus.gameObject.SetActive(true);
    }





    /// <summary> Crops the user selection. </summary>

    public void CropSelection()
    {

        if (!photoCaptured) { return; }

        CameraServices cameraServices = CameraServices.instance;
        Texture2D capturedPhoto = UIRepository.instance.ocrScreenElements.capturedPhoto.mainTexture as Texture2D;
        RectTransform capturePhotoRectT = UIRepository.instance.ocrScreenElements.capturedPhoto.GetComponent<RectTransform>();


        Texture2D tex = cameraServices.CropPhoto(capturedPhoto, capturePhotoRectT, minDimentions, true, true);
        Sprite sprite = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 100.0f);
        UIRepository.instance.ocrScreenElements.croppedSelection.preserveAspect = true;
        UIRepository.instance.ocrScreenElements.croppedSelection.sprite = sprite;
        
        //Set to black if the texture is black and white otherwise set this to white.
        if(cameraServices.pixelFormat == Vuforia.Image.PIXEL_FORMAT.GRAYSCALE)
        {
            UIRepository.instance.ocrScreenElements.croppedSelection.color = Color.black;
        }
       
    
        textureToDecode = tex;

        regionCropped = true;
        sameCroppedRegion = false;

    }




    /// <summary> Tries to recognize the text from the user cropped selection.If "useOnlineServices" is true then the method will use online services for async OCR otherwise google tesseract OCR engine will be used in offline blocking mode</summary>

    public void DoOCR(bool useOnlineServices)
    {
           
        // if the user hasn't cropped a selection or is searching for the same cropped region then return
        if(!regionCropped ||  sameCroppedRegion) { return; }
        

        CognitiveServices cognitiveServices = CognitiveServices.instance;
        InterfaceManager interfaceManager   = InterfaceManager.instance;
        

        Animator loading = UIRepository.instance.onScreenIndicators.boxLoading;
        InterfaceManager.instance.ToggleCurrentScreenInteractable();
        InterfaceManager.instance.ShowOrHideLoadingIndicator(true , loading, false);
        textureToDecode = UIRepository.instance.ocrScreenElements.croppedSelection.sprite.texture;


        if(useOnlineServices)
        {

            StartCoroutine(cognitiveServices.CarryOnlineOCR(textureToDecode, (string parsedText) => 
            {

                if (parsedText == null)
                {
                    detectedWord = null;
                    //UIRepository.instance.ocrScreenElements.detectedText.text = "Failed to detect text!.";
                    Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                    interfaceManager.ShowModalWindow("Internet failure", icon, "Internet failure!, Please check your internet connection or contact admin for further support.", true);
                }


                else if (parsedText == "")
                {
                    sameCroppedRegion = true;
                    Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                    interfaceManager.ShowModalWindow("No text detected", icon, "Failed to detect text from the captured image.Do you want to search the dictionary instead?", true, "Yes", "No",
                      () => { interfaceManager.OnBackButtonPress(); /*Go to the previous screen which is the dictionary screen*/ }
                    , () => { }
                    );
                }
                
                else
                {
                    sameCroppedRegion = true;
                    detectedWord = parsedText;
                    UIRepository.instance.ocrScreenElements.detectedText.text = detectedWord;
                }


                InterfaceManager.instance.ToggleCurrentScreenInteractable();
                InterfaceManager.instance.ShowOrHideLoadingIndicator(false, loading, false);

            }));

        }


        else
        {
            detectedWord = cognitiveServices.CarryOfflineOCR(textureToDecode);
            UIRepository.instance.ocrScreenElements.detectedText.text = detectedWord;
            InterfaceManager.instance.ToggleCurrentScreenInteractable();
            InterfaceManager.instance.ShowOrHideLoadingIndicator(false, loading, false);
        }


    }




    /// <summary> Searches the parsed OCR text in dictionary. </summary>

    public void SearchDecodedWordInDictionary()
    {
        if(string.IsNullOrEmpty(detectedWord) || string.IsNullOrWhiteSpace(detectedWord)) { return; }
        InterfaceManager.instance.GoToScreen(4);
        float screenFadeDuration = InterfaceManager.instance.screenFadeDuration;

        ResetOCRParameters();

        SystemServices.instance.RunDelayedCommand(screenFadeDuration, () => 
        {
            SearchForMeaning(false);
        });

    }




    /// <summary> Resets the OCR parametes. </summary>

    public void ResetOCRParameters()
    {
        photoCaptured = false;
        regionCropped = false;
        UIRepository.instance.ocrScreenElements.capturedPhoto.gameObject.SetActive(false);
        UIRepository.instance.ocrScreenElements.croppedSelection.sprite = null;
        InterfaceManager.instance.ShowRoi(false);
        UIRepository.instance.ocrScreenElements.cameraFeed.gameObject.SetActive(true);
        UIRepository.instance.ocrScreenElements.TouchToFocus.gameObject.SetActive(true);
    }

}