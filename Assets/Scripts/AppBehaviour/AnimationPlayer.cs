﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



/// <summary>
/// An instance of this class can represent a single animation associated with an instruction.
/// </summary>

public class AnimationPlayer : AugmentedMediaBehaviour {





    private int targetAnimation;





    ///<summary> Forward animation playback. </summary>
    ///
    public override void Forward()
    {
        throw new NotImplementedException();
    }


    ///<summary> Rewind animation playback. </summary>
    ///
    public override void Rewind()
    {
        throw new NotImplementedException();
    }


    ///<summary> Start the animation playback.</summary> 

    public override void PlayMedia()
    {
        throw new NotImplementedException();
    }


    ///<summary> Show animation playback options.If the parameter passed is false then the playback options will be hidden. </summary>

    public override void ShowPlayBackOptions(bool show)
    {
        throw new NotImplementedException();
    }



    ///<summary> Returns true if the animation is currently playing. </summary>
    
    public override bool IsCurrentlyPlaying()
    {
        throw new NotImplementedException();
    }

    public override void PauseMedia()
    {
        throw new NotImplementedException();
    }
}
