﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Class holds form information for user registration and methods that ultimately register the user on the server. 
/// </summary>


public class Registration : Singleton<Registration> {




    private string userName;                                 // The username fetched from the input form.
    private string email;                                    // The username fetched from the input form.
    private string deviceId;                                 // The deviceId of this device.

    private const string emailPattern =
          @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    private const string namePattern = @"^[a-zA-Z\s]+$";







    ///<summary> Validates the information provided for this user registration. </summary>

    public void ValidateInfo()
    {

        InterfaceManager interfaceManager = InterfaceManager.instance;

        userName = UIRepository.instance.registrationForm.nameField.text;
        email    = UIRepository.instance.registrationForm.emailField.text;
        deviceId = SystemServices.instance.deviceUniqueIdentifier;



        if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(email))

        {

                if (Regex.IsMatch(email, emailPattern))
                {
                    // email valid
                }

                else
                {
                // email not valid
                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Invalid info", icon, "Invalid email address, Please recheck the information you've typed", true);
                return;
                }
               
                if (Regex.IsMatch(name, namePattern))
                {
                    // name valid
                }

                else
                {
                // name not valid
                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Invalid info", icon, "Invalid name, Please recheck the information you've typed", true);
                return;
                }


                // All is valid register the user.
           
                RegisterUser();

        }


        else
        {
            // Error - Empty input fields
            //Debug.LogWarning("ALERT!" +"You must complete all sections.");
            Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
            interfaceManager.ShowModalWindow("Incomplete sections", icon, "You must complete all sections to continue.", true);
        }


    }



    ///<summary> Register this user on the server. </summary>
   
    private void RegisterUser()
    {

        // Show user a waiting message

        InterfaceManager interfaceManager = InterfaceManager.instance;

        InterfaceManager.instance.ToggleCurrentScreenInteractable();

        Animator boxLoading = UIRepository.instance.onScreenIndicators.boxLoading;
        InterfaceManager.instance.ShowOrHideLoadingIndicator(true, boxLoading, false);


        SystemServices systemServices = SystemServices.instance;

        WWWForm form = new WWWForm();
        form.AddField("function", "CreateUser");
        form.AddField("name", userName);
        form.AddField("email", email);
        form.AddField("deviceId", deviceId);

        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) => 
        {


            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
                string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                //Debug.LogWarning(netError);
                //Debug.Log("Failed to register, Please check your internet connection or contact admin for further support.");
                InterfaceManager.instance.ToggleCurrentScreenInteractable();
                InterfaceManager.instance.ShowOrHideLoadingIndicator(false, boxLoading, false);

                Sprite icon = UIRepository.instance.modalWindowElements.warningIcon;
                interfaceManager.ShowModalWindow("Failed to register", icon, "Please check your internet connection or contact admin for further support.", true);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
            {
                Debug.LogWarning("Null or empty response from the server.");
                InterfaceManager.instance.ToggleCurrentScreenInteractable();
                InterfaceManager.instance.ShowOrHideLoadingIndicator(false, boxLoading, false);
            }

            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
            {
                string[] splitResponse = response.Split('+');
                Debug.LogWarning(splitResponse[1]);
                InterfaceManager.instance.ToggleCurrentScreenInteractable();
                InterfaceManager.instance.ShowOrHideLoadingIndicator(false, boxLoading, false);
            }


            else
            {

                // On Success update information in User object
                User.instance.InitializeUserInfo((bool? didSucceed) =>
                {

                    if (didSucceed == null) { } // The system failed to initialize user info

                    InterfaceManager.instance.ToggleCurrentScreenInteractable();
                    InterfaceManager.instance.ShowOrHideLoadingIndicator(false, boxLoading, false);
                    Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                    interfaceManager.ShowModalWindow("Success", icon, "You have been successfully registered on our server.", true , ()=> 
                    {
                        interfaceManager.CloseModalWindow();
                        interfaceManager.OnBackButtonPress();
                    });

                });
                
            }



        }, form.data));



    }



    protected override void OnAwake()
    {

    }
}
