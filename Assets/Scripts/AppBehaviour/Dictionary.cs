﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;


/// <summary>
/// This class constains methods to find the meaning of a word and check for if a word is an ingredient or not.
/// </summary>


public class Dictionary : MonoBehaviour {




                                                  ///<summary> The possible food categories/domains, this will be needed to check whether a word domain lies in food or not.</summary>
    private List<string> foodDomain;                       
                                                  ///<summary> The base URL to the dictionary Web API.</summary>
    private readonly string dictBaseUrl;                   
                                                  ///<summary> This is the application ID, you should send with each API request to Oxfords dictionary server.</summary>
    private readonly string appId;                     
                                                  ///<summary> The application key used to authenticate requests to the Oxform server.</summary>
    private readonly string appKey;                       
                                                  ///<summary> The word for which the meaning will be searched.</summary>
    [HideInInspector]
    public string inquiryWord;                                              
                                                  ///<summary> The image for if the inquiry word is an ingredient.</summary>
    private Texture2D ingredientImage;                    
                                                  ///<summary> Is the inquiry word an ingredient?.</summary>
    private bool isFoodItem;                           
                                                  ///<summary> The audio file link containing the pronunciation for this word.</summary>
    private string pronunciationFile;
                                                  /// <summary> This array contains the search results for a word in the dictionary.</summary>
    public WordEntry[] wordEntries { private set; get; }






    /// <summary> This structure represents the dictionary result for a word. </summary>

    public struct WordEntry
    {
        public static string etymologies;
        public string wordMeaning;
        public string wordDomain;
        public string exampleSentence;
    }





    public Dictionary(String inquiryWord) 
    {

        this.inquiryWord = inquiryWord;
        dictBaseUrl = "https://od-api.oxforddictionaries.com/api/v1";
        appId  = "b24a6584";
        appKey = "a34d92314daec0973b50201584fa7c0a";


        foodDomain = new List<string>();

        foodDomain.Add("food");      foodDomain.Add("savoury");
        foodDomain.Add("spice");     foodDomain.Add("fruit");
        foodDomain.Add("sweet");     foodDomain.Add("drink");
        foodDomain.Add("beverage");  foodDomain.Add("snack");
        foodDomain.Add("vegetable"); foodDomain.Add("oil");
        //CheckIfFood();

    }





    public Dictionary()
    {


        dictBaseUrl = "https://od-api.oxforddictionaries.com/api/v1";
        appId = "b24a6584";
        appKey = "a34d92314daec0973b50201584fa7c0a";


        foodDomain = new List<string>();

        foodDomain.Add("food"); foodDomain.Add("savoury");
        foodDomain.Add("spice"); foodDomain.Add("fruit");
        foodDomain.Add("sweet"); foodDomain.Add("drink");
        foodDomain.Add("beverage"); foodDomain.Add("snack");
        foodDomain.Add("vegetable"); foodDomain.Add("oil");

        //CheckIfFood();

    }





    /// <summary> Returns true if the inquiry word is a food item based on its domain. </summary>

    private bool CheckIfFood(string domain)
    {


        domain = domain.ToLower();
        bool isFoodItem = false;


        foreach (string category in foodDomain)
        {

            if(domain.Equals(category))
            {
                isFoodItem = true;
                break;
            }
        }


        return isFoodItem;
    }





    /// <summary> Searches for the meaning of the word set in this object.The callback is passed true if a meaning is found, false if no meaning is found and null if system failed to fetch the meaning. 
    /// <para>    Note in the case of failures the wordEntry object will be null, it will only be filled with data if the system successfully fetched the meaning.</para>
    /// </summary>

    internal void SearchMeaning(Action<bool?> callBack)
    {

        InterfaceManager interfaceManager = InterfaceManager.instance;
        SystemServices systemServices = SystemServices.instance;

        wordEntries = null;

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("Accept", "application/json");
        headers.Add("app_id", appId);
        headers.Add("app_key", appKey);
        string sourceLang = "en";
        string encodedUrl = dictBaseUrl + "/entries" + "/" + sourceLang + "/" + inquiryWord;


        StartCoroutine(systemServices.AsyncGETRequest(encodedUrl, (string response) =>

        {

            
        // Note that a 404 error in this case can also indicate that no meaning of the word is found
        if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
        {
            string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
            Debug.LogWarning(netError);

            if (systemServices.responseCode == 404)
            {
               //Debug.Log("No meaning found for the word: " + "'" + inquiryWord + "'");
               callBack(false);
            }

            else { callBack(null); } 
        }


        else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
        {
            string netError = Regex.Replace(response, systemServices.regexPatterns.nullOrEmpty, "").ToString();
            Debug.LogWarning(netError);
            callBack(null);
        }


        else
        {

            response = Regex.Replace(response, @"\t|\n|\r", "");
            response = Regex.Replace(response, @"\s+", " ");


            Match etymologies        = Regex.Match(response, "\"etymologies\": \\[(.+?)\\]", RegexOptions.Compiled);
            Match pronunciation      = Regex.Match(response, "\"audioFile\": \"(.+?)\"", RegexOptions.Compiled);
            MatchCollection meanings = Regex.Matches(response, "\"definitions\": \\[(.+?)\\]", RegexOptions.Compiled);



            string[] definitionBlocks = Regex.Split(response, "\"id\": \"m_en", RegexOptions.Singleline);

            wordEntries = new WordEntry[meanings.Count];
            if(etymologies.Success) { WordEntry.etymologies = etymologies.Result("$1").Split('"')[1].Trim(); } 
            //else { WordEntry.etymologies = ""; }
            if(pronunciation.Success) { pronunciationFile = pronunciation.Result("$1").Trim();}
            //else { pronunciationFile = ""; }



                for (int a = 0; a < meanings.Count; a++)
                {
                    
                    WordEntry wordEntry;
                    string definition = definitionBlocks[a];


                    Match wordDomain      = Regex.Match(definition, "\"domains\": \\[(.+?)\\]", RegexOptions.Compiled);
                    Match exampleSentence = Regex.Match(definition, "\"examples\": \\[ \\{ \"text\": (.+?)\\}", RegexOptions.Compiled);

                    wordEntry.wordMeaning = meanings[a].Result("$1").Split('"')[1].Trim();
                    wordEntry.wordDomain  = wordDomain.Success ? wordDomain.Result("$1").Replace("\"", "").Trim() : null;
                    wordEntry.exampleSentence = exampleSentence.Success ? exampleSentence.Result("$1").Trim() : null;

                    wordEntries[a] = wordEntry;

                }


                if (wordEntries[0].wordDomain != null) { isFoodItem = CheckIfFood(wordEntries[0].wordDomain); }
                else { isFoodItem = false; }

                callBack(true);

          }


      }, headers));

        
    }






}
