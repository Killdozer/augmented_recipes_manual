﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


/// <summary>
/// An instance of this class is used to represent a recipe.
/// </summary>

public class Recipe  {




   /*
                                                    ///<summary> Indicates whether the procedure for this recipe has been set from the database or not.The procedure should only be fetched when a user views a recipe. </summary>                                               
    public bool isProcedureInitialized; */
                                                    ///<summary> The procedure/steps for this recipe. </summary>
    public string[] instructions;
                                                    ///<summary> The ingredients for this recipe. </summary>
    public string[] ingredients;
                                                    ///<summary> The title for this recipe. </summary>
    public string title;                        
                                                    ///<summary> The unique ID for this recipe used to identify it in the database. For a preloaded recipe this defaults to "-1". </summary>
    public int id;
                                                    ///<summary> The name of the author for this recipe. For a preloaded recipe this defaults to "devprl". </summary>
    public string authorName;
                                                    ///<summary> The date at which this recipe was uploaded.For a preloaded recipe this is null </summary>
    public DateTime? date;
                                                    ///<summary> The time in minutes it will take to prepare this recipe. </summary>
    public int preparationTime;
                                                    ///<summary> Is this recipe amongst the preloaded recipes?. </summary>                  
    public bool isPreloaded;




    public Recipe(string[] instructions, string[] ingredients, string title,  bool isPreloaded, int preparationTime ,  int id = -1  , string authorName = "devprl" , DateTime? date = null)
    {


        this.instructions = instructions;
        this.ingredients = ingredients;
        this.title = title;
        this.id = isPreloaded? -1 :id;
        this.authorName = isPreloaded ? "devprl" :authorName;
        this.date = isPreloaded ? null : date;
        this.preparationTime = preparationTime ;
        this.isPreloaded = isPreloaded;

    }



}
