﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UI.Extensions;

/// <summary>
/// This class is used in speech synthesis operations for single or multiple instructions.
/// </summary>


public class AudioPlayer : AugmentedMediaBehaviour {


    [Range(0,0.755f)]
                                                          ///<summary> The default volume on the knob when the application starts.</summary>
    public float defaultKnobVolume;     
                                                          ///<summary> Time to wait before speaking the next instruction.</summary>
    public float delayBetweenInstructions;              
                                                          ///<summary> The current volume of the audio player.</summary>
    public float volume { get; private set; }
                                                          ///<summary> The string array containing the sentences to speak.</summary>
    private string[] sentencesToSpeak { set; get; }
                                                          ///<summary> The index in the "textToSpeak" array pointing to the current sentence being spoken.</summary>
    private int sentencePointer;
                                                          ///<summary> Indicates whether user has turned on repeat mode or not.</summary>
    private bool isRepeatOn;
                                                          ///<summary> Indicates whether user has pressed the stop button on the audio player.</summary>
    private bool isPlayerStopped;
                                                          ///<summary> Indicates if the TextToSpeech engine is busy speaking.</summary>
    private bool isSpeaking;
                                                          ///<summary> Indicates if the StartPlayList routine is currently running or not.</summary>
    private bool isPlayRoutineRunning;





    public AudioPlayer(string[] sentencesToSpeak)
    {
        this.sentencesToSpeak = sentencesToSpeak;
    }





    private void Start()
    {
        StartCoroutine(RegisterUtteranceListeners());
        this.mediaType = SystemTypes.MediaType.Audio;
        UIRepository.instance.audioControls.volumeKnob.UpdateKnobValue(defaultKnobVolume);
        ChangeVolume(defaultKnobVolume);
    }



    ///<summary> prepares the audio player for the given recipe. </summary>

    private void PrepareForRecipe(Recipe recipe)
    {
        sentencesToSpeak = recipe.instructions;
        UIRepository.instance.audioControls.recipeTitle.text      = recipe.title.ToUpper();
        UIRepository.instance.audioControls.instructionLabel.text = sentencesToSpeak[0].ToUpper();
    }



    /// <summary> Starts and loads the audio player for the given recipe. </summary>

    public void LoadForRecipe(Recipe recipe)
    {
        Debug.Log("Loading audio player for  "+recipe.title);
        GetComponent<AudioPlayer>().PrepareForRecipe(recipe);
        InterfaceManager.instance.GoToScreen(2);
    }




    /// <summary> Starts and loads the audio player for the currently ative preloaded recipe. </summary>

    public void LoadForActiveRecipe()
    {
        Debug.Log("Loading audio player for  " + RecipeManager.instance.activeRecipe.title);
        GetComponent<AudioPlayer>().PrepareForRecipe(RecipeManager.instance.activeRecipe);
        InterfaceManager.instance.GoToScreen(2);
    }



    ///<summary> Forward audio playback. </summary>

    public override void Forward()
    {
   

        if (sentencePointer == sentencesToSpeak.Length - 1) { sentencePointer = 0; }

        else
        {
            sentencePointer++;
            string sentence = sentencesToSpeak[sentencePointer];
            UIRepository.instance.audioControls.instructionLabel.text = sentence.ToUpper();
        }


        if (!isPlayerStopped)
        {
            isSpeaking = false;
            isPlayerStopped = true;
            StartCoroutine(StartPlayList());
        }

    }





    ///<summary> Rewind audio playback. </summary>

    public override void Rewind()
    {

        if (isPlayerStopped) { return; }

        if (sentencePointer == 0) { sentencePointer = sentencesToSpeak.Length - 1; }

        else
        {
            sentencePointer--;
            string sentence = sentencesToSpeak[sentencePointer];
            UIRepository.instance.audioControls.instructionLabel.text = sentence.ToUpper();
        }


        if (!isPlayerStopped)
        {
            isSpeaking = false;
            isPlayerStopped = true;
            StartCoroutine(StartPlayList());
        }

    }





    ///<summary> Start the audio playback.</summary>

    public override void PlayMedia()
    {


       SystemServices systemServices = SystemServices.instance;
       bool failed = false;



       if(sentencesToSpeak == null || sentencesToSpeak.Length == 0)
        {
            Debug.LogError("Null, empty or no sentences provided to the Audio Player");
            isPlayerStopped = true;
            isRepeatOn = false;
            isSpeaking = false;
            return;
        }


       if(systemServices.textToSpeechFailed)
        {
            systemServices.ShowToast(" Please enable Text-to-speech output in Settings>Accessibility and restart the application.", TextToSpeech.ToastLength.LENGTH_LONG);
            isPlayerStopped  = true;
            isRepeatOn = false;
            isSpeaking = false;
            return;
        }


        
       systemServices.textToSpeech.Speak(sentencesToSpeak[sentencePointer], (string msg) =>
       {
               TTSFailure(msg);
               failed = true;
       });



       if(!failed)
       {
            string sentence = sentencesToSpeak[sentencePointer];
            UIRepository.instance.audioControls.instructionLabel.text = sentence.ToUpper();
            //SystemServices.instance.ShowToast(sentencesToSpeak[sentencePointer], TextToSpeech.ToastLength.LENGTH_SHORT);               
        }

       else
       {
            PauseMedia();
            isRepeatOn = false;
       }

       

    }






    ///<summary>Repeats playing the same instruction if "isRepeatOn" is true, otherwise it sequentially plays all the instruction of the recipe until the end.</summary>

    private IEnumerator StartPlayList()
    {


        while (isPlayRoutineRunning) {  yield return new WaitForEndOfFrame(); }


        isPlayerStopped = false;
        isPlayRoutineRunning = true;



        while (!isPlayerStopped)
        {


            if(!isPlayerStopped)
            {

                SystemServices.instance.textToSpeech.StopSpeech();

                Debug.Log("ISSpeaking Before PlayMedia()  "+isSpeaking);

                isSpeaking = true;                                             // Don't change this line(Because the UtteranceStartedCallback sometimes get's called after the call to PlayMedia() has been executed hence even if the playback has started isSpeaking is set to false)

                PlayMedia();

                Debug.Log("ISSpeaking AFTER PlayMedia()  " + isSpeaking);


                while (isSpeaking) { /*Debug.Log("Waiting");*/ yield return new WaitForSeconds(0); }


                if (isPlayerStopped) { isPlayRoutineRunning = false; yield break; }


                yield return new WaitForSeconds(delayBetweenInstructions);


                if (!isRepeatOn && !isPlayerStopped)
                {


                    if (sentencePointer == sentencesToSpeak.Length - 1)
                    {
                        //isPlayerStopped = true;
                        PauseMedia();
                        sentencePointer = 0;
                    }

                    else
                    {
                        sentencePointer++;
                    }

                }

                if (isPlayerStopped) { isPlayRoutineRunning = false; yield break; }

            }


            if (isPlayerStopped) { isPlayRoutineRunning = false; yield break; }

        }


        isPlayRoutineRunning = false;

    }





    ///<summary> Listens for event when the user presses the play button. </summary>

    public void PlayButtonHandler()
    {
        UIRepository.instance.audioControls.playButton.gameObject.SetActive(false);
        UIRepository.instance.audioControls.stopButton.gameObject.SetActive(true);
        Thread.Sleep(25);
        StartCoroutine(StartPlayList());
    }





    ///<summary> Show audio playback options.If the parameter passed is false then the playback options will be hidden. </summary>

    public override void ShowPlayBackOptions(bool show)
    {
        throw new NotImplementedException(); 
    }



    ///<summary> Stop audio playback. </summary>

    public override void PauseMedia()
    {
        isSpeaking = false;
        isPlayerStopped = true;
        SystemServices.instance.textToSpeech.StopSpeech();
        UIRepository.instance.audioControls.playButton.gameObject.SetActive(true);
        UIRepository.instance.audioControls.stopButton.gameObject.SetActive(false);
    }




    ///<summary> Listens for the event when the volume control knob is interacted with.</summary>

    public void ChangeVolume(float volume)
    {

        // Mapping the volume values from (0 - 0.755) to (0 - 1)
        this.volume = (1 / 0.755f) * volume;

        #if !UNITY_EDITOR

        SystemServices.instance.textToSpeech.SetVolume(this.volume);

#endif

    }




    public void ChangeSpeed(float value)
    {
        SystemServices.instance.textToSpeech.SetSpeed(value);
    }



    public void ChangeDelay(float value)
    {
        delayBetweenInstructions = value;
    }



    public void ChangePitch(float value)
    {
        SystemServices.instance.textToSpeech.SetPitch(value);
    }



    ///<summary> Returns true if the speech synthesizer is currently speaking. </summary>

    public override bool IsCurrentlyPlaying()
    {
        return SystemServices.instance.textToSpeech.IsSpeaking();
    }




    ///<summary> Turns on repeat mode. If repeat mode is turned on the same audio repeats playing otherwise it stops and the subsequent instruction audio starts playing. </summary>

    public void Repeat()
    {

        Button repeatButton = UIRepository.instance.audioControls.repeatButton;

        isRepeatOn = !isRepeatOn;

        if(isRepeatOn)
        {          
            repeatButton.GetComponent<Image>().sprite = UIRepository.instance.audioControls.repeatCheckedSprite;
        }
        else
        {
            repeatButton.GetComponent<Image>().sprite = UIRepository.instance.audioControls.repeatUncheckedSprite;
        }

        
    }






    /// <summary> Delegate that receives the response back from the call to TextToSpeech engine . </summary> 

    private void TTSFailure(string msg)
    {

            Debug.LogError(msg);
            SystemServices.instance.ShowToast(msg + " Failed to speak.Please enable Text-to-speech output in Settings>Accessibility.", TextToSpeech.ToastLength.LENGTH_LONG);
    }






    /// <summary> Listens for the event when TextToSpeech engine starts to speak.PLEASE NOTE these events might not return real time results. </summary> 

    public void OnUtteranceStart(string msg)
    {
        Debug.Log(msg);
    }




    /// <summary> Listens for the event when TextToSpeech engine fails to speak.PLEASE NOTE these events might not return real time results. </summary> 

    public void OnUtteranceError(string msg)
    {
        Debug.Log(msg);
    }




    /// <summary> Listens for the event when TextToSpeech engine is done speaking the given text.PLEASE NOTE these events might not return real time results. </summary> 

    public void OnUtteranceDone(string msg)
    {
        Debug.Log(msg);
        isSpeaking = false;
    }




    /// <summary> Used to register TextToSpeech listeners on startup. </summary> 

    private IEnumerator RegisterUtteranceListeners()
    {

        while (SystemServices.instance == null) { yield return new WaitForEndOfFrame(); }

        while (SystemServices.instance.textToSpeech == null) { yield return new WaitForEndOfFrame(); }

        SystemServices.instance.textToSpeech.RegisterUtteranceListeners("OnUtteranceStart", "OnUtteranceError", "OnUtteranceDone");

        
    }



}
