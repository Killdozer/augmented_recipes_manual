﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using Vuforia;

/// <summary>
/// This class listens for trackable found and lost events and performs an appropriate action on target marker detection.
/// <para> Based on the marker detected this class can instantiate the appropriate AugmentedMediaBehaviour type object. </para>
/// </summary>


public class TargetHandler : MonoBehaviour
{

    [Tooltip("Should the video be paused when the target marker is obsecured and resumed when the marker becomes visible again.")]
                                                                ///<summary> Should the video be paused when the target marker is obsecured and resumed when the marker becomes visible again.</summary>
    public bool pauseVideoOnObescurity; 
       
    private int targetID;
    private SystemTypes.MediaType mediaType;
    private VideoPlayer videoPlayer;                            // The videoPlayer child of the instruction marker that was detected.  




    /// <summary> The action that will be taken when a trackable object is found. This method acts as listener for on trackable found type system event. </summary>

    public void ActionOnDetection(TrackableBehaviour target)
    {

        // The action against a trackable's detection should only be taken if the current screen is ARScreen.
        if (InterfaceManager.instance.currentScreenSpace != UIStates.ScreenSpace.ARScreen) { return; }
       
        if (target.Trackable == null) { return; }
        
        this.targetID = target.Trackable.ID;
        string tag    = target.gameObject.tag;


        switch(tag)
        {

                           
            case "VideoMarker":
                //Debug.Log("Play Video of  "+target.transform.parent.name);
                //RenderChildrenExcept(target.gameObject , target.transform.GetChild(0).gameObject , true);
                AugVideoPlayer augVideoPlayer = GetComponent<AugVideoPlayer>();
                augVideoPlayer.ShowPlayBackOptions(true);
                augVideoPlayer.DisableOrEnableVideoControls(true);

                augVideoPlayer.videoPlayer.GetComponent<AudioSource>().volume = augVideoPlayer.volume;

                Renderer renderer = augVideoPlayer.videoPlayer.GetComponent<MeshRenderer>();
                if(renderer != null) { renderer.enabled = true; }
                augVideoPlayer.PlayMedia();

                break;


            case "RecipeLoaderMarker":
                //RenderChildrenExcept(target.transform.parent.gameObject, target.gameObject, true);
                //RecipeManager.instance.LoadPreloadedRecipe(target.transform.parent.name); baw did
                RecipeManager.instance.LoadPreloadedRecipe(target.transform.name.Split('(')[0]);
                break;


            case "Ingredients":
                //Debug.Log("Show ingredients.");
                //RenderChildrenExcept(target.transform.parent.gameObject, target.gameObject, true);

                var rendererComponents = target.GetComponentsInChildren<Renderer>(true);
                foreach (var component in rendererComponents) { component.enabled = true; }

                GetComponent<IngredientsAnimator>().StartAnimating();
                break;


            case "InstructionMarker":
                //Debug.Log("Play instruction animation. of  "+target.transform.parent.parent.name);
                //RenderChildrenExcept(target.transform.parent.gameObject, target.gameObject, true);
                videoPlayer = target.transform.GetChild(0).GetComponent<VideoPlayer>();
                renderer = videoPlayer.GetComponent<Renderer>();
                if (renderer != null) { renderer.enabled = true; }
                videoPlayer.Play();

                break;


            
        }

   
    }




    /// <summary> The action that will be taken when a trackable object is lost. This method acts as listener for on trackable found type system event. </summary>

    public void ActionOnObsecurity(TrackableBehaviour target)
    {

        
        /*
        // The action against a trackable's obsecurity should only be taken if the current screen is ARScreen.
        if (InterfaceManager.instance != null)
        {
            if (InterfaceManager.instance.currentScreenSpace != UIStates.ScreenSpace.ARScreen) { return; }
        }
        */

        if (target.Trackable == null) { return; }



        this.targetID = target.Trackable.ID;
        string tag = target.gameObject.tag;


        var rendererComponents = target.GetComponentsInChildren<Renderer>(true);
        var colliderComponents = target.GetComponentsInChildren<Collider>(true);
        var canvasComponents   = target.GetComponentsInChildren<Canvas>(true);


        foreach (var component in rendererComponents) { component.enabled = false; }

        foreach (var component in colliderComponents) { component.enabled = false; }

        foreach (var component in canvasComponents)   { component.enabled = false; }



        switch (tag)
        {

            case "VideoMarker":
                Debug.Log("Pause Video.");
                AugVideoPlayer augVideoPlayer = GetComponent<AugVideoPlayer>();
                augVideoPlayer.ShowPlayBackOptions(false);
                augVideoPlayer.DisableOrEnableVideoControls(false);

                augVideoPlayer.videoPlayer.GetComponent<AudioSource>().volume = 0;

                if (pauseVideoOnObescurity) { augVideoPlayer.videoPlayer.Pause(); }
                else { augVideoPlayer.videoPlayer.Stop(); }

                break;


            case "RecipeLoaderMarker":

                break;


            case "Ingredients":
                GetComponent<IngredientsAnimator>().StopAnimating();
                break;


            case "InstructionMarker":
                //Debug.Log("Pause instruction animation.");
                if (videoPlayer != null)
                {
                    if (pauseVideoOnObescurity) { videoPlayer.Pause(); }
                    else {videoPlayer.Stop(); }                   
                }

                break;


        }


    }




    /// <summary>
    /// This will enable or disable all renderers on the children on the given gameObject except the one passed as the exclusion parameter. 
    /// </summary>
    /// <param name="parent"> The parent game objects whose children to render or unrender.</param>
    /// <param name="excludedChild"> The child to exclude disabling or eanbling the renderer of.</param>
    /// <param name="unRender">If this parameter is true then the Renders will be disabled otherwise they will be enabled.</param>

    public void RenderChildrenExcept(GameObject parent, GameObject excludedChild, bool unRender)
    {


        foreach(Transform child in parent.transform)
        {

            if(child.name.Equals(excludedChild.name)) { continue; }

            if (unRender) { child.gameObject.SetActive(false); Debug.Log("Deactiviationig  "+child.name + "  of  "+parent.name); }
            else          { child.gameObject.SetActive(true); }


/*
            Renderer[] rendererComponents = child.GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = child.GetComponentsInChildren<Collider>(true);
            Canvas[] canvasComponents     = child.GetComponentsInChildren<Canvas>(true);

            foreach (var component in rendererComponents) { component.enabled = unRender ? false : true; }

            foreach (var component in colliderComponents) { component.enabled = unRender ? false : true; }

            foreach (var component in canvasComponents)   { component.enabled = unRender ? false : true; }
*/
        }


    }


}