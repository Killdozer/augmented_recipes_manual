﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class IngredientsAnimator : MonoBehaviour {


                                                         ///<summary> Flag indicates whether to fade alpha during animation or just rotate.</summary>
    [Tooltip("Flag indicates whether to fade alpha during animation or just rotate.")]
    public bool fadeAlpha;
                                                         ///<summary> The value upto which the model will be faded. </summary>
    [Tooltip("The value upto which the model will be faded.")]
    public float fadeToAmount;
                                                         ///<summary> The time duration for which the fade effect will last. </summary>
    [Tooltip("The time duration for which the fade effect will last.")]
    public int fadeAwayDuration;
                                                         ///<summary> The time duration for which the overall animation effect(rotation + fading) will last. </summary>
    [Tooltip("The time duration for which the overall animation effect(rotation + fading) will last.")]
    public int timeToLive;
                                                         ///<summary> The speed with which the model will be rotated. </summary>    
    [Tooltip("The speed with which the model will be rotated.")]
    public float rotationSpeed;
                                                         ///<summary> The array which holds the ingredient models for the current loaded recipe. </summary>    
    [HideInInspector]
    public Transform[] ingredientsModels;



    private bool isPaused = true;
    private float timeElapsed;
    private int ingredientPointer;
    private bool routineRunning;
    private Material modelMaterial = null;






    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		

        if(!isPaused)
        {
            //var transform = ingredientsModels[ingredientPointer];
            //transform.Rotate(transform.up, rotationSpeed * Time.deltaTime , Space.Self); 
        }


	}



    private IEnumerator ElapseTime()
    {


        routineRunning = true;
        Color original = new Color();
        GameObject previous = null;

        while (!isPaused)
        {

            yield return new WaitForSeconds(1);
            timeElapsed++;

            if(timeElapsed == timeToLive - fadeAwayDuration)
            {
               
                if(fadeAlpha)
                {
                    modelMaterial = ingredientsModels[ingredientPointer].gameObject.GetComponent<MeshRenderer>().material;
                    original = modelMaterial.color;

                    if (modelMaterial != null)
                    {
                        previous = ingredientsModels[ingredientPointer].gameObject;
                        iTween.FadeTo(previous, fadeToAmount, fadeAwayDuration);
                    }
                }


            }


            if(timeElapsed == timeToLive)
            {

                timeElapsed = 0;

                if (modelMaterial != null && fadeAlpha)
                {
                    modelMaterial.color = original;
                    if (previous) iTween.Stop(previous);
                }

                ingredientsModels[ingredientPointer].gameObject.SetActive(false);
                //ingredientsModels[ingredientPointer].gameObject.GetComponent<MeshRenderer>().enabled = false;
                if (ingredientPointer != ingredientsModels.Length - 1) { ingredientPointer++; }
                else { ingredientPointer = 0; }
                ingredientsModels[ingredientPointer].gameObject.SetActive(true);
                //ingredientsModels[ingredientPointer].gameObject.GetComponent<MeshRenderer>().enabled = true;

            }

        }


        timeElapsed = 0;
        routineRunning = false;

        if (modelMaterial != null && fadeAlpha)
        {
            modelMaterial.color = original;
            if(previous) iTween.Stop(previous);
        }

        ingredientsModels[ingredientPointer].gameObject.SetActive(false);
        //ingredientsModels[ingredientPointer].gameObject.GetComponent<MeshRenderer>().enabled = false;

    }





    public void StartAnimating()
    {
        if (!routineRunning)
        {
            //Debug.Log("Should start now!  "+ ingredientsModels[ingredientPointer].gameObject.name);
            ingredientsModels[ingredientPointer].gameObject.SetActive(true);

            //ingredientsModels[ingredientPointer].gameObject.GetComponent<MeshRenderer>().enabled = true;

            isPaused = false;
            timeElapsed = 0;
            StartCoroutine(ElapseTime());
        }
    }




    public void StopAnimating()
    {
        isPaused = true;
        routineRunning = false;
    }
}
