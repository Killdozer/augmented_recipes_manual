﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;




/// <summary>
/// This is the base class for all media behaviour types associated with image targets. 
/// </summary>


public abstract class AugmentedMediaBehaviour : MonoBehaviour {



                                                         ///<summary> Unique identifier for an image target used to identify the target prefab in the targets repository. </summary>
    internal string           targetID;
                                                         ///<summary> The playback duration/length of this media . </summary>
    internal float            mediaLength;
                                                         ///<summary> The type of this media . </summary>
    internal SystemTypes.MediaType  mediaType;




                                                        

    public abstract void PlayMedia();

    public abstract void PauseMedia();

    public abstract bool IsCurrentlyPlaying();

    public abstract void Forward();

    public abstract void Rewind();

    public abstract void ShowPlayBackOptions(bool show);

  
}
