﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;





/// <summary>
/// This class represents a user by holding the user information if registered and can be used to get user status on server. 
/// </summary>

public class User : Singleton<User>
{



                                            
    private string deviceId;                     // The deviceId for this device.
    private bool? isBlocked = null;              // Is this user blocked.The value null represents that either the user isn't registered or the system failed to get blocked status.
    private bool? isRegistered = null;           // Is this user registered.The value null represents that the system failed to get registration status.


    ///<summary> Wraps information for this user obtained from the json object from the system web apis.</summary>
    [HideInInspector]
    public UserInfo userInfo;


    public class UserInfo
    {
        public string userId;
        public string name;
        public string email;
        public string deviceId;
        public bool blocked;
        public int spamCount;
    }






    protected override void OnAwake()
    {

        deviceId = SystemServices.instance.deviceUniqueIdentifier;

        //InterfaceManager.instance.ToggleCurrentScreenInteractable();
        InitializeUserInfo((bool? didSucceed) =>
        {   
            //InterfaceManager.instance.ToggleCurrentScreenInteractable();
            if (didSucceed == null) { Debug.Log("System failed to initialize user info."); }
        });
    }




    void Start()
    {

    }



    /// <summary> Returns true if this user is blocked on the server.False if the user isn't blocked and null in case if the user isn't registered or if failed.Note that this is a blocking call. 
    /// <para> If the argument passed in the reCheck parameter is true the method polls the server to get user's fresh blocked status, otherwise it just returns the old status. </para>
    /// </summary> 


    public bool? CheckBlockedStatusBlockingMode(bool reCheck)
    {

        bool? isBlocked = false;

        if(reCheck)
        {
            
            SystemServices systemServices = SystemServices.instance;
            WWWForm form = new WWWForm();
            form.AddField("function", "IsBlocked");
            form.AddField("deviceId", deviceId);

            systemServices.BlockingPOSTRequest(systemServices.systemApiEndpoint, (string response) => 
            {

                if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                {
                    isBlocked = null;
                    Debug.LogWarning("Can't check blocked status internet failure!");
                }

                bool flag;
                bool result;

                flag = Boolean.TryParse(response, out result);
                isBlocked = flag ? (bool?)result : null;

            }
            , form.data);

            return isBlocked;
        }

        else { return this.isBlocked; }

    }






    /// <summary> Asynchronously checks if the user is blocked on the server or not and calls the provided callback.
    /// <para> Note that the callback is passed true argument if this user is blocked on the server.False if the user isn't blocked and null in case if the user isn't registered or if failed. </para>
    /// <para> If the argument passed in the reCheck parameter is true the method polls the server to get user's fresh blocked status, otherwise it just returns the old status. </para>
    /// </summary> 


    public void CheckBlockedStatusAsyncMode(bool reCheck, Action<bool?> asyncCallback)
    {
        

        bool? isBlocked = false;

        if (reCheck)
        {

            SystemServices systemServices = SystemServices.instance;
            WWWForm form = new WWWForm();
            form.AddField("function", "IsBlocked");
            form.AddField("deviceId", deviceId);

            StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>
            {

                if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                {
                    isBlocked = null;
                    Debug.LogWarning("Can't check blocked status internet failure!");
                }

                bool flag;
                bool result;

                flag = Boolean.TryParse(response, out result);
                isBlocked = flag ? (bool?)result : null;

                asyncCallback(isBlocked);
            }
            , form.data)

            );

            
        }

        else { asyncCallback(this.isBlocked); }

    }





    /// <summary>
    /// Returns true if this user is registered on the server.False if the user isn't registered.Null if failed to check registration status. Note that this is a blocking call.
    /// <para> If the argument passed in the reCheck parameter is true the method polls the server to get user's fresh registration status, otherwise it just returns the old status. </para>
    /// </summary>


    public bool? CheckRegistrationStatusBlockingMode(bool reCheck)
    {

        bool? isRegistered = false;

        if (reCheck)
        {

            SystemServices systemServices = SystemServices.instance;
            WWWForm form = new WWWForm();
            form.AddField("function", "IsRegistered");
            form.AddField("deviceId", deviceId);

            systemServices.BlockingPOSTRequest(systemServices.systemApiEndpoint, (string response) => 
            {

                if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                {
                    isRegistered = null;
                    Debug.LogWarning("Can't check registration status internet failure!");
                }


                bool flag;
                bool result;

                flag = Boolean.TryParse(response, out result);
                isRegistered = flag ? (bool?)result : null;

            }
            , form.data);

            return isRegistered;
        }

        else { return this.isRegistered; }
    }





    /// <summary> Asynchronously checks if the user is registered on the server or not and calls the provided callback.
    /// <para> Note that the callback is passed true if this user is registered on the server.False if the user isn't registered.Null if failed to check registration status. </para>
    /// <para> If the argument passed in the reCheck parameter is true the method polls the server to get user's fresh blocked status, otherwise it just returns the old status. </para>
    /// </summary> 


    public void CheckRegistrationStatusAsyncMode(bool reCheck, Action<bool?> asyncCallback)
    {
        

        bool? isRegistered = false;

        if (reCheck)
        {

            SystemServices systemServices = SystemServices.instance;
            WWWForm form = new WWWForm();
            form.AddField("function", "IsRegistered");
            form.AddField("deviceId", deviceId);

            StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) =>
            {

                if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
                {
                    isRegistered = null;
                }

                bool flag;
                bool result;

                flag = Boolean.TryParse(response, out result);
                isRegistered = flag ? (bool?)result : null;

                asyncCallback(isRegistered);
            }
            , form.data)
            
            );

            
        }

        else { asyncCallback(this.isRegistered); }

    }






    /// <summary> Fetches user information for the given deviceId and then initializes the userinfo object.If the user isn't registered then this method prompts for user registration.Note that this is an asynchronous call.
    /// <para>    Calls the method provided to the callback parameter with true if user Info has been successfully initialized, false if the user isn't registered and null if the system failed to get user info.</para>
    /// </summary>

    public void InitializeUserInfo(Action<bool?> callback)
    {

        if (userInfo != null) { callback(true); return; }             // If userInfo has already been initialized then don't re-initialize it

        string deviceId = this.deviceId;
        SystemServices systemServices = SystemServices.instance;
        WWWForm form = new WWWForm();
        form.AddField("function", "GetUserInfo");
        form.AddField("deviceId", deviceId);

        StartCoroutine(systemServices.AsyncPOSTRequest(systemServices.systemApiEndpoint, (string response) => 
        
        {

            
            if (Regex.IsMatch(response, systemServices.regexPatterns.netError))
            {
               
                string netError = Regex.Replace(response, systemServices.regexPatterns.netError, "").ToString();
                Debug.LogWarning(netError);
                callback(null);
            }


            else if (Regex.IsMatch(response, systemServices.regexPatterns.nullOrEmpty))
            {
                Debug.LogWarning("Null or empty response from the server.");
                callback(null);
            }


            else if (Regex.IsMatch(response, systemServices.regexPatterns.generalError))
            {
                string[] splitResponse = response.Split('+');
                //Debug.LogWarning(splitResponse[1]);
                InterfaceManager interfaceManager = InterfaceManager.instance;

                Sprite icon = UIRepository.instance.modalWindowElements.infoIcon;
                interfaceManager.ShowModalWindow("Unregistered User", icon, "You need to register yourself to use all of our services, do you want to register now?", true, "Yes", "No",
                  () => { interfaceManager.GoToScreen(8); /*Go to the registrations screen*/ }
                , () => {  }
                );


                isRegistered = null;
                isBlocked = null;

                callback(false);
            }


            else if (!response.Equals("\"[]\""))
            {
                response = response.Remove(0, 2);
                response = response.Remove(response.Length - 6);
                response = response.Replace("\\\"", "\"");
                userInfo = JsonUtility.FromJson<UserInfo>(response);    // json parse error sometimes
                isRegistered = true;
                isBlocked = userInfo.blocked;
                //Debug.Log("Already registered");
                callback(true);
            }


            else
            {
                //isRegistered = null;
                //isBlocked = null;
                //Debug.Log("Prompt for registration");
            }
            

        } 
        
        , form.data) );


    }


}
