﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class TextToSpeech :MonoBehaviour
{




    public enum Locale
    {
        UK = 0,
        US = 1

    }


    public enum ToastLength
    {
        LENGTH_LONG  = 0,
        LENGTH_SHORT = 1
    }



    private string toastString;
    private AndroidJavaObject currentActivity;
    private AndroidJavaObject TTSExample = null;
    private AndroidJavaObject activityContext = null;
    private Locale _lang;
    public Locale Language { get { return _lang; } set { SetLanguage(value); } }
    private float _pitch, _speed, _volume;
    public float pitch { get { return _pitch; } private set { } }
    public float speed { get { return _speed; } private set { } }
    public float volume { get { return _volume; } private set { } }


    public delegate void OnErrorCallbackHandler(string error);
    private OnErrorCallbackHandler _callback;
    private string toastLength;

    public TextToSpeech()
    {
        //Initialize();

    }



    public TextToSpeech(Locale language)
    {
        Initialize();
    }



    public TextToSpeech(Locale language,float speed,float pitch)
    {
        Initialize();
        this.Language = language;
        this.pitch = pitch;
        this.speed = speed;
        SetLanguage(this.Language);
        SetSpeed(this.speed);
        SetPitch(this.pitch);
    }



    public void Speak(string toSay,OnErrorCallbackHandler callback)
    {
        if (TTSExample == null)
        {
            Initialize();
        }
        this._callback = callback;


        TTSExample.Call("TTSMEWithCallBack", toSay, gameObject.name, "OnError");
       
    }
    public void OnError(string error)
    {
        if (_callback != null)
        {
            if (error.Length > 0)
            {
                _callback.Invoke(error);
            }
        }
       
    }
    public void Speak(string toSay)
    {
        if (TTSExample == null)
        {
            Initialize();
        }

        TTSExample.Call("TTSME", toSay);

    }
    public void SetLanguage(Locale lan)
    {
        this._lang = lan;
        string[] Language = new string[] {"UK","US" };
        if (TTSExample == null)
        {
            Initialize();
        }
        TTSExample.Call("SetLang", Language[(int)lan]);
    }
    public void SetSpeed(float speed)
    {
        this._speed = speed;
        if (TTSExample == null)
        {
            Initialize();
        }
        TTSExample.Set<float>("Speed", speed);
    }
    public void SetPitch(float pitch)
    {
        this._pitch = pitch;
        if (TTSExample == null)
        {
            Initialize();
        }
        TTSExample.Set<float>("Pitch", pitch);
    }

    public void SetVolume(float volume)
    {
        this._volume = volume;
        if (TTSExample == null)
        {
            Initialize();
        }
        TTSExample.Set<string>("volume", "" + volume);
    }
    private void Initialize()
    {
        if (TTSExample == null)
        {
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            using (AndroidJavaClass pluginClass = new AndroidJavaClass("ir.hoseinporazar.androidtts.TTS"))
            {
                if (pluginClass != null)
                {
                    TTSExample = pluginClass.CallStatic<AndroidJavaObject>("instance");
                    TTSExample.Call("setContext", activityContext);

                  

                }
            }
        }


    }





    /*
    public void ShowToast(string msg)
    {

        if (TTSExample == null)
        {
            using (AndroidJavaClass activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");
            }

            using (AndroidJavaClass pluginClass = new AndroidJavaClass("ir.hoseinporazar.androidtts.TTS"))
            {
                if (pluginClass != null)
                {
                    TTSExample = pluginClass.CallStatic<AndroidJavaObject>("instance");
                    TTSExample.Call("setContext", activityContext);
                    activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() =>
                    {
                        TTSExample.Call("showMessage", msg);
                    }));
                }
            }
        }
        else
        {
            activityContext.Call("runOnUiThread", new AndroidJavaRunnable(() =>
            {
                TTSExample.Call("showMessage", msg);
            }));
        }
    }

    */



    public void Toast(string message , ToastLength length)
    {
        AndroidJavaClass UnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");

        currentActivity = UnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        this.toastString = message;
        this.toastLength = Enum.GetName(typeof(ToastLength) , (int)length);
        currentActivity.Call("runOnUiThread", new AndroidJavaRunnable(showToast));
    }




    private void showToast()
    {
        AndroidJavaObject context = currentActivity.Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaClass Toast = new AndroidJavaClass("android.widget.Toast");
        AndroidJavaObject javaString = new AndroidJavaObject("java.lang.String", toastString);
        AndroidJavaObject toast = Toast.CallStatic<AndroidJavaObject>("makeText", context, javaString, Toast.GetStatic<int>(toastLength));
        toast.Call("show");
    }






    public void StopSpeech()
    {
        TTSExample.Call("StopSpeech");
    }


    /// <summary> This doesn't return realtime accurate result.</summary>

    public bool IsSpeaking()
    {
        return TTSExample.Call<bool>("IsSpeaking");
    }




    public void RegisterUtteranceListeners(string onUtteranceStartedListener, string onUtteranceErrorListener, string onUtteranceDoneListener)
    {
        if(TTSExample == null) { Initialize(); }
        TTSExample.Call("RegisterUtteranceListeners" , onUtteranceStartedListener, onUtteranceErrorListener, onUtteranceDoneListener);

    }



    }
