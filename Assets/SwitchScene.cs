﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Video;
using Vuforia;

public class SwitchScene : MonoBehaviour {


    private void Start()
    {
    }

    public void ChangeScene(bool arScene)
    {

        if(arScene)  {  SceneManager.LoadScene("Scenes/ARVideo" , LoadSceneMode.Single); }

        else
        {
            VideoPlayer videoPlayer = GameObject.Find("VideoMarker").GetComponentInChildren<VideoPlayer>();
            if (videoPlayer) { videoPlayer.Stop(); }
            SceneManager.LoadScene("Scenes/Recipe1", LoadSceneMode.Single);  
        }

    }





   


}
