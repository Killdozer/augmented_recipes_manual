﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SpeechSynthesis : MonoBehaviour {




    private TextToSpeech tts;                                 // Holds the reference to the TTS component attached to this GameObject.

    [SerializeField]
    private InputField inputField;                            // Holds the text to be spoken.
    [SerializeField]
    private string url;                                       // Holds the youtube URL to be opened in the native app.



    void Start () {

        tts = GetComponent<TextToSpeech>();
        StartCoroutine(LoadTTSEngine());
    }





    public void Speak()
    {
        tts.Speak(inputField.text, (string msg) =>
        {
            if (msg.Length > 0) { tts.Toast(msg + " Please enable Text-to-speech output in Settings>Accessibility.", TextToSpeech.ToastLength.LENGTH_LONG); }
        });
    }



    public void StopSpeech()
    {
        tts.StopSpeech();
    }




    public void ChangeSpeed(float value)
    {
        tts.SetSpeed(value);
    }




    public void ChangePitch(float value)
    {
        tts.SetPitch(value);
    }




    public void ShowToast()
    {
        tts.Toast(inputField.text , TextToSpeech.ToastLength.LENGTH_SHORT);
    }



    public void OpenLink()
    {
        Application.OpenURL(url);
    }





    private IEnumerator LoadTTSEngine()
    {
        yield return new WaitForSeconds(0);
        tts.Speak("");
    }



}
