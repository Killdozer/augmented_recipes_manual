﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Test : MonoBehaviour {



    private string consumerKey = "4ce11d6babde40f89b3e3b19da411266";
    private string consumerSecret = "48b740156efe4ce9a17300c98dfb3008";


    // Use this for initialization
    void Start()
    {

        SystemServices systemServices = SystemServices.instance;

        
        systemServices.RunDelayedCommand(3f , ()=> 
        {

           
            SortedDictionary<string, string> parameters = new SortedDictionary<string, string>();
            string baseUrl = "http://platform.fatsecret.com/rest/server.api";
            
            parameters.Add("format", "xml");
            parameters.Add("max_results", "2");
            parameters.Add("method", "foods.search");
            parameters.Add("search_expression", "tomato");


            SystemServices.HTTPMethod meth = new SystemServices.HTTPMethod(SystemServices.HTTPMethod.HTTPMethods.POST);

            SortedDictionary<string, string> newPars = GenerateSignature(meth , baseUrl, consumerSecret ,consumerKey, parameters);


            WWWForm form = new WWWForm();

            foreach(var par in newPars)
            {
                form.AddField(par.Key , par.Value);
            }

            StartCoroutine(systemServices.AsyncPOSTRequest(baseUrl, (string response) =>

            {
                Debug.Log("response is    " + response);
            }, form.data));

            /*
            int a = 0;
            baseUrl += "?";
            foreach (var par in newPars)
            {
                if (a > 0 && a < newPars.Count) baseUrl += "&";

                baseUrl += par.Key + "=" + par.Value;

                a++;
            }


            StartCoroutine(systemServices.AsyncGETRequest(baseUrl, (string response) =>

            {
                Debug.Log("response is    "+response);

            })
            
            );
            */

        }); 
        

    }






    /// <summary>
    /// This method returns the final parameters of an OAuth request.The parameters can then be sent to a server which requires oAuth authentication 1.0.
    /// </summary>
    /// <param name="httpMethod">The HTTP method (GET,POST,PUT)</param>
    /// <param name="baseUrl">The url of the service or api requiring oauth.</param>
    /// <param name="consumerSecret">The consumer secret.</param>
    /// <param name="consumerKey">The consumer key.</param>
    /// <param name="extraParams">The additional parameters that you need to send apart from the oAuth request.</param>
    /// <returns>The oauth parameters and the additional parameters passed in sorted order.</returns>

    public SortedDictionary<string, string> GenerateSignature(SystemServices.HTTPMethod httpMethod, string baseUrl, string consumerSecret, string consumerKey, SortedDictionary<string, string> extraParams = null)
    {

        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;


        SortedDictionary<string, string> finalParams;

        if(extraParams == null) { finalParams = new SortedDictionary<string, string>(); }
        else { finalParams = new SortedDictionary<string, string>(extraParams); }
        


        Uri uri = new Uri(Uri.EscapeUriString(baseUrl));
        string nonce = Guid.NewGuid().ToString();
        string timeStamp = GenerateTimeStamp();


        string signatureBase = CreateSignatureBaseString(httpMethod.method, nonce, timeStamp, uri, finalParams);
        string signatureKey = string.Format("{0}&{1}", consumerSecret, "");
        HMACSHA1 hmac = new HMACSHA1(Encoding.ASCII.GetBytes(signatureKey));

        string signature = Convert.ToBase64String(hmac.ComputeHash(new ASCIIEncoding().GetBytes(signatureBase)));
        finalParams.Add("oauth_signature", signature);

        return finalParams;
    }





    private string NormalizeParameters(SortedDictionary<string, string> parameters)
    {

        StringBuilder sb = new StringBuilder();

        int a = 0;

        foreach (var parameter in parameters)
        {
            if (a > 0) { sb.Append("&"); }
                
            sb.AppendFormat("{0}={1}", parameter.Key, parameter.Value);

            a++;
        }

        return sb.ToString();

    }





    private string CreateSignatureBaseString(string HTTPMethod , string nonce, string timeStamp, Uri url , SortedDictionary<string, string> parameters)
    {

        parameters.Add("oauth_consumer_key", consumerKey);
        parameters.Add("oauth_nonce", nonce);
        parameters.Add("oauth_signature_method", "HMAC-SHA1");
        parameters.Add("oauth_timestamp", timeStamp);
        parameters.Add("oauth_version", "1.0");
        //parameters.Add("search_expression", "tomato");




        StringBuilder sb = new StringBuilder();
        sb.Append(HTTPMethod);
        sb.Append("&" + Uri.EscapeDataString(url.AbsoluteUri));
        sb.Append("&" + Uri.EscapeDataString(NormalizeParameters(parameters)));
        
        return sb.ToString();

    }





    private string GenerateTimeStamp()
    {
        TimeSpan ts = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0));
        string timeStamp = ts.TotalSeconds.ToString();
        timeStamp = timeStamp.Substring(0, timeStamp.IndexOf("."));
        return timeStamp;
    }



}


